# Flywheel Migration Toolkit

**flywheel-migration** is a library that provides configurable parsing and
de-identification of foreign data.

## Development

Install the project using [poetry](https://python-poetry.org/) and enable
[pre-commit](https://pre-commit.com/).

```bash
poetry install
pre-commit install
```

## Testing

Tests are handled in pre-commit containers using
[qa-ci](https://gitlab.com/flywheel-io/tools/etc/qa-ci).

```bash
pre-commit run -a pytest
```

To run all pre-commit hooks, run:

```bash
pre-commit run -a
```

This will execute all hooks defined in [.pre-commit-config.yaml](.pre-commit-config.yaml).

Alternatively, you can run pytests natively by executing the poetry virtual environment,
then calling `pytest` with

```bash
poetry shell
pytest -vv tests/
```

To run individual tests natively such as `test_jitter_for_dicom` in
`test_dicom_file_profile.py`

```bash
poetry shell
pytest -vv tests/test_dicom_file_profile.py -sk test_jitter_for_dicom
```

## Release

Releases are handled with qa-ci automation. Head to
[Build >> Pipelines](https://gitlab.com/flywheel-io/public/migration-toolkit/-/pipelines)
page on repository's sidebar, then click on
[Run Pipeline](https://gitlab.com/flywheel-io/public/migration-toolkit/-/pipelines/new).
Select `Variable` in **Variables** dropdown, and entere `REASEASE` as **variable key**.
Set **variable value** to desired version and click on `Run Pipeline`. This will
create a new release MR that can than be reviewed and merged to deploy a new release.

## Documentation

Documentation is automatically handled by `pre-commit` hook `generate_docs`. The
HTML page is built into `public` and deployed to GitLab pages. Documentation is
available at Documentation at
<https://flywheel-io.gitlab.io/public/migration-toolkit/>.
