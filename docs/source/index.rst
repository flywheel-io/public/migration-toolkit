.. flywheel-migration documentation master file, created by
   sphinx-quickstart on Wed Nov 13 12:25:06 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

flywheel-migration's Documentation
==========================================


.. toctree::
   :maxdepth: 2
   :caption: Contents:


   pages/getting_started
   pages/file_profiles.rst
   pages/fields.rst
   pages/examples.rst
   pages/pixels.rst
   pages/api


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
