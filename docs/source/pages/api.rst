API
***

Below is a description of the flywheel-migration-toolkit API.

DeID Profile
------------
.. automodule:: flywheel_migration.deidentify.deid_profile
   :members:
   :undoc-members:
   :show-inheritance:

DeID Field
----------
.. automodule:: flywheel_migration.deidentify.deid_field
   :members:
   :undoc-members:
   :show-inheritance:

File Profile
------------
.. automodule:: flywheel_migration.deidentify.file_profile
   :members:
   :undoc-members:
   :show-inheritance:

DICOM File Profile
------------------
.. automodule:: flywheel_migration.deidentify.dicom_file_profile
   :members:
   :undoc-members:
   :show-inheritance:

PNG File Profile
------------------
.. automodule:: flywheel_migration.deidentify.png_file_profile
   :members:
   :undoc-members:
   :show-inheritance:

JPG File Profile
------------------
.. automodule:: flywheel_migration.deidentify.jpg_file_profile
   :members:
   :undoc-members:
   :show-inheritance:

TIFF File Profile
------------------
.. automodule:: flywheel_migration.deidentify.tiff_file_profile
   :members:
   :undoc-members:
   :show-inheritance:

KEY/VALUE File Profile
----------------------
.. automodule:: flywheel_migration.deidentify.key_value_text_file_profile
   :members:
   :undoc-members:
   :show-inheritance:

JSON File Profile
------------------
.. automodule:: flywheel_migration.deidentify.json_file_profile
   :members:
   :undoc-members:
   :show-inheritance:

XML File Profile
------------------
.. automodule:: flywheel_migration.deidentify.xml_file_profile
   :members:
   :undoc-members:
   :show-inheritance:

Table File Profile
------------------
.. automodule:: flywheel_migration.deidentify.table_file_profile
   :members:
   :undoc-members:
   :show-inheritance:

Exceptions
----------
.. automodule:: flywheel_migration.deidentify.exceptions
   :members:
   :undoc-members:
   :show-inheritance:
