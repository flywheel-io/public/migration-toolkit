.. _deid-profile-examples-page:

De-Id Profile Examples
======================

We are listing in this section a few profile examples for illustration purposes ONLY.

Multi files de-id profile
-------------------------

Here is an example profile that covers most of the available deid file profile and
a few of the available settings. The profile can be downloaded :download:`here <profile-samples/multi-profiles-example.yml>`.

.. code-block:: yaml

    name: An example de-id profile

    description: A de-id profile that show case a lot of the available settings

    dicom:
      remove-private-tags: true
      date-increment: 10
      filenames:
        - input-regex: '.*'
          output: '{SOPInstanceUID}.dcm'
      fields:
        - name: PatientID
          replace-with: MY_PATIENT_ID
        - name: StudyInstanceUID
          hashuid: true
        - name: SeriesInstanceUID
          hashuid: true
        - name: SOPInstanceUID
          hashuid: true
        - name: PatientName
          remove: true
        - name: AccessionNumber
          remove: true
        - name: InstitutionName
          remove: true
        - name: InstitutionAddress
          remove: true
        - name: ReferringPhysicianName
          remove: true
        - name: ReferringPhysicianAddress
          remove: true
        - name: ReferringPhysicianTelephoneNumbers
          remove: true
        - name: InstitutionalDepartmentName
          remove: true
        - name: PhysiciansOfRecord
          remove: true
        - name: PerformingPhysicianName
          remove: true
        - name: NameOfPhysiciansReadingStudy
          remove: true
        - name: OperatorsName
          remove: true
        - name: AdmittingDiagnosesDescription
          remove: true
        - name: PatientBirthTime
          remove: true
        - name: PatientInsurancePlanCodeSequence
          remove: true
        - name: OtherPatientIDs
          remove: true
        - name: OtherPatientNames
          remove: true
        - name: OtherPatientIDsSequence
          remove: true
        - name: PatientBirthName
          remove: true
        - name: PatientAddress
          remove: true
        - name: PatientMotherBirthName
          remove: true
        - name: MilitaryRank
          remove: true
        - name: MedicalRecordLocator
          remove: true
        - name: PatientTelephoneNumbers
          remove: true
        - name: EthnicGroup
          remove: true
        - name: Occupation
          remove: true
        - name: AdditionalPatientHistory
          remove: true
        - name: ResponsiblePerson
          remove: true
        - name: PatientComments
          remove: true
        - name: ClinicalTrialSponsorName
          remove: true
        - name: ClinicalTrialProtocolID
          remove: true
        - name: ClinicalTrialProtocolName
          remove: true
        - name: ClinicalTrialSiteID
          remove: true
        - name: ClinicalTrialSiteName
          remove: true
        - name: ClinicalTrialSubjectID
          remove: true
        - name: ClinicalTrialTimePointID
          remove: true
        - name: ClinicalTrialTimePointDescription
          remove: true
        - name: ClinicalTrialCoordinatingCenterName
          remove: true
        - name: ProtocolName
          remove: true
        - name: ImageComments
          remove: true
        - name: StudyComments
          remove: true
        - name: RequestingPhysician
          remove: true
        - name: RequestAttributesSequence
          remove: true
        - name: NamesOfIntendedRecipientsOfResults
          remove: true
        - name: PersonIdentificationCodeSequence
          remove: true
        - name: PersonAddress
          remove: true
        - name: PersonTelephoneNumbers
          remove: true
        - name: VerifyingObserverName
          remove: true
        - name: PersonName
          remove: true
        - name: ContentSequence
          remove: true
        - name: ContentCreatorName
          remove: true
        - name: ReviewerName
          remove: true
        - name: OriginalAttributesSequence
          remove: true
        - name: StudyDescription
          remove: true
        - name: DerivationDescription
          remove: true
        - name: ClinicalTrialSeriesDescription
          remove: true
        - name: TherapyDescription
          remove: true
        - name: InterventionDescription
          remove: true
        - name: RequestedProcedureDescription
          remove: true
        - name: AcquisitionProtocolDescription
          remove: true
        - name: ScheduledStationAETitle
          remove: true
        - name: ScheduledPerformingPhysicianName
          remove: true
        - name: DeviceDescription
          remove: true
        - name: DischargeDiagnosisDescription
          remove: true
        - name: StationName
          remove: true
        - name: ScheduledStationName
          remove: true
        - name: PerformedStationAETitle
          remove: true
        - name: PerformedStationName
          remove: true
        - name: PerformedProcedureStepDescription
          remove: true
        - name: DeviceSerialNumber
          remove: true
        - name: PerformedProcedureStepID
          remove: true
        - name: ClinicalTrialSubjectReadingID
          remove: true
        - name: IssuerOfPatientID
          remove: true
        - name: DigitalSignaturesSequence
          remove: true
        - regex: .*IdentificationSequence.*
          remove: true
        - name: NameOfPhysiciansReadingStudy
          remove: true
        - regex: .*DateTime.*
          increment-datetime: true
        - regex: .*Date(?!Time).*
          increment-date: true
        - name: FrameOfReferenceUID
          hashuid: true
        - regex: .*InstanceCreatorUID.*
          hashuid: true
        - regex: .*DimensionOrganizationUID.*
          hashuid: true
        - regex: .*ReferencedSOPInstanceUID.*
          hashuid: true

    zip:
      filenames:
      - input-regex: '(?P<used>.*).dcm.zip$'
        output: '{used}.dcm.zip'
      hash-subdirectories: true
      validate-zip-members: true
      fields:
      - name: comment
        replace-with: FLYWHEEL

    jpg:
      filenames:
        - output: '{name}.jpg'
          input-regex: '(?P<name>.*)\.jpg$'
      date-increment: -17
      remove-gps: True
      fields:
      - name: DateTime
        increment-datetime: true
      - name: Artist
        remove: true
      - name: DateTimeOriginal
        increment-datetime: true
      - name: DateTimeDigitized
        increment-datetime: true
      - name: CameraOwnerName
        replace-with: 'REDACTED'

    png:
      filenames:
        - output: '{name_value}.png'
          input-regex: '(?P<name_value>.*)\.png'
      remove-private-chunks: True
      fields:
        - name: tEXt
          remove: true
        - name: eXIf
          remove: true

    tiff:
      filenames:
        - output: '{DateTime}_{name_value}.tiff'
          input-regex: '(?P<name_value>.*)\.tiff'
      date-increment: -17
      remove-private-tags: True
      fields:
        - name: DateTime
          increment-datetime: true
        - name: Software
          remove: true
        - name: Model
          replace-with: 'REDACTED'

    xml:
      date-increment: -17
      fields:
        - name: /Patient/Patient_Date_Of_Birth
          replace-with: '1900-01-01'
        - name: /Patient/Patient_Name
          remove: true
        - name: /Patient/SUBJECT_ID
          hash: true
        - name: /Patient/Visit/Scan/ScanTime
          increment-datetime: true

    json:
      date-increment: 1
      filenames:
        - output: '{label}_{filename}.json'
          input-regex: '(?P<filename>.*)\.json$'
      fields:
        - name: timestamp
          increment-datetime: True
        - name: info.SiteID
          remove: True
          # regex-sub will be applied first in fields list when processing file
        - name: label
          regex-sub:
            - input-regex: '(?P<current_label>.*)'
              output: '{current_label}_{subject.lastname}_{timestamp}'
              groups:
                - name: current_label
                  replace-with: 'one_cool_cat'
                - name: subject.lastname
                  keep: true
                - name: timestamp
                  increment-datetime: True
        - regex: info\.subject_raw\..*
          replace-with: "REDACTED"
        - name: info.test
          replace-with:
            new: value
            type: dict

    key-value-text-file:
      delimiter: '\s+=\s+'
      encoding: 'utf-8'
      filenames:
        - input-regex: '(?P<first>.*)-(?P<second>.*)-OD-(?P<third>.*)-OCT.mhd'
          output: '{first}-{second}-OD-{third}-OCT.mhd'
          groups:
            - name: first
              replace-with: '111111'
            - name: second
              hash: true
      fields:
        - name: ElementDataFile
          regex-sub:
            - input-regex: '(?P<first>.*)-(?P<second>.*)-OD-(?P<third>.*)-OCT.raw'
              output: '{first}-{second}-OD-{third}-OCT.raw'
              groups:
                - name: first
                  replace-with: '111111'
                - name: second
                  hash: true
        - name: Spam
          replace-with: Eggs

    csv:
      date-increment: 1
      date-format: "%Y-%m-%d"
      datetime-format: "%d%b%Y %H:%M:%S"
      filenames:
        - output: '{info}_{date}.dcm'
          input-regex: '^(?P<info>\w+)-(?P<date>\d{4}-\d{2}-\d{2}).dcm$'
          groups:
            - name: date
              increment-date: true
      fields:
        - name: SeriesDateTime
          increment-datetime: True
        - name: SeriesNumber
          remove: True
          # regex-sub will be applied first in fields list when processing file
        - name: SiteSubject
          regex-sub:
            - input-regex: '(?P<current_value>.*)'
              output: '{current_value}_{SeriesNumber}_{SeriesDateTime}'
              groups:
                - name: current_value
                  replace-with: 'one_cool_cat'
                - name: SeriesNumber
                  keep: true
                - name: SeriesDateTime
                  increment-datetime: True


Part 15 Compliant DICOM de-id profile
-------------------------------------

Here is an example of a best effort profile that aligns with the
`DICOM Part 15E Attribute Confidentiality Profile <https://dicom.nema.org/medical/dicom/current/output/html/part15.html#chapter_E>`_
guidelines. It includes:

* actions for public dicom tags as specified by NEMA.
* retention of private dicom tags specified safe by NEMA.
* removal of long modified dates.
* removal of all tags not specified in the profile.
* recursion through sequence data element.

The profile can be downloaded :download:`here <profile-samples/nema-part-15E-based-profile.yml>`.

.. code-block:: yaml

    name: part15_compliant_deid_profile
    description: Deid based on DICOM Part 15E NEMA Guidelines
    dicom:
      recurse-sequence: true
      remove-undefined: true
      date-increment: 25
    filenames:
      - output: "{filename}_deid.dcm"
        input-regex: ^(?P<filename>.*)$
    fields:
      - name: AccessionNumber
        replace-with: ""
      - name: AcquisitionComments
        remove: true
      - name: AcquisitionContextSequence
        replace-with: []
      - name: AcquisitionDate
        increment-date: true
      - name: AcquisitionDateTime
        increment-datetime: true
      - name: AcquisitionDeviceProcessingDescription
        replace-with: " "
      - name: AcquisitionFieldOfViewLabel
        replace-with: " "
      - name: AcquisitionProtocolDescription
        remove: true
      - name: AcquisitionTime
        remove: true
      - name: AcquisitionUID
        hashuid: true
      - name: ActualHumanPerformersSequence
        remove: true
      - name: AdditionalPatientHistory
        remove: true
      - name: AddressTrial
        remove: true
      - name: AdmissionID
        remove: true
      - name: AdmittingDate
        increment-date: true
      - name: AdmittingDiagnosesCodeSequence
        remove: true
      - name: AdmittingDiagnosesDescription
        remove: true
      - name: AdmittingTime
        remove: true
      - name: AffectedSOPInstanceUID
        remove: true
      - name: Allergies
        remove: true
      - name: AnnotationGroupDescription
        remove: true
      - name: AnnotationGroupLabel
        replace-with: " "
      - name: AnnotationGroupUID
        replace-with: ""
      - name: ApprovalStatusDateTime
        increment-datetime: true
      - name: Arbitrary
        remove: true
      - name: AssertionDateTime
        increment-datetime: true
      - name: AssertionExpirationDateTime
        increment-datetime: true
      - name: AttributeModificationDateTime
        increment-datetime: true
      - name: AuthorObserverSequence
        remove: true
      - name: BarcodeValue
        replace-with: ""
      - name: BeamDescription
        remove: true
      - name: BeamHoldTransitionDateTime
        increment-datetime: true
      - name: BolusDescription
        remove: true
      - name: BranchOfService
        remove: true
      - name: CalibrationDate
        increment-date: true
      - name: CalibrationDateTime
        increment-datetime: true
      - name: CalibrationTime
        remove: true
      - name: CameraOwnerName
        remove: true
      - name: CassetteID
        remove: true
      - name: CertificateOfSigner
        replace-with: ""
      - name: CertifiedTimestamp
        jitter: true
      - name: ClinicalTrialCoordinatingCenterName
        replace-with: ""
      - name: ClinicalTrialProtocolEthicsCommitteeApprovalNumber
        remove: true
      - name: ClinicalTrialProtocolEthicsCommitteeName
        replace-with: " "
      - name: ClinicalTrialProtocolID
        replace-with: " "
      - name: ClinicalTrialProtocolName
        replace-with: ""
      - name: ClinicalTrialSeriesDescription
        remove: true
      - name: ClinicalTrialSeriesID
        remove: true
      - name: ClinicalTrialSiteID
        replace-with: ""
      - name: ClinicalTrialSiteName
        replace-with: ""
      - name: ClinicalTrialSponsorName
        replace-with: " "
      - name: ClinicalTrialSubjectID
        replace-with: " "
      - name: ClinicalTrialSubjectReadingID
        replace-with: " "
      - name: ClinicalTrialTimePointDescription
        remove: true
      - name: ClinicalTrialTimePointID
        replace-with: ""
      - name: CommentsOnRadiationDose
        remove: true
      - name: CommentsOnThePerformedProcedureStep
        remove: true
      - name: CompensatorDescription
        remove: true
      - name: ConcatenationUID
        hashuid: true
      - name: ConceptualVolumeCombinationDescription
        replace-with: ""
      - name: ConceptualVolumeDescription
        replace-with: ""
      - name: ConceptualVolumeUID
        hashuid: true
      - name: ConfidentialityConstraintOnPatientDataDescription
        remove: true
      - name: ConstituentConceptualVolumeUID
        hashuid: true
      - name: ConsultingPhysicianName
        replace-with: ""
      - name: ConsultingPhysicianIdentificationSequence
        remove: true
      - name: ContainerComponentID
        remove: true
      - name: ContainerDescription
        remove: true
      - name: ContainerIdentifier
        replace-with: " "
      - name: ContentCreatorIdentificationCodeSequence
        remove: true
      - name: ContentCreatorName
        replace-with: " "
      - name: ContentDate
        increment-date: true
      - name: ContentSequence
        replace-with: []
      - name: ContentTime
        remove: true
      - name: ContextGroupLocalVersion
        increment-datetime: true
      - name: ContextGroupVersion
        increment-datetime: true
      - name: ContrastBolusAgent
        replace-with: " "
      - name: ContrastBolusStartTime
        remove: true
      - name: ContrastBolusStopTime
        remove: true
      - name: ContributionDateTime
        increment-datetime: true
      - name: ContributionDescription
        remove: true
      - name: CountryOfResidence
        remove: true
      - name: CreationDate
        increment-date: true
      - name: CreationTime
        remove: true
      - name: CurrentObserverTrial
        remove: true
      - name: CurrentPatientLocation
        remove: true
      - name: CurveDate
        increment-date: true
      - name: CurveTime
        remove: true
      - name: CustodialOrganizationSequence
        remove: true
      - name: DataSetTrailingPadding
        remove: true
      - name: Date
        increment-date: true
      - name: DateOfDocumentOrVerbalTransactionTrial
        increment-date: true
      - name: (0018,1205)
        jitter: true
      - name: DateOfLastCalibration
        increment-date: true
      - name: DateOfLastDetectorCalibration
        increment-date: true
      - name: (0018,1204)
        jitter: true
      - name: DateOfSecondaryCapture
        increment-date: true
      - name: DateTime
        increment-datetime: true
      - name: DateTimeOfLastCalibration
        increment-datetime: true
      - name: DecayCorrectionDateTime
        increment-datetime: true
      - name: DecompositionDescription
        remove: true
      - name: DerivationDescription
        remove: true
      - name: DestinationAE
        replace-with: " "
      - name: DetectorID
        replace-with: " "
      - name: DeviceAlternateIdentifier
        replace-with: ""
      - name: DeviceDescription
        remove: true
      - name: DeviceLabel
        replace-with: " "
      - name: DeviceSerialNumber
        replace-with: " "
      - name: DeviceSettingDescription
        remove: true
      - name: DeviceUID
        hashuid: true
      - name: DigitalSignatureDateTime
        increment-datetime: true
      - name: DigitalSignaturesSequence
        remove: true
      - name: DigitalSignatureUID
        hashuid: true
      - name: DimensionOrganizationUID
        hashuid: true
      - name: DischargeDate
        increment-date: true
      - name: DischargeDiagnosisDescription
        remove: true
      - name: DischargeTime
        remove: true
      - name: DisplacementReferenceLabel
        remove: true
      - name: DistributionAddress
        remove: true
      - name: DistributionName
        remove: true
      - name: DoseReferenceDescription
        remove: true
      - name: DoseReferenceUID
        hashuid: true
      - name: DosimetricObjectiveUID
        hashuid: true
      - name: EffectiveDateTime
        increment-datetime: true
      - name: EncapsulatedDocument
        replace-with: ""
      - name: EndAcquisitionDateTime
        increment-datetime: true
      - name: EntityDescription
        remove: true
      - name: EntityLabel
        replace-with: " "
      - name: EntityLongLabel
        replace-with: " "
      - name: EntityName
        remove: true
      - name: EquipmentFrameOfReferenceDescription
        remove: true
      - name: EthicsCommitteeApprovalEffectivenessEndDate
        increment-date: true
      - name: EthicsCommitteeApprovalEffectivenessStartDate
        increment-date: true
      - name: EthnicGroup
        remove: true
      - name: (0010,2161)
        remove: true
      - name: ExclusionStartDateTime
        increment-datetime: true
      - name: ExpectedCompletionDateTime
        increment-datetime: true
      - name: FailedSOPInstanceUIDList
        hashuid: true
      - name: FiducialUID
        hashuid: true
      - name: FillerOrderNumberImagingServiceRequest
        replace-with: ""
      - name: FilterLookupTableDescription
        remove: true
      - name: FindingsGroupRecordingDateTrial
        increment-date: true
      - name: FindingsGroupRecordingTimeTrial
        remove: true
      - name: FirstTreatmentDate
        increment-date: true
      - name: FixationDeviceDescription
        remove: true
      - name: FlowIdentifier
        replace-with: ""
      - name: FlowIdentifierSequence
        replace-with: []
      - name: FractionationNotes
        replace-with: ""
      - name: FractionGroupDescription
        remove: true
      - name: FrameAcquisitionDateTime
        increment-datetime: true
      - name: FrameComments
        remove: true
      - name: FrameOfReferenceUID
        hashuid: true
      - name: FrameOriginTimestamp
        jitter: true
      - name: FrameReferenceDateTime
        increment-datetime: true
      - name: FunctionalSyncPulse
        increment-datetime: true
      - name: GantryID
        remove: true
      - name: GeneratorID
        remove: true
      - name: GPSAltitude
        remove: true
      - name: GPSAltitudeRef
        remove: true
      - name: GPSAreaInformation
        remove: true
      - name: GPSDateStamp
        increment-datetime: true
      - name: GPSDestBearing
        remove: true
      - name: GPSDestBearingRef
        remove: true
      - name: GPSDestDistance
        remove: true
      - name: GPSDestDistanceRef
        remove: true
      - name: GPSDestLatitude
        remove: true
      - name: GPSDestLatitudeRef
        remove: true
      - name: GPSDestLongitude
        remove: true
      - name: GPSDestLongitudeRef
        remove: true
      - name: GPSDifferential
        remove: true
      - name: GPSDOP
        remove: true
      - name: GPSImgDirection
        remove: true
      - name: GPSImgDirectionRef
        remove: true
      - name: GPSLatitude
        remove: true
      - name: GPSLatitudeRef
        remove: true
      - name: GPSLongitude
        remove: true
      - name: GPSLongitudeRef
        remove: true
      - name: GPSMapDatum
        remove: true
      - name: GPSMeasureMode
        remove: true
      - name: GPSProcessingMethod
        remove: true
      - name: GPSSatellites
        remove: true
      - name: GPSSpeed
        remove: true
      - name: GPSSpeedRef
        remove: true
      - name: GPSStatus
        remove: true
      - name: GPSTimeStamp
        remove: true
      - name: GPSTrack
        remove: true
      - name: GPSTrackRef
        remove: true
      - name: GPSVersionID
        remove: true
      - name: GraphicAnnotationSequence
        replace-with: []
      - name: HangingProtocolCreationDateTime
        increment-datetime: true
      - name: HL7DocumentEffectiveTime
        increment-datetime: true
      - name: HumanPerformerName
        remove: true
      - name: HumanPerformerOrganization
        remove: true
      - name: IconImageSequence
        remove: true
      - name: IdentifyingComments
        remove: true
      - name: ImageComments
        remove: true
      - name: ImagePresentationComments
        remove: true
      - name: ImagingServiceRequestComments
        remove: true
      - name: ImpedanceMeasurementDateTime
        increment-datetime: true
      - name: Impressions
        remove: true
      - name: InformationIssueDateTime
        increment-datetime: true
      - name: InstanceCoercionDateTime
        increment-datetime: true
      - name: InstanceCreationDate
        increment-date: true
      - name: InstanceCreationTime
        remove: true
      - name: InstanceCreatorUID
        hashuid: true
      - name: InstanceOriginStatus
        remove: true
      - name: InstitutionAddress
        remove: true
      - name: InstitutionalDepartmentName
        remove: true
      - name: InstitutionalDepartmentTypeCodeSequence
        remove: true
      - name: InstitutionCodeSequence
        replace-with: []
      - name: InstitutionName
        replace-with: " "
      - name: InstructionPerformedDateTime
        increment-datetime: true
      - name: InsurancePlanIdentification
        remove: true
      - name: IntendedFractionStartTime
        remove: true
      - name: IntendedPhaseEndDate
        increment-date: true
      - name: IntendedPhaseStartDate
        increment-date: true
      - name: IntendedRecipientsOfResultsIdentificationSequence
        remove: true
      - name: InterlockDateTime
        increment-datetime: true
      - name: InterlockDescription
        replace-with: " "
      - name: InterlockOriginDescription
        replace-with: " "
      - name: InterpretationApprovalDate
        increment-date: true
      - name: InterpretationApprovalTime
        remove: true
      - name: InterpretationApproverSequence
        remove: true
      - name: InterpretationAuthor
        remove: true
      - name: InterpretationDiagnosisDescription
        remove: true
      - name: InterpretationID
        remove: true
      - name: InterpretationIDIssuer
        remove: true
      - name: InterpretationRecordedDate
        increment-date: true
      - name: InterpretationRecordedTime
        remove: true
      - name: InterpretationRecorder
        remove: true
      - name: InterpretationText
        remove: true
      - name: InterpretationTranscriber
        remove: true
      - name: InterpretationTranscriptionDate
        increment-date: true
      - name: InterpretationTranscriptionTime
        remove: true
      - name: InterventionDrugStartTime
        remove: true
      - name: InterventionDrugStopTime
        remove: true
      - name: IrradiationEventUID
        hashuid: true
      - name: IssueDateOfImagingServiceRequest
        increment-date: true
      - name: IssuerOfAdmissionID
        remove: true
      - name: IssuerOfAdmissionIDSequence
        remove: true
      - name: (0012,0022)
        remove: true
      - name: (0012,0073)
        remove: true
      - name: (0012,0032)
        remove: true
      - name: (0012,0041)
        remove: true
      - name: (0012,0043)
        remove: true
      - name: (0012,0055)
        remove: true
      - name: IssuerOfPatientID
        remove: true
      - name: IssuerOfServiceEpisodeID
        remove: true
      - name: IssuerOfServiceEpisodeIDSequence
        remove: true
      - name: IssuerOfTheContainerIdentifierSequence
        replace-with: []
      - name: IssuerOfTheSpecimenIdentifierSequence
        replace-with: []
      - name: IssueTimeOfImagingServiceRequest
        remove: true
      - name: LabelText
        replace-with: ""
      - name: LargePaletteColorLookupTableUID
        hashuid: true
      - name: LastMenstrualDate
        increment-date: true
      - name: LensMake
        remove: true
      - name: LensModel
        remove: true
      - name: LensSerialNumber
        remove: true
      - name: LensSpecification
        remove: true
      - name: LongDeviceDescription
        remove: true
      - name: MAC
        remove: true
      - name: MakerNote
        remove: true
      - name: ManufacturerDeviceClassUID
        hashuid: true
      - name: ManufacturerDeviceIdentifier
        replace-with: ""
      - name: MediaStorageSOPInstanceUID
        hashuid: true
      - name: MedicalAlerts
        remove: true
      - name: MedicalRecordLocator
        remove: true
      - name: MilitaryRank
        remove: true
      - name: ModifiedAttributesSequence
        remove: true
      - name: ModifiedImageDate
        increment-date: true
      - name: ModifiedImageDescription
        remove: true
      - name: ModifiedImageTime
        remove: true
      - name: ModifyingDeviceID
        remove: true
      - name: ModifyingSystem
        replace-with: " "
      - name: MostRecentTreatmentDate
        increment-date: true
      - name: MultienergyAcquisitionDescription
        remove: true
      - name: MultiplexGroupUID
        hashuid: true
      - name: NameOfPhysiciansReadingStudy
        remove: true
      - name: NamesOfIntendedRecipientsOfResults
        remove: true
      - name: NetworkID
        remove: true
      - name: NonconformingDataElementValue
        remove: true
      - name: NonconformingModifiedAttributesSequence
        remove: true
      - name: ObservationDateTrial
        increment-date: true
      - name: ObservationDateTime
        increment-datetime: true
      - name: ObservationStartDateTime
        increment-datetime: true
      - name: ObservationSubjectUIDTrial
        hashuid: true
      - name: ObservationTimeTrial
        remove: true
      - name: ObservationUID
        hashuid: true
      - name: Occupation
        remove: true
      - name: OperatorIdentificationSequence
        replace-with: []
      - name: OperatorsName
        replace-with: " "
      - name: OrderCallbackPhoneNumber
        remove: true
      - name: OrderCallbackTelecomInformation
        remove: true
      - name: OrderEnteredBy
        remove: true
      - name: OrderEntererLocation
        remove: true
      - name: OriginalAttributesSequence
        remove: true
      - name: Originator
        remove: true
      - name: (0012,0023)
        remove: true
      - name: OtherPatientIDs
        remove: true
      - name: OtherPatientIDsSequence
        remove: true
      - name: OtherPatientNames
        remove: true
      - name: (60xx,4000)
        remove: true
      - name: (60xx,3000)
        remove: true
      - name: OverlayDate
        increment-date: true
      - name: OverlayTime
        remove: true
      - name: OverrideDateTime
        increment-datetime: true
      - name: PaletteColorLookupTableUID
        hashuid: true
      - name: ParticipantSequence
        remove: true
      - name: ParticipationDateTime
        increment-datetime: true
      - name: PatientAddress
        remove: true
      - name: PatientAge
        remove: true
      - name: PatientBirthDate
        replace-with: ""
      - name: PatientBirthName
        remove: true
      - name: PatientBirthTime
        remove: true
      - name: PatientInstitutionResidence
        remove: true
      - name: PatientInsurancePlanCodeSequence
        remove: true
      - name: PatientMotherBirthName
        remove: true
      - name: PatientName
        replace-with: ""
      - name: PatientPrimaryLanguageCodeSequence
        remove: true
      - name: PatientPrimaryLanguageModifierCodeSequence
        remove: true
      - name: PatientReligiousPreference
        remove: true
      - name: PatientSex
        replace-with: ""
      - name: PatientSexNeutered
        replace-with: ""
      - name: PatientSize
        remove: true
      - name: PatientTelecomInformation
        remove: true
      - name: PatientTelephoneNumbers
        remove: true
      - name: PatientWeight
        remove: true
      - name: PatientComments
        remove: true
      - name: PatientID
        replace-with: " "
      - name: PatientSetupPhotoDescription
        remove: true
      - name: PatientSetupUID
        hashuid: true
      - name: PatientState
        remove: true
      - name: PatientTransportArrangements
        remove: true
      - name: PatientTreatmentPreparationMethodDescription
        remove: true
      - name: PatientTreatmentPreparationProcedureParameterDescription
        remove: true
      - name: PerformedLocation
        remove: true
      - name: PerformedProcedureStepDescription
        remove: true
      - name: PerformedProcedureStepEndDate
        increment-date: true
      - name: PerformedProcedureStepEndDateTime
        increment-datetime: true
      - name: PerformedProcedureStepEndTime
        remove: true
      - name: PerformedProcedureStepID
        remove: true
      - name: PerformedProcedureStepStartDate
        increment-date: true
      - name: PerformedProcedureStepStartDateTime
        increment-datetime: true
      - name: PerformedProcedureStepStartTime
        remove: true
      - name: PerformedStationAETitle
        remove: true
      - name: PerformedStationGeographicLocationCodeSequence
        remove: true
      - name: PerformedStationName
        remove: true
      - name: PerformedStationNameCodeSequence
        remove: true
      - name: PerformingPhysicianName
        remove: true
      - name: PerformingPhysicianIdentificationSequence
        remove: true
      - name: PersonAddress
        remove: true
      - name: PersonTelecomInformation
        remove: true
      - name: PersonTelephoneNumbers
        remove: true
      - name: PersonIdentificationCodeSequence
        replace-with: []
      - name: PersonName
        replace-with: " "
      - name: PhysiciansOfRecord
        remove: true
      - name: PhysiciansOfRecordIdentificationSequence
        remove: true
      - name: PhysiciansReadingStudyIdentificationSequence
        remove: true
      - name: PhysicianApprovingInterpretation
        remove: true
      - name: PlacerOrderNumberImagingServiceRequest
        replace-with: ""
      - name: PlateID
        remove: true
      - name: PositionAcquisitionTemplateDescription
        remove: true
      - name: PositionAcquisitionTemplateName
        remove: true
      - name: PregnancyStatus
        remove: true
      - name: PreMedication
        remove: true
      - name: PrescriptionDescription
        remove: true
      - name: PrescriptionNotes
        replace-with: ""
      - name: PrescriptionNotesSequence
        replace-with: []
      - name: PresentationCreationDate
        increment-date: true
      - name: PresentationCreationTime
        remove: true
      - name: PresentationDisplayCollectionUID
        hashuid: true
      - name: PresentationSequenceCollectionUID
        hashuid: true
      - name: PriorTreatmentDoseDescription
        remove: true
      - name: ProcedureStepCancellationDateTime
        increment-datetime: true
      - name: ProductExpirationDateTime
        increment-datetime: true
      - name: ProtocolName
        replace-with: " "
      - name: PyramidDescription
        remove: true
      - name: PyramidLabel
        remove: true
      - name: PyramidUID
        hashuid: true
      - name: RadiationDoseIdentificationLabel
        replace-with: " "
      - name: RadiationDoseInVivoMeasurementLabel
        replace-with: " "
      - name: RadiationGenerationModeDescription
        replace-with: ""
      - name: RadiationGenerationModeLabel
        replace-with: " "
      - name: RadiopharmaceuticalStartDateTime
        increment-datetime: true
      - name: RadiopharmaceuticalStartTime
        remove: true
      - name: RadiopharmaceuticalStopDateTime
        increment-datetime: true
      - name: RadiopharmaceuticalStopTime
        remove: true
      - name: ReasonForOmissionDescription
        remove: true
      - name: ReasonForRequestedProcedureCodeSequence
        remove: true
      - name: ReasonForStudy
        remove: true
      - name: ReasonForSuperseding
        replace-with: ""
      - name: ReasonForTheAttributeModification
        replace-with: "0"
      - name: ReasonForTheImagingServiceRequest
        remove: true
      - name: ReasonForTheRequestedProcedure
        remove: true
      - name: ReasonForVisit
        remove: true
      - name: ReasonForVisitCodeSequence
        remove: true
      - name: ReceivingAE
        remove: true
      - name: RecordedRTControlPointDateTime
        increment-datetime: true
      - name: ReferencedConceptualVolumeUID
        hashuid: true
      - name: ReferencedDateTime
        increment-datetime: true
      - name: ReferencedDigitalSignatureSequence
        remove: true
      - name: ReferencedDoseReferenceUID
        hashuid: true
      - name: ReferencedDosimetricObjectiveUID
        hashuid: true
      - name: ReferencedFiducialsUID
        hashuid: true
      - name: ReferencedFrameOfReferenceUID
        hashuid: true
      - name: ReferencedGeneralPurposeScheduledProcedureStepTransactionUID
        hashuid: true
      - name: ReferencedImageSequence
        hashuid: true
      - name: ReferencedObservationUIDTrial
        hashuid: true
      - name: ReferencedPatientAliasSequence
        remove: true
      - name: ReferencedPatientPhotoSequence
        remove: true
      - name: ReferencedPatientSequence
        remove: true
      - name: ReferencedPerformedProcedureStepSequence
        replace-with: []
      - name: ReferencedSOPInstanceMACSequence
        remove: true
      - name: ReferencedSOPInstanceUID
        hashuid: true
      - name: ReferencedSOPInstanceUIDInFile
        hashuid: true
      - name: ReferencedStudySequence
        replace-with: []
      - name: ReferencedTreatmentPositionGroupUID
        hashuid: true
      - name: ReferringPhysicianAddress
        remove: true
      - name: ReferringPhysicianName
        replace-with: ""
      - name: ReferringPhysicianTelephoneNumbers
        remove: true
      - name: ReferringPhysicianIdentificationSequence
        remove: true
      - name: RegionOfResidence
        remove: true
      - name: RelatedFrameOfReferenceUID
        hashuid: true
      - name: RequestAttributesSequence
        remove: true
      - name: RequestedContrastAgent
        remove: true
      - name: RequestedProcedureComments
        remove: true
      - name: RequestedProcedureDescription
        replace-with: ""
      - name: RequestedProcedureID
        remove: true
      - name: RequestedProcedureLocation
        remove: true
      - name: RequestedSeriesDescription
        remove: true
      - name: RequestedSOPInstanceUID
        hashuid: true
      - name: RequestingAE
        remove: true
      - name: RequestingPhysician
        remove: true
      - name: RequestingService
        remove: true
      - name: RespiratoryMotionCompensationTechniqueDescription
        remove: true
      - name: ResponsibleOrganization
        remove: true
      - name: ResponsiblePerson
        remove: true
      - name: ResultsComments
        remove: true
      - name: ResultsDistributionListSequence
        remove: true
      - name: ResultsID
        remove: true
      - name: ResultsIDIssuer
        remove: true
      - name: RetrieveAETitle
        remove: true
      - name: ReviewDate
        increment-date: true
      - name: ReviewerName
        replace-with: ""
      - name: ReviewTime
        remove: true
      - name: (3006,004D)
        remove: true
      - name: (3006,002D)
        jitter: true
      - name: ROIDescription
        remove: true
      - name: ROIGenerationDescription
        remove: true
      - name: ROIInterpreter
        replace-with: ""
      - name: (3006,004E)
        remove: true
      - name: ROIName
        replace-with: ""
      - name: (3006,002E)
        jitter: true
      - name: ROIObservationDescription
        remove: true
      - name: ROIObservationLabel
        remove: true
      - name: RTAccessoryDeviceSlotID
        replace-with: ""
      - name: RTAccessoryHolderSlotID
        replace-with: ""
      - name: RTPhysicianIntentNarrative
        replace-with: ""
      - name: RTPlanDate
        increment-date: true
      - name: RTPlanDescription
        remove: true
      - name: RTPlanLabel
        replace-with: " "
      - name: RTPlanName
        remove: true
      - name: RTPlanTime
        remove: true
      - name: RTPrescriptionLabel
        replace-with: " "
      - name: RTToleranceSetLabel
        replace-with: " "
      - name: RTTreatmentApproachLabel
        replace-with: " "
      - name: RTTreatmentPhaseUID
        hashuid: true
      - name: SafePositionExitDate
        increment-date: true
      - name: SafePositionExitTime
        remove: true
      - name: SafePositionReturnDate
        increment-date: true
      - name: SafePositionReturnTime
        remove: true
      - name: ScheduledAdmissionDate
        increment-date: true
      - name: ScheduledAdmissionTime
        remove: true
      - name: ScheduledDischargeDate
        increment-date: true
      - name: ScheduledDischargeTime
        remove: true
      - name: ScheduledHumanPerformersSequence
        remove: true
      - name: ScheduledPatientInstitutionResidence
        remove: true
      - name: ScheduledPerformingPhysicianName
        remove: true
      - name: ScheduledPerformingPhysicianIdentificationSequence
        remove: true
      - name: ScheduledProcedureStepDescription
        remove: true
      - name: ScheduledProcedureStepEndDate
        increment-date: true
      - name: ScheduledProcedureStepEndTime
        remove: true
      - name: ScheduledProcedureStepExpirationDateTime
        increment-datetime: true
      - name: ScheduledProcedureStepID
        remove: true
      - name: ScheduledProcedureStepLocation
        remove: true
      - name: ScheduledProcedureStepModificationDateTime
        increment-datetime: true
      - name: ScheduledProcedureStepStartDate
        increment-date: true
      - name: ScheduledProcedureStepStartDateTime
        increment-datetime: true
      - name: ScheduledProcedureStepStartTime
        remove: true
      - name: ScheduledStationAETitle
        remove: true
      - name: ScheduledStationGeographicLocationCodeSequence
        remove: true
      - name: ScheduledStationName
        remove: true
      - name: ScheduledStationNameCodeSequence
        remove: true
      - name: ScheduledStudyLocation
        remove: true
      - name: ScheduledStudyLocationAETitle
        remove: true
      - name: ScheduledStudyStartDate
        increment-date: true
      - name: ScheduledStudyStartTime
        remove: true
      - name: ScheduledStudyStopDate
        increment-date: true
      - name: ScheduledStudyStopTime
        remove: true
      - name: SelectorAEValue
        replace-with: " "
      - name: SelectorASValue
        replace-with: 0Y
      - name: SelectorDAValue
        increment-date: true
      - name: SelectorDTValue
        increment-datetime: true
      - name: SelectorLOValue
        replace-with: " "
      - name: SelectorLTValue
        replace-with: " "
      - name: SelectorOBValue
        replace-with: ""
      - name: SelectorPNValue
        replace-with: " "
      - name: SelectorSHValue
        replace-with: " "
      - name: SelectorSTValue
        replace-with: " "
      - name: SelectorTMValue
        remove: true
      - name: SelectorUNValue
        replace-with: ""
      - name: SelectorURValue
        replace-with: " "
      - name: SelectorUTValue
        replace-with: " "
      - name: SeriesDate
        increment-date: true
      - name: SeriesDescription
        remove: true
      - name: SeriesInstanceUID
        hashuid: true
      - name: SeriesTime
        remove: true
      - name: ServiceEpisodeDescription
        remove: true
      - name: ServiceEpisodeID
        remove: true
      - name: SetupTechniqueDescription
        remove: true
      - name: ShieldingDeviceDescription
        remove: true
      - name: SlideIdentifier
        remove: true
      - name: SmokingStatus
        remove: true
      - name: SOPAuthorizationDateTime
        increment-datetime: true
      - name: SOPInstanceUID
        hashuid: true
      - name: SourceConceptualVolumeUID
        hashuid: true
      - name: SourceEndDateTime
        increment-datetime: true
      - name: SourceFrameOfReferenceUID
        hashuid: true
      - name: SourceIdentifier
        replace-with: ""
      - name: SourceImageSequence
        hashuid: true
      - name: SourceManufacturer
        remove: true
      - name: SourceOfPreviousValues
        replace-with: ""
      - name: SourceSerialNumber
        replace-with: ""
      - name: SourceStartDateTime
        increment-datetime: true
      - name: SourceStrengthReferenceDate
        increment-date: true
      - name: SourceStrengthReferenceTime
        remove: true
      - name: SpecialNeeds
        remove: true
      - name: SpecimenAccessionNumber
        remove: true
      - name: SpecimenDetailedDescription
        remove: true
      - name: SpecimenIdentifier
        replace-with: " "
      - name: SpecimenPreparationSequence
        replace-with: []
      - name: SpecimenShortDescription
        remove: true
      - name: SpecimenUID
        hashuid: true
      - name: StartAcquisitionDateTime
        increment-datetime: true
      - name: StationAETitle
        remove: true
      - name: StationName
        replace-with: " "
      - name: StorageMediaFileSetUID
        hashuid: true
      - name: StructureSetDate
        increment-date: true
      - name: StructureSetDescription
        remove: true
      - name: StructureSetLabel
        replace-with: " "
      - name: StructureSetName
        remove: true
      - name: StructureSetTime
        remove: true
      - name: StudyArrivalDate
        increment-date: true
      - name: StudyArrivalTime
        remove: true
      - name: StudyComments
        remove: true
      - name: StudyCompletionDate
        increment-date: true
      - name: StudyCompletionTime
        remove: true
      - name: StudyDate
        increment-date: true
      - name: StudyDescription
        remove: true
      - name: StudyID
        replace-with: ""
      - name: StudyIDIssuer
        remove: true
      - name: StudyInstanceUID
        hashuid: true
      - name: StudyReadDate
        increment-date: true
      - name: StudyReadTime
        remove: true
      - name: StudyTime
        remove: true
      - name: StudyVerifiedDate
        increment-date: true
      - name: StudyVerifiedTime
        remove: true
      - name: SubstanceAdministrationDateTime
        increment-datetime: true
      - name: SynchronizationFrameOfReferenceUID
        hashuid: true
      - name: (300A,0054)
        hashuid: true
      - name: TargetUID
        hashuid: true
      - name: TelephoneNumberTrial
        remove: true
      - name: TemplateExtensionCreatorUID
        hashuid: true
      - name: TemplateExtensionOrganizationUID
        hashuid: true
      - name: TemplateLocalVersion
        increment-datetime: true
      - name: TemplateVersion
        increment-datetime: true
      - name: TextComments
        remove: true
      - name: TextString
        remove: true
      - name: Time
        remove: true
      - name: TimeOfDocumentCreationOrVerbalTransactionTrial
        remove: true
      - name: TimeOfLastCalibration
        remove: true
      - name: TimeOfLastDetectorCalibration
        remove: true
      - name: TimeOfSecondaryCapture
        remove: true
      - name: TimezoneOffsetFromUTC
        hash: true
      - name: TopicAuthor
        remove: true
      - name: TopicKeywords
        remove: true
      - name: TopicSubject
        remove: true
      - name: TopicTitle
        remove: true
      - name: TrackingUID
        hashuid: true
      - name: TransactionUID
        hashuid: true
      - name: TransducerIdentificationSequence
        remove: true
      - name: TreatmentControlPointDate
        increment-date: true
      - name: TreatmentControlPointTime
        remove: true
      - name: TreatmentDate
        increment-date: true
      - name: TreatmentMachineName
        replace-with: ""
      - name: TreatmentPositionGroupLabel
        replace-with: " "
      - name: TreatmentPositionGroupUID
        hashuid: true
      - name: TreatmentSessionUID
        hashuid: true
      - name: TreatmentSite
        replace-with: " "
      - name: TreatmentSites
        remove: true
      - name: TreatmentTechniqueNotes
        replace-with: ""
      - name: TreatmentTime
        remove: true
      - name: TreatmentToleranceViolationDateTime
        increment-datetime: true
      - name: TreatmentToleranceViolationDescription
        replace-with: " "
      - name: UDISequence
        remove: true
      - name: UID
        hashuid: true
      - name: UniqueDeviceIdentifier
        remove: true
      - name: UserContentLabel
        replace-with: " "
      - name: UserContentLongLabel
        replace-with: " "
      - name: VerbalSourceTrial
        remove: true
      - name: VerbalSourceIdentifierCodeSequenceTrial
        remove: true
      - name: VerificationDateTime
        increment-datetime: true
      - name: VerifyingObserverIdentificationCodeSequence
        replace-with: []
      - name: VerifyingObserverName
        replace-with: " "
      - name: VerifyingObserverSequence
        replace-with: []
      - name: VerifyingOrganization
        replace-with: " "
      - name: VisitComments
        remove: true
      - name: WaveformFilterDescription
        remove: true
      - name: XRayDetectorID
        replace-with: " "
      - name: XRayDetectorLabel
        remove: true
      - name: XRaySourceID
        replace-with: " "
      - name: (7053, Philips PET Private Group, 00)
        jitter: true
      - name: (7053, Philips PET Private Group, 09)
        jitter: true
      - name: (00E1, ELSCINT1, 21)
        jitter: true
      - name: (00E1, ELSCINT1, 50)
        jitter: true
      - name: (01E1, ELSCINT1, 26)
        remove: true
      - name: (01F1, ELSCINT1, 01)
        remove: true
      - name: (01F1, ELSCINT1, 07)
        jitter: true
      - name: (01F1, ELSCINT1, 26)
        jitter: true
      - name: (01F1, ELSCINT1, 27)
        jitter: true
      - name: (0019, GEMS_ACQU_01, 23)
        jitter: true
      - name: (0019, GEMS_ACQU_01, 24)
        jitter: true
      - name: (0019, GEMS_ACQU_01, 27)
        jitter: true
      - name: (0019, GEMS_ACQU_01, 9E)
        hash: true
      - name: (0043, GEMS_PARM_01, 27)
        hash: true
      - name: (0045, GEMS_HELIOS_01, 01)
        jitter: true
      - name: (0045, GEMS_HELIOS_01, 02)
        jitter: true
      - name: (0903, GEIIS PACS, 10)
        jitter: true
      - name: (0903, GEIIS PACS, 11)
        jitter: true
      - name: (0903, GEIIS PACS, 12)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 01)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 02)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 03)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 04)
        remove: true
      - name: (2001, Philips Imaging DD 001, 05)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 06)
        remove: true
      - name: (2001, Philips Imaging DD 001, 07)
        remove: true
      - name: (2001, Philips Imaging DD 001, 08)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 09)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 0a)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 0b)
        remove: true
      - name: (2001, Philips Imaging DD 001, 0c)
        remove: true
      - name: (2001, Philips Imaging DD 001, 0e)
        remove: true
      - name: (2001, Philips Imaging DD 001, 0f)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 10)
        remove: true
      - name: (2001, Philips Imaging DD 001, 11)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 12)
        remove: true
      - name: (2001, Philips Imaging DD 001, 13)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 14)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 15)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 16)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 17)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 18)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 19)
        remove: true
      - name: (2001, Philips Imaging DD 001, 1a)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 1b)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 1c)
        remove: true
      - name: (2001, Philips Imaging DD 001, 1d)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 1e)
        remove: true
      - name: (2001, Philips Imaging DD 001, 1f)
        remove: true
      - name: (2001, Philips Imaging DD 001, 21)
        remove: true
      - name: (2001, Philips Imaging DD 001, 22)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 23)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 24)
        remove: true
      - name: (2001, Philips Imaging DD 001, 25)
        hash: true
      - name: (2001, Philips Imaging DD 001, 26)
        remove: true
      - name: (2001, Philips Imaging DD 001, 27)
        remove: true
      - name: (2001, Philips Imaging DD 001, 28)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 29)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 2a)
        remove: true
      - name: (2001, Philips Imaging DD 001, 2b)
        remove: true
      - name: (2001, Philips Imaging DD 001, 2c)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 2d)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 2e)
        remove: true
      - name: (2001, Philips Imaging DD 001, 2f)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 30)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 31)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 32)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 33)
        remove: true
      - name: (2001, Philips Imaging DD 001, 34)
        remove: true
      - name: (2001, Philips Imaging DD 001, 35)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 36)
        remove: true
      - name: (2001, Philips Imaging DD 001, 37)
        remove: true
      - name: (2001, Philips Imaging DD 001, 38)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 39)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 3a)
        remove: true
      - name: (2001, Philips Imaging DD 001, 3b)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 3c)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 3d)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 3e)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 3f)
        remove: true
      - name: (2001, Philips Imaging DD 001, 40)
        remove: true
      - name: (2001, Philips Imaging DD 001, 41)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 42)
        remove: true
      - name: (2001, Philips Imaging DD 001, 43)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 44)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 45)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 46)
        remove: true
      - name: (2001, Philips Imaging DD 001, 47)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 48)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 49)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 4a)
        remove: true
      - name: (2001, Philips Imaging DD 001, 4b)
        remove: true
      - name: (2001, Philips Imaging DD 001, 4c)
        remove: true
      - name: (2001, Philips Imaging DD 001, 4d)
        remove: true
      - name: (2001, Philips Imaging DD 001, 4e)
        remove: true
      - name: (2001, Philips Imaging DD 001, 4f)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 50)
        hash: true
      - name: (2001, Philips Imaging DD 001, 51)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 53)
        remove: true
      - name: (2001, Philips Imaging DD 001, 54)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 55)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 56)
        remove: true
      - name: (2001, Philips Imaging DD 001, 57)
        remove: true
      - name: (2001, Philips Imaging DD 001, 58)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 59)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 5a)
        hash: true
      - name: (2001, Philips Imaging DD 001, 5d)
        hash: true
      - name: (2001, Philips Imaging DD 001, 5e)
        hash: true
      - name: (2001, Philips Imaging DD 001, 5f)
        remove: true
      - name: (2001, Philips Imaging DD 001, 60)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 61)
        remove: true
      - name: (2001, Philips Imaging DD 001, 62)
        remove: true
      - name: (2001, Philips Imaging DD 001, 63)
        remove: true
      - name: (2001, Philips Imaging DD 001, 64)
        hash: true
      - name: (2001, Philips Imaging DD 001, 65)
        remove: true
      - name: (2001, Philips Imaging DD 001, 66)
        remove: true
      - name: (2001, Philips Imaging DD 001, 67)
        remove: true
      - name: (2001, Philips Imaging DD 001, 68)
        remove: true
      - name: (2001, Philips Imaging DD 001, 69)
        remove: true
      - name: (2001, Philips Imaging DD 001, 6a)
        remove: true
      - name: (2001, Philips Imaging DD 001, 6b)
        remove: true
      - name: (2001, Philips Imaging DD 001, 6d)
        hash: true
      - name: (2001, Philips Imaging DD 001, 6e)
        hash: true
      - name: (2001, Philips Imaging DD 001, 6f)
        remove: true
      - name: (2001, Philips Imaging DD 001, 71)
        remove: true
      - name: (2001, Philips Imaging DD 001, 72)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 73)
        remove: true
      - name: (2001, Philips Imaging DD 001, 74)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 75)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 76)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 77)
        remove: true
      - name: (2001, Philips Imaging DD 001, 79)
        remove: true
      - name: (2001, Philips Imaging DD 001, 7a)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 7b)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 7c)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 7d)
        hash: true
      - name: (2001, Philips Imaging DD 001, 7e)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 7f)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 80)
        hash: true
      - name: (2001, Philips Imaging DD 001, 81)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 82)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 83)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 84)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 85)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 86)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 87)
        hash: true
      - name: (2001, Philips Imaging DD 001, 88)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 89)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 8a)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 8b)
        hash: true
      - name: (2001, Philips Imaging DD 001, 8c)
        remove: true
      - name: (2001, Philips Imaging DD 001, 90)
        hash: true
      - name: (2001, Philips Imaging DD 001, 91)
        hash: true
      - name: (2001, Philips Imaging DD 001, 92)
        hash: true
      - name: (2001, Philips Imaging DD 001, 93)
        hash: true
      - name: (2001, Philips Imaging DD 001, 94)
        hash: true
      - name: (2001, Philips Imaging DD 001, 9a)
        remove: true
      - name: (2001, Philips Imaging DD 001, 9b)
        jitter: true
      - name: (2001, Philips Imaging DD 001, 9d)
        hash: true
      - name: (2001, Philips Imaging DD 001, 9f)
        jitter: true
      - name: (2001, Philips Imaging DD 001, a1)
        remove: true
      - name: (2001, Philips Imaging DD 001, a2)
        jitter: true
      - name: (2001, Philips Imaging DD 001, a3)
        jitter: true
      - name: (2001, Philips Imaging DD 001, a4)
        jitter: true
      - name: (2001, Philips Imaging DD 001, a5)
        jitter: true
      - name: (2001, Philips Imaging DD 001, c0)
        jitter: true
      - name: (2001, Philips Imaging DD 001, c1)
        hash: true
      - name: (2001, Philips Imaging DD 001, c2)
        jitter: true
      - name: (2001, Philips Imaging DD 001, c3)
        jitter: true
      - name: (2001, Philips Imaging DD 001, c5)
        remove: true
      - name: (2001, Philips Imaging DD 001, c6)
        remove: true
      - name: (2001, Philips Imaging DD 001, c7)
        remove: true
      - name: (2001, Philips Imaging DD 001, ca)
        remove: true
      - name: (2001, Philips Imaging DD 001, cb)
        jitter: true
      - name: (2001, Philips Imaging DD 001, d0)
        remove: true
      - name: (2001, Philips Imaging DD 001, d1)
        jitter: true
      - name: (2001, Philips Imaging DD 001, d2)
        jitter: true
      - name: (2001, Philips Imaging DD 001, d3)
        jitter: true
      - name: (2001, Philips Imaging DD 001, d4)
        jitter: true
      - name: (2001, Philips Imaging DD 001, d5)
        jitter: true
      - name: (2001, Philips Imaging DD 001, d6)
        jitter: true
      - name: (2001, Philips Imaging DD 001, d7)
        jitter: true
      - name: (2001, Philips Imaging DD 001, d8)
        jitter: true
      - name: (2001, Philips Imaging DD 001, d9)
        jitter: true
      - name: (2001, Philips Imaging DD 001, da)
        remove: true
      - name: (2001, Philips Imaging DD 001, db)
        jitter: true
      - name: (2001, Philips Imaging DD 001, dc)
        jitter: true
      - name: (2001, Philips Imaging DD 001, dd)
        jitter: true
      - name: (2001, Philips Imaging DD 001, de)
        jitter: true
      - name: (2001, Philips Imaging DD 001, df)
        jitter: true
      - name: (2001, Philips Imaging DD 001, e9)
        remove: true
      - name: (2001, Philips Imaging DD 001, f1)
        jitter: true
      - name: (2001, Philips Imaging DD 001, f2)
        jitter: true
      - name: (2001, Philips Imaging DD 001, f3)
        remove: true
      - name: (2001, Philips Imaging DD 001, f4)
        jitter: true
      - name: (2001, Philips Imaging DD 001, f5)
        remove: true
      - name: (2001, Philips Imaging DD 001, f6)
        jitter: true
      - name: (2001, Philips Imaging DD 001, f7)
        hash: true
      - name: (2001, Philips Imaging DD 001, f9)
        remove: true
      - name: (2001, Philips Imaging DD 001, fb)
        remove: true
      - name: (2001, Philips Imaging DD 001, fc)
        remove: true
      - name: (2001, Philips Imaging DD 001, fd)
        remove: true
      - name: (2001, Philips Imaging DD 001, ff)
        hash: true
      - name: (2001, Philips Imaging DD 002, 01)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 02)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 03)
        remove: true
      - name: (2001, Philips Imaging DD 002, 04)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 05)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 06)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 07)
        remove: true
      - name: (2001, Philips Imaging DD 002, 08)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 09)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 0a)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 0b)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 0c)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 0d)
        remove: true
      - name: (2001, Philips Imaging DD 002, 0e)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 0f)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 10)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 11)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 12)
        remove: true
      - name: (2001, Philips Imaging DD 002, 13)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 14)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 15)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 16)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 17)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 18)
        remove: true
      - name: (2001, Philips Imaging DD 002, 19)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 1a)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 1b)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 1c)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 1d)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 1e)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 1f)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 20)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 21)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 22)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 23)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 24)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 25)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 26)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 27)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 28)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 29)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 2a)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 2b)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 2c)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 2d)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 2e)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 2f)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 30)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 31)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 32)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 33)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 34)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 35)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 36)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 37)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 38)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 39)
        remove: true
      - name: (2001, Philips Imaging DD 002, 3a)
        remove: true
      - name: (2001, Philips Imaging DD 002, 3b)
        remove: true
      - name: (2001, Philips Imaging DD 002, 3c)
        remove: true
      - name: (2001, Philips Imaging DD 002, 3d)
        remove: true
      - name: (2001, Philips Imaging DD 002, 3e)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 3f)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 40)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 50)
        remove: true
      - name: (2001, Philips Imaging DD 002, 52)
        hash: true
      - name: (2001, Philips Imaging DD 002, 53)
        remove: true
      - name: (2001, Philips Imaging DD 002, 57)
        remove: true
      - name: (2001, Philips Imaging DD 002, 58)
        remove: true
      - name: (2001, Philips Imaging DD 002, 5a)
        hash: true
      - name: (2001, Philips Imaging DD 002, 5c)
        hash: true
      - name: (2001, Philips Imaging DD 002, 5d)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 5e)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 5f)
        remove: true
      - name: (2001, Philips Imaging DD 002, 63)
        hash: true
      - name: (2001, Philips Imaging DD 002, 64)
        remove: true
      - name: (2001, Philips Imaging DD 002, 65)
        remove: true
      - name: (2001, Philips Imaging DD 002, 66)
        remove: true
      - name: (2001, Philips Imaging DD 002, 67)
        hash: true
      - name: (2001, Philips Imaging DD 002, 68)
        hash: true
      - name: (2001, Philips Imaging DD 002, 6b)
        hash: true
      - name: (2001, Philips Imaging DD 002, 71)
        remove: true
      - name: (2001, Philips Imaging DD 002, 72)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 73)
        jitter: true
      - name: (2001, Philips Imaging DD 002, 74)
        hash: true
      - name: (2001, Philips Imaging DD 002, 75)
        jitter: true
      - name: (2001, Philips Imaging DD 097, 01)
        remove: true
      - name: (2001, Philips Imaging DD 097, 02)
        jitter: true
      - name: (2001, Philips Imaging DD 097, 03)
        jitter: true
      - name: (2001, Philips Imaging DD 097, 04)
        jitter: true
      - name: (2001, Philips Imaging DD 097, 05)
        remove: true
      - name: (2001, Philips Imaging DD 097, 06)
        remove: true
      - name: (2001, Philips Imaging DD 097, 07)
        jitter: true
      - name: (2001, Philips Imaging DD 097, 08)
        remove: true
      - name: (2001, Philips Imaging DD 097, 0a)
        remove: true
      - name: (2001, Philips Imaging DD 097, 0b)
        remove: true
      - name: (2001, Philips Imaging DD 097, 0d)
        jitter: true
      - name: (2001, Philips Imaging DD 097, 0e)
        remove: true
      - name: (2001, Philips Imaging DD 097, 0f)
        jitter: true
      - name: (2001, Philips Imaging DD 097, 12)
        remove: true
      - name: (2001, Philips Imaging DD 097, 17)
        jitter: true
      - name: (2001, Philips Imaging DD 097, 18)
        jitter: true
      - name: (2001, Philips Imaging DD 097, 19)
        hash: true
      - name: (2001, Philips Imaging DD 097, 1a)
        jitter: true
      - name: (2001, Philips Imaging DD 097, 1b)
        jitter: true
      - name: (2001, Philips Imaging DD 097, 1c)
        jitter: true
      - name: (2001, Philips Imaging DD 097, 1d)
        jitter: true
      - name: (2001, Philips Imaging DD 097, 1e)
        jitter: true
      - name: (2001, Philips Imaging DD 097, 1f)
        jitter: true
      - name: (2001, Philips Imaging DD 097, 21)
        jitter: true
      - name: (2001, Philips Imaging DD 097, 22)
        jitter: true
      - name: (2001, Philips Imaging DD 097, 23)
        jitter: true
      - name: (2001, Philips Imaging DD 097, 24)
        jitter: true
      - name: (2001, Philips Imaging DD 097, 25)
        jitter: true
      - name: (2001, Philips Imaging DD 097, 26)
        jitter: true
      - name: (2001, Philips Imaging DD 097, 27)
        jitter: true
      - name: (2001, Philips Imaging DD 097, 28)
        hash: true
      - name: (2001, Philips Imaging DD 097, 2a)
        hash: true
      - name: (2001, Philips Imaging DD 097, 2b)
        jitter: true
      - name: (2001, Philips Imaging DD 097, 2c)
        jitter: true
      - name: (2001, Philips Imaging DD 097, 2d)
        jitter: true
      - name: (2001, Philips Imaging DD 097, 2e)
        remove: true
      - name: (2001, Philips Imaging DD 097, 2f)
        jitter: true
      - name: (2001, Philips Imaging DD 097, 30)
        jitter: true
      - name: (2001, Philips Imaging DD 097, 31)
        jitter: true
      - name: (2001, Philips Imaging DD 097, 32)
        jitter: true
      - name: (2001, Philips Imaging DD 097, 36)
        hash: true
      - name: (2001, Philips Imaging DD 097, 37)
        jitter: true
      - name: (2001, Philips Imaging DD 097, 38)
        jitter: true
      - name: (2001, Philips Imaging DD 097, 39)
        jitter: true
      - name: (2001, Philips Imaging DD 097, 3a)
        jitter: true
      - name: (2001, Philips Imaging DD 097, 3b)
        jitter: true
      - name: (2001, Philips Imaging DD 097, 3c)
        remove: true
      - name: (2001, Philips Imaging DD 097, 3d)
        jitter: true
      - name: (2001, Philips Imaging DD 097, 3e)
        remove: true
      - name: (2001, Philips Imaging DD 097, 40)
        jitter: true
      - name: (2001, Philips Imaging DD 097, 41)
        hash: true
      - name: (2001, Philips Imaging DD 097, 42)
        remove: true
      - name: (2001, Philips Imaging DD 097, 44)
        hash: true
      - name: (2001, Philips Imaging DD 097, 45)
        jitter: true
      - name: (2001, Philips Imaging DD 097, 46)
        hash: true
      - name: (2001, Philips Imaging DD 097, 47)
        jitter: true
      - name: (2001, Philips Imaging DD 097, 49)
        remove: true
      - name: (2001, Philips Imaging DD 097, 4a)
        jitter: true
      - name: (2001, Philips Imaging DD 097, 4b)
        jitter: true
      - name: (2001, Philips Imaging DD 097, 4c)
        hash: true
      - name: (2001, Philips Imaging DD 097, a1)
        remove: true
      - name: (2001, Philips Imaging DD 097, a2)
        jitter: true
      - name: (2001, Philips Imaging DD 097, a3)
        jitter: true
      - name: (2001, Philips Imaging DD 097, a4)
        jitter: true
      - name: (2001, Philips Imaging DD 097, a5)
        jitter: true
      - name: (2001, Philips Imaging DD 097, a6)
        jitter: true
      - name: (2001, Philips Imaging DD 097, a8)
        jitter: true
      - name: (2001, Philips Imaging DD 097, a9)
        remove: true
      - name: (2001, Philips Imaging DD 097, aa)
        jitter: true
      - name: (2001, Philips Imaging DD 097, ab)
        jitter: true
      - name: (2001, Philips Imaging DD 129, 00)
        remove: true
      - name: (2001, Philips Imaging DD 129, 01)
        remove: true
      - name: (2001, Philips Imaging DD 129, 02)
        remove: true
      - name: (2001, Philips Imaging DD 129, 03)
        jitter: true
      - name: (2001, Philips Imaging DD 129, 04)
        remove: true
      - name: (2001, Philips Imaging DD 129, 05)
        jitter: true
      - name: (2001, Philips Imaging DD 129, 06)
        jitter: true
      - name: (2001, Philips Imaging DD 129, 07)
        remove: true
      - name: (2001, Philips Imaging DD 129, 08)
        remove: true
      - name: (2001, Philips Imaging DD 129, 09)
        jitter: true
      - name: (0025, Philips ST80i, 01)
        hash: true
      - name: (0019, SIEMENS MR HEADER, 0C)
        jitter: true
      - name: (0019, SIEMENS MR HEADER, 0D)
        remove: true
      - name: (0019, SIEMENS MR HEADER, 0E)
        jitter: true
      - name: (0019, SIEMENS MR HEADER, 27)
        jitter: true
      - name: (0043, GEMS_PARM_01, 39)
        jitter: true
      - name: (0043, GEMS_PARM_01, 6F)
        jitter: true
      - name: (0025, GEMS_SERS_01, 07)
        jitter: true
      - name: (7E01, HOLOGIC, Inc., 01)
        hash: true
      - name: (7E01, HOLOGIC, Inc., 02)
        hash: true
      - name: (7E01, HOLOGIC, Inc., 10)
        remove: true
      - name: (7E01, HOLOGIC, Inc., 11)
        remove: true
      - name: (7E01, HOLOGIC, Inc., 12)
        jitter: true
      - name: (0099, NQHeader, 01)
        remove: true
      - name: (0099, NQHeader, 02)
        remove: true
      - name: (0099, NQHeader, 04)
        jitter: true
      - name: (0099, NQHeader, 05)
        hash: true
      - name: (0099, NQHeader, 10)
        jitter: true
      - name: (0099, NQHeader, 20)
        hash: true
      - name: (0099, NQHeader, 21)
        jitter: true
      - name: (0199, NQLeft, 01)
        jitter: true
      - name: (0199, NQLeft, 02)
        jitter: true
      - name: (0199, NQLeft, 03)
        jitter: true
      - name: (0199, NQLeft, 04)
        jitter: true
      - name: (0199, NQLeft, 05)
        jitter: true
      - name: (0199, NQLeft, 06)
        jitter: true
      - name: (0199, NQLeft, 07)
        jitter: true
      - name: (0199, NQLeft, 08)
        jitter: true
      - name: (0199, NQLeft, 09)
        jitter: true
      - name: (0199, NQLeft, 0a)
        jitter: true
      - name: (0199, NQLeft, 0b)
        jitter: true
      - name: (0199, NQLeft, 0c)
        jitter: true
      - name: (0199, NQLeft, 0d)
        jitter: true
      - name: (0199, NQLeft, 0e)
        jitter: true
      - name: (0199, NQLeft, 0f)
        jitter: true
      - name: (0199, NQLeft, 10)
        jitter: true
      - name: (0199, NQLeft, 11)
        jitter: true
      - name: (0199, NQLeft, 12)
        jitter: true
      - name: (0199, NQLeft, 13)
        jitter: true
      - name: (0199, NQLeft, 14)
        jitter: true
      - name: (0199, NQLeft, 15)
        jitter: true
      - name: (0199, NQLeft, 16)
        jitter: true
      - name: (0299, NQRight, 01)
        jitter: true
      - name: (0299, NQRight, 02)
        jitter: true
      - name: (0299, NQRight, 03)
        jitter: true
      - name: (0299, NQRight, 04)
        jitter: true
      - name: (0299, NQRight, 05)
        jitter: true
      - name: (0299, NQRight, 06)
        jitter: true
      - name: (0299, NQRight, 07)
        jitter: true
      - name: (0299, NQRight, 08)
        jitter: true
      - name: (0299, NQRight, 09)
        jitter: true
      - name: (0299, NQRight, 0a)
        jitter: true
      - name: (0299, NQRight, 0b)
        jitter: true
      - name: (0299, NQRight, 0c)
        jitter: true
      - name: (0299, NQRight, 0d)
        jitter: true
      - name: (0299, NQRight, 0e)
        jitter: true
      - name: (0299, NQRight, 0f)
        jitter: true
      - name: (0299, NQRight, 10)
        jitter: true
      - name: (0299, NQRight, 11)
        jitter: true
      - name: (0299, NQRight, 12)
        jitter: true
      - name: (0299, NQRight, 13)
        jitter: true
      - name: (0299, NQRight, 14)
        jitter: true
      - name: (0299, NQRight, 15)
        jitter: true
      - name: (0299, NQRight, 16)
        jitter: true
      - name: (2005, Philips MR Imaging DD 001, 0D)
        jitter: true
      - name: (2005, Philips MR Imaging DD 001, 0E)
        jitter: true
      - name: (0119, SIEMENS Ultrasound SC2000, 00)
        hash: true
      - name: (0119, SIEMENS Ultrasound SC2000, 01)
        jitter: true
      - name: (0119, SIEMENS Ultrasound SC2000, 02)
        remove: true
      - name: (0119, SIEMENS Ultrasound SC2000, 03)
        remove: true
      - name: (0119, SIEMENS Ultrasound SC2000, 04)
        jitter: true
      - name: (0119, SIEMENS Ultrasound SC2000, 05)
        jitter: true
      - name: (0119, SIEMENS Ultrasound SC2000, 06)
        jitter: true
      - name: (0119, SIEMENS Ultrasound SC2000, 07)
        jitter: true
      - name: (0119, SIEMENS Ultrasound SC2000, 08)
        jitter: true
      - name: (0119, SIEMENS Ultrasound SC2000, 09)
        jitter: true
      - name: (0119, SIEMENS Ultrasound SC2000, 10)
        hash: true
      - name: (0119, SIEMENS Ultrasound SC2000, 11)
        remove: true
      - name: (0119, SIEMENS Ultrasound SC2000, 12)
        remove: true
      - name: (0119, SIEMENS Ultrasound SC2000, 13)
        jitter: true
      - name: (0119, SIEMENS Ultrasound SC2000, 21)
        hash: true
      - name: (0129, SIEMENS Ultrasound SC2000, 00)
        remove: true
      - name: (0129, SIEMENS Ultrasound SC2000, 02)
        remove: true
      - name: (0129, SIEMENS Ultrasound SC2000, 03)
        remove: true
      - name: (0129, SIEMENS Ultrasound SC2000, 04)
        remove: true
      - name: (0129, SIEMENS Ultrasound SC2000, 05)
        remove: true
      - name: (0129, SIEMENS Ultrasound SC2000, 06)
        remove: true
      - name: (0129, SIEMENS Ultrasound SC2000, 07)
        remove: true
      - name: (0129, SIEMENS Ultrasound SC2000, 08)
        jitter: true
      - name: (0129, SIEMENS Ultrasound SC2000, 09)
        remove: true
      - name: (0129, SIEMENS Ultrasound SC2000, 10)
        jitter: true
      - name: (0129, SIEMENS Ultrasound SC2000, 11)
        remove: true
      - name: (0129, SIEMENS Ultrasound SC2000, 12)
        remove: true
      - name: (0129, SIEMENS Ultrasound SC2000, 20)
        remove: true
      - name: (0129, SIEMENS Ultrasound SC2000, 21)
        jitter: true
      - name: (0129, SIEMENS Ultrasound SC2000, 22)
        jitter: true
      - name: (0129, SIEMENS Ultrasound SC2000, 29)
        jitter: true
      - name: (0129, SIEMENS Ultrasound SC2000, 30)
        remove: true
      - name: (0139, SIEMENS Ultrasound SC2000, 01)
        jitter: true
      - name: (0149, SIEMENS Ultrasound SC2000, 01)
        jitter: true
      - name: (0149, SIEMENS Ultrasound SC2000, 02)
        jitter: true
      - name: (0149, SIEMENS Ultrasound SC2000, 03)
        jitter: true
      - name: (7FD1, SIEMENS Ultrasound SC2000, 01)
        jitter: true
      - name: (7FD1, SIEMENS Ultrasound SC2000, 09)
        remove: true
      - name: (7FD1, SIEMENS Ultrasound SC2000, 10)
        jitter: true
      - name: (7FD1, SIEMENS Ultrasound SC2000, 11)
        jitter: true
      - name: (7FD1, SIEMENS SYNGO ULTRA-SOUND TOYON DATA STREAMING, 01)
        jitter: true
      - name: (7FD1, SIEMENS SYNGO ULTRA-SOUND TOYON DATA STREAMING, 09)
        remove: true
      - name: (7FD1, SIEMENS SYNGO ULTRA-SOUND TOYON DATA STREAMING, 10)
        jitter: true
      - name: (7FD1, SIEMENS SYNGO ULTRA-SOUND TOYON DATA STREAMING, 11)
        jitter: true
