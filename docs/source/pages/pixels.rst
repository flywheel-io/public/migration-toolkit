.. _pixel-scrubbing-page:

Pixel Scrubbing
===============

.. note::
   Currently only `DICOM` is supported for pixel scrubbing. If you want
   to use the pixel scrubbing capability, you need to make sure that the
   `pydicom deid <https://pydicom.github.io/deid/>`_ package is installed
   in your environment.  This can be done by running ``pip install deid`` or
   ``pip install flywheel-sdk[pixel]``

Often there will be burned in PHI in the pixels of an image, i.e. Patient Name,
DOB, MRN number.  This section of the documentation covers how the
migration-toolkit can be used to black out or keep a section of pixels



DICOM
^^^^^

DICOM pixel scrubbing is done via the `pydicom deid <https://pydicom.github.io/deid/>`_ module.

To enable pixel alteration, the top level key ``alter_pixels`` must be used.
Under the ``alter_pixels`` key, there are two setting keys that are relavent:
``detect`` and ``pixels``.

Detect
------
``detect`` uses the
`built-in rules from pydicom <https://github.com/pydicom/deid/tree/master/deid/data>`_ to flag
and remove certain known burned-in PHI. ``detect`` accepts a list of strings that correspond to
filenames in the above folder from the upstream repository.

Pixels
------
``pixels`` allows specifying custom regions and conditions to keep or remove PHI.
``pixels`` accepts a list of dictionaries with two keys

1. either ``keep`` or ``remove`` which takes a list of coordinates in pixel space as a value:

.. code-block:: yaml

	keep: [x0, y0, x1, y1]

The ``keep`` action will remove everything except for the region its value defines,
whereas the ``remove`` action will keep everything except for the region its value
defines.

2. ``when`` which takes a string as a value.  This string follows the syntax of
``filters`` in the deid package

These filters take on the general form of ``<criteria> <field> <value>`` and can
be chained together with ands (``+``) or ors (``||``)

For example, the following filter would match a dicom that has an ImageType with
the value ``SAVE``, a SeriesDescription with ``SAVE`` in the string, and a value
of ``YES`` for the BurnedInAnnotation tag :

.. code-block:: bash

  contains ImageType SAVE
  + contains SeriesDescription SAVE
  + contains BurnedInAnnotation YES

See more on the details of the syntax in the deid package:
`recipe filters <https://pydicom.github.io/deid/user-docs/recipe-filters/>`_.


Example
-------

.. WARNING::
   Configuration of pixel-scrubbing including conditions and coordinates is up
   to the user.  The included rules here are NOT thorough.


.. code-block:: yaml

    dicom:
      alter_pixels:
        detect:
          - dicom
          - dicom.xray.chest

        pixels:
          - keep: [0, 0, 10, 10]
            when: |
              contains Modality CT
                + contains Manufacturer GE

          - remove: [20, 20, 30, 30]
            when: |
              contains SeriesDescription Dose Report
