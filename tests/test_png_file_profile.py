import logging
import os
import shutil

import png
import pytest
import six
from fs.osfs import OSFS
from PIL import UnidentifiedImageError

from flywheel_migration import deidentify
from flywheel_migration.deidentify.deid_field import DeIdField
from flywheel_migration.deidentify.file_profile import FileProfile
from flywheel_migration.deidentify.png_file_profile import PNGFileProfile, PNGRecord


def test_png_file_profile(png_file):
    # Verify that subclass is registered
    file_profile_names = FileProfile.profile_names()
    assert len(file_profile_names) > 0
    assert "png" in file_profile_names

    path = png_file("exif", "sample1.png")
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)
    with test_fs.open(basename, "rb") as fd:
        reader = png.Reader(file=fd)
        metadata = list(reader.chunks())
        assert len([ch for ch in metadata if ch[0] == b"eXIf"]) > 0
        assert len([ch for ch in metadata if ch[0] == b"tEXt"]) > 0

    profile = PNGFileProfile()
    profile.add_field(DeIdField.factory({"name": "eXIf", "remove": True}))
    profile.add_field(DeIdField.factory({"name": "tEXt", "remove": True}))

    assert profile.matches_packfile("png")

    profile.process_files(test_fs, test_fs, [basename])

    with test_fs.open(basename, "rb") as fd:
        reader = png.Reader(file=fd)
        metadata = list(reader.chunks())
        assert len([ch for ch in metadata if ch[0] == b"eXIf"]) == 0
        assert len([ch for ch in metadata if ch[0] == b"tEXt"]) == 0


def test_png_file_profile_raises_on_replace(png_file):
    path = png_file("exif", "sample1.png")
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    profile = PNGFileProfile()
    profile.add_field(DeIdField.factory({"name": "eXIf", "replace-with": "TEST"}))

    with pytest.raises(NotImplementedError):
        profile.process_files(test_fs, test_fs, [basename])


def test_png_file_profile_validate(png_file, caplog):
    profile = PNGFileProfile()
    profile.add_field(DeIdField.factory({"name": "eXIf", "hash": True}))
    profile.add_field(DeIdField.factory({"name": "eXIf", "hashuid": True}))
    profile.add_field(DeIdField.factory({"name": "eXIf", "increment-date": True}))
    profile.add_field(DeIdField.factory({"name": "eXIf", "increment-datetime": True}))
    profile.add_field(DeIdField.factory({"name": "eXIf", "replace-with": "TEST"}))
    errors = profile.validate()
    assert any(
        [
            error == "Action is not currently supported for this profile: hash"
            for error in errors
        ]
    )
    assert any(
        [
            error == "Action is not currently supported for this profile: hashuid"
            for error in errors
        ]
    )
    assert any(
        [
            error
            == "Action is not currently supported for this profile: increment-date"
            for error in errors
        ]
    )
    assert any(
        [
            error
            == "Action is not currently supported for this profile: increment-datetime"
            for error in errors
        ]
    )
    assert any(
        [
            error == "Action is not currently supported for this profile: replace-with"
            for error in errors
        ]
    )

    profile = PNGFileProfile()
    profile.add_field(DeIdField.factory({"name": "orNT", "remove": True}))
    with caplog.at_level(logging.INFO):
        _ = profile.validate()
    assert "orNT is a private chunk" in caplog.messages[0]

    profile = PNGFileProfile()
    profile.add_field(DeIdField.factory({"name": "eXIf", "remove": True}))
    profile.validate()


def test_png_file_profile_can_remove_private_chunks(png_file):
    path = png_file("exif", "sample1.png")
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)
    with test_fs.open(basename, "rb") as fd:
        reader = png.Reader(file=fd)
        metadata = list(reader.chunks())
        # private chunks are defined by a chunk type 2 letter being lower case (e.g. 'orNT')
        assert len([ch for ch in metadata if ch[0].decode()[1].islower()]) > 0

    profile = PNGFileProfile()
    profile.remove_private_chunks = True

    profile.process_files(test_fs, test_fs, [basename])

    with test_fs.open(basename, "rb") as fd:
        reader = png.Reader(file=fd)
        metadata = list(reader.chunks())
        assert len([ch for ch in metadata if ch[0].decode()[1].islower()]) == 0


def test_png_record_raised_if_not_png_file(dicom_file, png_file, jpg_file):
    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    with pytest.raises(UnidentifiedImageError):
        with open(path, "rb") as fp:
            PNGRecord(fp)

    path = jpg_file("camera", "DSCN0010_with_gps.jpg")
    with pytest.raises(TypeError):
        with open(path, "rb") as fp:
            PNGRecord(fp)


def test_png_profile_log_if_not_loading_png(dicom_file, caplog):
    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)
    profile = PNGFileProfile()
    with caplog.at_level(logging.INFO):
        profile.load_record({}, test_fs, basename)
        assert "IGNORING" in caplog.messages[0]


def test_png_profile_can_load_template(png_file, template_file):
    path = png_file("exif", "sample1.png")
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    profile = deidentify.load_deid_profile(template_file("png-profile"))
    profile.process_packfile("png", test_fs, test_fs, [basename])

    with test_fs.open(basename, "rb") as fd:
        reader = png.Reader(file=fd)
        metadata = list(reader.chunks())
        assert len([ch for ch in metadata if ch[0] == b"eXIf"]) == 0
        assert len([ch for ch in metadata if ch[0] == b"tEXt"]) == 0
        assert len([ch for ch in metadata if ch[0].decode()[1].islower()]) == 0


def test_png_profile_can_run_without_touching_metadata(png_file):
    path = png_file("exif", "sample1.png")
    dirname, _ = os.path.split(path)
    basename = "1.2.276.0.75.2.2.40.33166211806.201002051135279530.png"
    shutil.copy(path, os.path.join(dirname, basename))
    test_fs = OSFS(dirname)
    profile = PNGFileProfile()

    config = {
        "filenames": [
            {
                "output": "{group1}.png",
                "input-regex": r"^(?P<group1>[\d\.]+).png$",
                "groups": [{"name": "group1", "hashuid": True}],
            }
        ]
    }
    profile.load_config(config)
    profile.process_files(test_fs, test_fs, [basename])

    # hashuid is 1.2.276.0.131163.531882.440130.237143.225259.182160.279530
    expected_filename = "1.2.276.0.131163.531882.440130.237143.225259.182160.279530.png"
    assert os.path.exists(os.path.join(dirname, expected_filename))


def test_png_profile_matches_file():
    profile = PNGFileProfile()
    assert profile.matches_file("test.PNG")
    assert profile.matches_file("test.png")
    assert not profile.matches_file("test.jpg")


def test_png_matches_byte_sig(efile, png_file):
    # Test png
    profile = PNGFileProfile()
    path = png_file("exif", "sample1.png")
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)
    assert profile.matches_byte_sig(test_fs, basename)

    # Test non-png
    path = efile
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)
    assert not profile.matches_byte_sig(test_fs, basename)
