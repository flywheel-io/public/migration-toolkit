import os
import tempfile

import nibabel as nib
import pytest
import six
from fs.osfs import OSFS

from flywheel_migration import deidentify
from flywheel_migration.deidentify.deid_field import DeIdField
from flywheel_migration.deidentify.file_profile import FileProfile
from flywheel_migration.deidentify.nifti_file_profile import (
    NiftiFileProfile,
    NiftiRecord,
)


def test_nifti_file_profile(nifti_file):
    # Verify that subclass is registered
    file_profile_names = FileProfile.profile_names()
    assert len(file_profile_names) > 0
    assert "nifti" in file_profile_names

    path = nifti_file("nifti", "example.nii.gz")
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    profile = NiftiFileProfile()
    profile.add_field(DeIdField.factory({"name": "descrip", "remove": True}))
    profile.add_field(
        DeIdField.factory({"name": "aux_file", "replace-with": "REDACTED"})
    )
    profile.add_field(DeIdField.factory({"name": "slice_duration", "remove": True}))

    assert profile.matches_packfile("nifti")
    sys_path = test_fs.getsyspath(basename)
    nifti = nib.load(sys_path)
    assert nifti.header["descrip"].item() != b""
    assert nifti.header["aux_file"].item() != b"REDACTED"
    assert nifti.header["slice_duration"].item() != 0.0

    profile.process_files(test_fs, test_fs, [basename])

    sys_path = test_fs.getsyspath(basename)
    nifti = nib.load(sys_path)
    assert nifti.header["descrip"].item() == b""
    assert nifti.header["aux_file"].item() == b"REDACTED"
    assert nifti.header["slice_duration"].item() == 0.0


def test_nifti_record_raises_error_if_not_nifti_file(nifti_file):
    path = nifti_file("not_a_nifti", "sample1.png")
    with pytest.raises(TypeError):
        with open(path, "rb") as fp:
            NiftiRecord(fp)


def test_nifti_record_can_save_nifti_file(nifti_file):
    path = nifti_file("nifti", "example.nii.gz")
    record = NiftiRecord(path)

    with tempfile.NamedTemporaryFile(suffix=".nii.gz") as tmpfile:
        record.save_as(tmpfile.name)

        assert os.path.exists(tmpfile.name)
        assert os.path.getsize(tmpfile.name)

        # can be reopened
        NiftiRecord(tmpfile.name)


def test_nifti_profile_validate_returns_errors_if_field_invalid(nifti_file):
    profile = NiftiFileProfile()
    profile.add_field(DeIdField.factory({"name": "Undefined", "replace-with": "None"}))
    profile.add_field(DeIdField.factory({"name": "Fake", "remove": True}))
    errors = profile.validate()
    assert len(errors) == 2


def test_nifti_profile_can_load_template(nifti_file, template_file):
    path = nifti_file("nifti", "example.nii.gz")
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    profile = deidentify.load_deid_profile(template_file("nifti-profile"))
    profile.process_packfile("nifti", test_fs, test_fs, [basename])

    sys_path = test_fs.getsyspath(basename)
    nifti = nib.load(sys_path)
    assert nifti.header["descrip"].item() == b""
    assert nifti.header["aux_file"].item() == b"REDACTED"
    assert nifti.header["slice_duration"].item() == 0.0


def test_nifti_filter_file():
    profile = NiftiFileProfile()
    assert profile.matches_file("test.nii")
    assert profile.matches_file("test.nii.gz")
    assert not profile.matches_file("test.png")
