import os
import random
import secrets
from unittest.mock import MagicMock, patch

import pydicom
import pytest
import six
from fs.osfs import OSFS

from flywheel_migration import deidentify
from flywheel_migration.deidentify.deid_field import UID_MAX_LENGTH, DeIdField
from flywheel_migration.deidentify.file_profile import FileProfile


def test_regex_sub_deidfield(dicom_file, template_file):
    profile = deidentify.load_deid_profile(template_file("regex-sub-profile"))
    errors = profile.validate()
    assert not errors
    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)
    dcm = pydicom.dcmread(path)

    assert dcm.PatientID == "flywheel/reaper"
    assert dcm.PatientBirthDate == "20000101"
    assert profile.process_packfile("dicom", test_fs, test_fs, [basename])
    new_name = "1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.MR.dcm"
    dst_path = os.path.join(dirname, new_name)

    dcm = pydicom.dcmread(dst_path)
    assert dcm.PatientID == "one_cool_cat_20000118"
    assert dcm.PatientBirthDate == "20000118"
    assert profile.process_packfile("dicom", test_fs, test_fs, [new_name])
    dcm = pydicom.dcmread(dst_path)
    assert dcm.PatientID == "one_cool_cucumber"

    # test no input-regex matches raises ValueError
    with pytest.raises(ValueError) as val_exc:
        assert profile.process_packfile("dicom", test_fs, test_fs, [new_name])
    assert (
        str(val_exc.value)
        == f"Field PatientID value {dcm.PatientID} does not match any of the input-regex values"
    )

    # Test when input value is None, no action is taken
    dcm = pydicom.dcmread(path)
    del dcm["PatientID"]
    assert profile.process_packfile("dicom", test_fs, test_fs, [basename])
    assert "PatientID" not in dcm

    # test validate
    invalid_dict = {
        "name": "PatientID",
        "regex-sub": [
            {
                "input-regex": "(?P<current_value>.*)",
                "output": "{current_value}_{SeriesNumber}",
            }
        ],
    }
    invalid_field = DeIdField.factory(invalid_dict)
    for subprofile in profile.file_profiles:
        if subprofile.name == "dicom":
            subprofile.add_field(invalid_field)
    errors = profile.validate()
    assert errors


def test_jitter_deidfield(caplog):
    value = 1.3
    profile = FileProfile()
    profile.read_field = MagicMock(return_value=value)
    record = MagicMock()
    field_config = {"name": "Toto", "jitter": True}
    field = DeIdField.factory(field_config)
    new_value = field.get_value(profile, {}, record)
    assert new_value != value
    assert type(new_value) is type(value)
    assert new_value <= value + profile.jitter_range
    assert new_value >= value - profile.jitter_range

    # testing jitter_range on profile and type consistency
    value = 1.0
    profile.read_field = MagicMock(return_value=value)
    profile.jitter_range = 10
    with patch("random.uniform", return_value=0.4):
        new_value = field.get_value(profile, {}, record)
    assert new_value == value + 4
    assert type(new_value) is type(value)
    assert new_value <= value + profile.jitter_range
    assert new_value >= value - profile.jitter_range

    # testing jitter-range on field config
    field_config = {"name": "Toto", "jitter": True, "jitter-range": 100}
    field = DeIdField.factory(field_config)
    with patch("random.uniform", return_value=0.4):
        new_value = field.get_value(profile, {}, record)
    assert new_value == value + 40
    assert type(new_value) is type(value)
    assert new_value <= value + field.jitter_range
    assert new_value >= value - field.jitter_range

    # jitter with original value is 0
    profile.read_field = MagicMock(return_value=0.0)
    field_config = {"name": "Toto", "jitter": True, "jitter-range": 1}
    field = DeIdField.factory(field_config)
    new_value = field.get_value(profile, {}, record)
    assert new_value != value
    assert new_value <= value + profile.jitter_range
    assert new_value >= value - profile.jitter_range

    # raise ValueError if jitter action fails
    profile.read_field = MagicMock(return_value="wth")
    with pytest.raises(ValueError) as err:
        field.get_value(profile, {}, record)
        assert "Unable to jitter field: Toto" in str(err)


def test_jitter_min():
    value = 0.0
    profile = FileProfile()
    profile.read_field = MagicMock(return_value=value)
    record = MagicMock()
    field_config = {"name": "Toto", "jitter": True, "jitter-min": 0.0}
    field = DeIdField.factory(field_config)
    with patch("random.uniform", return_value=-0.4):
        new_value = field.get_value(profile, {}, record)
    assert field.jitter_min == 0.0
    assert new_value == 0.0


def test_jitter_max():
    value = 0.0
    profile = FileProfile()
    profile.read_field = MagicMock(return_value=value)
    record = MagicMock()
    field_config = {"name": "Toto", "jitter": True, "jitter-max": 0.0}
    field = DeIdField.factory(field_config)
    with patch("random.uniform", return_value=0.4):
        new_value = field.get_value(profile, {}, record)
    assert field.key == "jitter"
    assert field.jitter_max == 0.0
    assert new_value == 0.0


def test_deid_jitter_load_save_to_from_config():
    # jitter-range load/save to/from profile
    profile = FileProfile()
    profile.load_config({"jitter-range": 10})
    assert profile.jitter_range == 10
    config = profile.to_config()
    assert config["jitter-range"] == 10
    assert config["jitter-type"] == profile.jitter_type

    profile = FileProfile()
    profile.load_config({"jitter-type": "int"})
    assert profile.jitter_type == "int"
    config = profile.to_config()
    assert config["jitter-type"] == "int"
    assert config["jitter-range"] == profile.jitter_range

    # jitter-range load/save to/from field config
    profile = FileProfile()
    profile.load_config(
        {
            "fields": [
                {
                    "name": "PatientWeight",
                    "jitter": True,
                    "jitter-type": "int",
                    "jitter-range": 5,
                    "jitter-min": 30,
                    "jitter-max": 80,
                }
            ]
        }
    )
    field = profile.field_map["PatientWeight"]
    assert field.jitter_type == "int"
    assert field.jitter_range == 5
    assert field.jitter_min == 30
    assert field.jitter_max == 80
    config = field.to_config()
    assert config["jitter-range"] == 5
    assert config["jitter-type"] == "int"
    assert config["jitter-min"] == 30
    assert config["jitter-max"] == 80


def test_keep_action():
    profile = FileProfile()
    profile.load_config({"fields": [{"name": "toto", "keep": True}]})
    field = profile.field_map["toto"]
    assert field.key == "keep"

    config = field.to_config()
    assert config["name"] == "toto"
    assert config["keep"] is True

    profile.read_field = MagicMock(return_value="same")
    value = field.get_value(profile, {}, None)
    assert value == "same"


def test_default_action_is_keep():
    profile = FileProfile()
    profile.load_config({"fields": [{"name": "toto"}]})
    field = profile.field_map["toto"]
    assert field.key == "keep"


@pytest.mark.parametrize("set_field_config", [False, True])
@pytest.mark.parametrize(
    "return_value,replace_with_insert,expected",
    [
        (None, True, "Toto"),
        ("something", True, "Toto"),
        (None, False, None),
        ("something", False, "Toto"),
    ],
)
def test_replace_with_get_value(
    return_value, replace_with_insert, expected, set_field_config
):
    profile = FileProfile()
    record = MagicMock()

    if set_field_config:
        field_config = {"name": "Toto", "replace-with": "Toto"}
        profile.replace_with_insert = replace_with_insert
    else:
        field_config = {
            "name": "Toto",
            "replace-with": "Toto",
            "replace-with-insert": replace_with_insert,
        }

    profile.read_field = MagicMock(return_value=return_value)
    field = DeIdField.factory(field_config)
    new_value = field.get_value(profile, {}, record)
    assert new_value is expected


def test_hashuid_get_value_has_64_max_character():
    profile = FileProfile()
    profile.uid_numeric_name = "1.12.123.1234.12345.123456.1234567.12345678"
    profile.uid_prefix_fields = 8
    profile.read_field = MagicMock(return_value="12345678901234567890")
    field_config = {"name": "Toto", "hashuid": True}
    field = DeIdField.factory(field_config)
    new_value = field.get_value(profile, {}, MagicMock())
    assert len(new_value) == UID_MAX_LENGTH


def test_hashuid_does_not_generate_uid_with_leading_0():
    profile = FileProfile()
    # This value generates digest parts that starts with "O"
    profile.read_field = MagicMock(
        return_value="1.3.6.1.4.1.5962.1.1.1.1.1.20040119072730.12322"
    )
    field_config = {"name": "Toto", "hashuid": True}
    field = DeIdField.factory(field_config)
    new_value = field.get_value(profile, {}, MagicMock())
    assert all([p[0] != "0" for p in new_value.split(".")])


@pytest.mark.parametrize(
    "in_, out_, conf",
    [
        ("2022-03-04 10:00:00", "2022-03-14 10:00:00", {}),
        ("2022-03-04 10:00:00", "2022-03-24 10:00:00", {"date-increment-override": 20}),
        # Jitter
        ("2022-03-04 10:00:00", "2022-03-19 10:00:00", {"jitter-date": True}),
        (
            "2022-03-04 10:00:00",
            "2022-04-18 10:00:00",
            {
                "jitter-date": True,
                "jitter-unit": "weeks",
                "datetime-min": "-10years",
                "datetime-max": "+0years",
            },
        ),
        (
            "2022-03-04 10:00:00",
            "2022-04-08 10:00:00",
            {"jitter-date": True, "jitter-unit": "weeks", "date-increment-override": 0},
        ),
        (
            1689613845,
            1692637845,
            {
                "jitter-date": True,
                "jitter-unit": "weeks",
                "date-increment-override": 0,
                "date-format": "timestamp",
            },
        ),
    ],
)
def test_increment_date_field(mocker, in_, out_, conf):
    mocker.patch.object(random, "randint", lambda *_: 5)
    random.seed(0)
    profile = FileProfile()
    profile.date_format = "%Y-%m-%d %H:%M:%S"
    profile.date_increment = 10
    profile.read_field = MagicMock(return_value=in_)
    field_config = {"name": "test", "increment-date": True, **conf}

    field = DeIdField.factory(field_config)
    assert field.get_value(profile, {}, MagicMock()) == out_


def test_perform_date_inc_fail():
    test_DeIdField = DeIdField(fieldname="StudyDate")
    profile = MagicMock()
    profile.read_field.return_value = 123456

    with pytest.raises(TypeError) as err:
        test_DeIdField._perform_date_inc(
            profile=profile,
            state=None,
            record=None,
            fmt="%Y%m%d",
            date_increment_override=None,
            jitter=None,
            jitter_range=None,
            jitter_unit=None,
            datetime_min=None,
            datetime_max=None,
        )
    assert "Expected string, found 123456 (<class 'int'>)" in str(err)


@pytest.mark.parametrize(
    "in_, out_, conf",
    [
        ("2022-03-04 10:00:00", "2022-03-14 10:00:00", {}),
        ("2022-03-04 10:00:00", "2022-03-24 10:00:00", {"date-increment-override": 20}),
        # Jitter
        ("2022-03-04 10:00:00", "2022-03-19 10:00:00", {"jitter-date": True}),
        (
            "2022-03-04 10:00:00",
            "2022-04-18 10:00:00",
            {
                "jitter-date": True,
                "jitter-unit": "weeks",
                "datetime-min": "-10years",
                "datetime-max": "+0years",
            },
        ),
        (
            "2022-03-04 10:00:00",
            "2022-04-08 10:00:00",
            {"jitter-date": True, "jitter-unit": "weeks", "date-increment-override": 0},
        ),
        (
            1689613845,
            1692637845,
            {
                "jitter-date": True,
                "jitter-unit": "weeks",
                "date-increment-override": 0,
                "datetime-format": "timestamp",
            },
        ),
    ],
)
def test_increment_datetime_field(mocker, in_, out_, conf):
    mocker.patch.object(random, "randint", lambda *_: 5)
    random.seed(0)
    profile = FileProfile()
    profile.datetime_format = "%Y-%m-%d %H:%M:%S"
    profile.date_increment = 10
    profile.read_field = MagicMock(return_value=in_)
    field_config = {"name": "test", "increment-datetime": True, **conf}

    field = DeIdField.factory(field_config)
    assert field.get_value(profile, {}, MagicMock()) == out_


@pytest.mark.parametrize(
    "in_, out_, conf",
    [
        (
            "2022-03-04 10:00:00",
            "2021-03-04 00:00:00",
            {
                "jitter-date": True,
                "jitter-unit": "weeks",
                "jitter-range": 20,
                "datetime-max": "20210304",
            },
        ),
        (
            "2022-03-04 10:00:00",
            "2024-03-04 00:00:00",
            {
                "jitter-date": True,
                "jitter-unit": "weeks",
                "jitter-range": 20,
                "datetime-min": "20240304",
            },
        ),
    ],
)
def test_datetime_minmax(in_, out_, conf):
    profile = FileProfile()
    profile.datetime_format = "%Y-%m-%d %H:%M:%S"
    profile.date_increment = 10
    profile.read_field = MagicMock(return_value=in_)
    field_config = {"name": "test", "increment-datetime": True, **conf}

    field = DeIdField.factory(field_config)
    new_value = field.get_value(profile, {}, MagicMock())
    assert new_value == out_


def test_encrypt_decrypt():
    secret_key = secrets.token_hex(16)
    e_profile = FileProfile()
    e_profile.secret_key = secret_key
    e_profile.read_field = MagicMock(return_value="test value")
    e_config = {
        "name": "test",
        "encrypt": True,
    }

    field = DeIdField.factory(e_config)
    encrypted_value = field.get_value(e_profile, {}, MagicMock())

    assert encrypted_value != "test value"

    d_profile = FileProfile()
    d_profile.secret_key = secret_key

    d_profile.read_field = MagicMock(return_value=encrypted_value)
    d_config = {
        "name": "test",
        "decrypt": True,
    }

    field = DeIdField.factory(d_config)
    decrypted_value = field.get_value(d_profile, {}, MagicMock())
    assert decrypted_value == "test value"


def test_encrypt_decrypt_force_nonce():
    secret_key = secrets.token_hex(16)
    e_profile = FileProfile()
    e_profile.secret_key = secret_key
    e_profile.force_nonce = "Gfd5PrWzD38="
    e_profile.read_field = MagicMock(return_value="test value")
    e_config = {
        "name": "test",
        "encrypt": True,
    }

    field = DeIdField.factory(e_config)
    encrypted_value = field.get_value(e_profile, {}, MagicMock())

    assert encrypted_value != "test value"
    # The beginning of the encrypted value should match force_nonce
    assert encrypted_value[:12] == "Gfd5PrWzD38="
