import re
from datetime import datetime
from unittest import mock

import pytz

from flywheel_migration import EFile, PFile
from flywheel_migration.pfile import _RawPFile


def grep_file(path, expr):
    pattern = re.compile(expr)
    with open(path, "r") as f:
        for line in f.readlines():
            if pattern.match(line):
                return True
    return False


def test_de_identify_efile(efile):
    assert grep_file(efile, r"start_loc\s*=\s*0\.9\s+19\.5\s+-58\.3")
    assert grep_file(efile, r"patient id\s*=\s* X. XXXXX")
    assert grep_file(efile, r"patient name\s*=\s*XXXXXXXX\^YYYYYYY")

    EFile(efile, de_identify=True)

    assert grep_file(efile, r"start_loc\s*=\s*0\.9\s+19\.5\s+-58\.3")
    assert not grep_file(efile, r"patient id\s*=\s* X. XXXXX")
    assert not grep_file(efile, r"patient name\s*=\s*XXXXXXXX\^YYYYYYY")


@mock.patch("flywheel_migration.util.DEFAULT_TZ", pytz.utc)
def test_parse_pfile(pfile):
    pf = PFile(pfile)

    assert (
        pf.session_uid == "1.2.840.113619.6.408.209672366014497044159138030163830231721"
    )
    assert pf.series_uid == "1.2.840.113619.2.408.15512023.2926070.29349.1505321625.989"
    assert (
        pf.acquisition_uid
        == "1.2.840.113619.2.408.15512023.2926070.29349.1505321625.989"
    )
    assert pf.acquisition_label == "HO Shim"
    assert pf.subject_label == "ex14997"
    assert pf.subject_code == "ex14997"

    expected_timestamp = datetime.fromtimestamp(1505307720.0, pytz.UTC).astimezone(
        pf.acquisition_timestamp.tzinfo
    )
    assert pf.acquisition_timestamp == expected_timestamp

    assert pf.raw.patient_id == "cni/test"


@mock.patch("flywheel_migration.util.DEFAULT_TZ", pytz.utc)
def test_parse_pfile_acq_uid(pfile):
    def parse(*_, **__):
        return (
            {
                "logo": b"GE_MED_NMR",
                "scan_date": b"09/13/117\x00",
                "scan_time": b"13:02\x00\x00\x00",
                "exam_no": 14997,
                "exam_uid": "1.2.840.113619.6.408.209672366014497044159138030163830231721",
                "patient_name": b"mr^phantom",
                "patient_id": b"cni/test",
                "patient_dob": b"",
                "accession_no": b"",
                "series_no": 4,
                "series_desc": b"HO Shim",
                "series_uid": "1.2.840.113619.2.408.15512023.2926070.29349.1505321625.989",
                "prescribed_duration": 9348096.0,
                "im_datetime": 0,
                "acq_no": "2",
                "psd_name": b"sprl_hos",
            },
            None,
        )

    with mock.patch("flywheel_migration.pfile._RawPFile.parse", parse):
        pf = PFile(pfile)

        assert (
            pf.session_uid
            == "1.2.840.113619.6.408.209672366014497044159138030163830231721"
        )
        assert (
            pf.series_uid
            == "1.2.840.113619.2.408.15512023.2926070.29349.1505321625.989"
        )
        assert (
            pf.acquisition_uid
            == "1.2.840.113619.2.408.15512023.2926070.29349.1505321625.989_2"
        )
        assert pf.acquisition_label == "HO Shim"
        assert pf.subject_label == "ex14997"
        assert pf.subject_code == "ex14997"

        expected_timestamp = datetime.fromtimestamp(1505307720.0, pytz.UTC).astimezone(
            pf.acquisition_timestamp.tzinfo
        )
        assert pf.acquisition_timestamp == expected_timestamp

        assert pf.raw.patient_id == "cni/test"


def test_de_identify_pfile(pfile):
    PFile(pfile, de_identify=True)

    # Raw parse to verify de-identification
    attrs, offsets = _RawPFile.parse(pfile)
    assert attrs["patient_id"] == b""
    assert attrs["patient_name"] == b""
    assert attrs["patient_dob"] == b""
