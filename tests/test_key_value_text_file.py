import logging
import os
import shutil

import six
from fs.osfs import OSFS

from flywheel_migration import deidentify
from flywheel_migration.deidentify.key_value_text_file_profile import (
    KeyValueTextFileProfile,
    KeyValueTextFileRecord,
)

log = logging.getLogger(__name__)


def test_key_value_text_file(kvtext_file, template_file):
    path = kvtext_file("MHD", "000000-M00-OD-001-OCT.mhd")
    dir_name, base_name = os.path.split(path)
    base_name = six.u(base_name)  # fs requires unicode
    test_fs = OSFS(dir_name)
    profile = deidentify.load_profile(template_file("key-value-text-file-profile"))

    profile.process_packfile("key-value-text-file", test_fs, test_fs, [base_name])

    # Verify the de-id
    delimiter = profile.get_file_profile("key-value-text-file").delimiter
    ignore_bad_lines = profile.get_file_profile("key-value-text-file").ignore_bad_lines
    dst_path = os.path.join(dir_name, "111111-cccf3d518006f6b2-OD-001-OCT.mhd")
    with open(dst_path) as fobj:
        record = KeyValueTextFileRecord(fobj, delimiter, ignore_bad_lines)

    assert (
        record["ElementDataFile"] == "111111-cccf3d518006f6b2-OD-001-OCT-20210316.raw"
    )
    # Test upsert
    assert record["Spam"] == "Eggs"
    assert record["SomeDate"] == "20210311"
    assert len(record) == 15
    shutil.rmtree(dir_name)


def test_key_value_profile_to_config():
    profile = KeyValueTextFileProfile()
    config = {"date-increment": 1, "fields": [{"name": "Toto", "replace-with": "NA"}]}
    profile.load_config(config)

    res_config = profile.to_config()

    assert res_config["date-increment"] == 1
    assert len(config["fields"]) == 1
