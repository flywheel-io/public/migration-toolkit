import pathlib

import pydicom
from pytest import raises, warns

from flywheel_migration import (
    DicomFile,
    global_ignore_unknown_tags,
    set_vr_mismatch_callback,
)


def test_parse_dicom_file(dicom_file):
    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )

    df = DicomFile(path, parse=True)

    assert (
        df.session_uid == "1.2.840.113619.6.408.128090802883025653595086587293755801755"
    )
    assert (
        df.acquisition_uid
        == "1.2.840.113619.2.408.5282380.5220731.30424.1516669014.474"
    )
    assert df.acquisition_label == "3Plane Loc fgre"
    assert df.subject_label == "ex16844"
    assert df.subject_code == "ex16844"
    assert df.subject_firstname == "Firstname"
    assert df.subject_lastname == "Lastname"

    assert "PatientName" in df.raw
    assert "PatientBirthDate" in df.raw
    assert "PatientID" in df.raw


def test_parse_dicom_file_with_acquisition_number_postfix(dicom_file):
    path = dicom_file(
        "16844_3_2_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.594.dcm",
    )

    df = DicomFile(path, parse=True)

    assert (
        df.acquisition_uid
        == "1.2.840.113619.2.408.5282380.5220731.30424.1516669014.480_2"
    )


def test_parse_dicom_file_ignore_unknown(dicom_file):
    reset_config = global_ignore_unknown_tags()
    try:
        path = dicom_file(
            "16844_1_1_dicoms",
            "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
        )

        df = DicomFile(path, parse=True)

        assert (
            df.session_uid
            == "1.2.840.113619.6.408.128090802883025653595086587293755801755"
        )
        assert (
            df.acquisition_uid
            == "1.2.840.113619.2.408.5282380.5220731.30424.1516669014.474"
        )
        assert df.acquisition_label == "3Plane Loc fgre"
        assert df.subject_label == "ex16844"
        assert df.subject_code == "ex16844"
        assert df.subject_firstname == "Firstname"
        assert df.subject_lastname == "Lastname"

        assert "PatientName" in df.raw
        assert "PatientBirthDate" in df.raw
        assert "PatientID" in df.raw
    finally:
        reset_config()


def test_de_identify_dicom_file(dicom_file):
    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )

    df = DicomFile(path, de_identify=True)

    assert (
        df.session_uid == "1.2.840.113619.6.408.128090802883025653595086587293755801755"
    )
    assert (
        df.acquisition_uid
        == "1.2.840.113619.2.408.5282380.5220731.30424.1516669014.474"
    )
    assert df.acquisition_label == "3Plane Loc fgre"
    assert df.subject_label == "ex16844"
    assert df.subject_code == "ex16844"
    assert df.subject_firstname is None
    assert df.subject_lastname is None

    raw = pydicom.read_file(path, stop_before_pixels=True)
    assert "PatientName" not in raw
    assert "PatientBirthDate" not in raw
    assert "PatientID" not in raw


def test_de_identify_dicom_file_ignore_unknown(dicom_file):
    reset_config = global_ignore_unknown_tags()
    try:
        path = dicom_file(
            "16844_1_1_dicoms",
            "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
        )

        df = DicomFile(path, de_identify=True)

        assert (
            df.session_uid
            == "1.2.840.113619.6.408.128090802883025653595086587293755801755"
        )
        assert (
            df.acquisition_uid
            == "1.2.840.113619.2.408.5282380.5220731.30424.1516669014.474"
        )
        assert df.acquisition_label == "3Plane Loc fgre"
        assert df.subject_label == "ex16844"
        assert df.subject_code == "ex16844"
        assert df.subject_firstname is None
        assert df.subject_lastname is None

        raw = pydicom.read_file(path, stop_before_pixels=True)
        assert "PatientName" not in raw
        assert "PatientBirthDate" not in raw
        assert "PatientID" not in raw

    finally:
        reset_config()


def test_dicom_tags_mismatch_vr(dicom_file):
    reset_config = set_vr_mismatch_callback()
    vr_mismatch_element_callback = pydicom.config.data_element_callback
    try:
        path = dicom_file(
            "invalid",
            "invalid_VR_IS_encoded_in_EchoTime.dcm",
        )

        df = DicomFile(path, parse=True)

        assert df.raw["EchoTime"].VR == "DS"

    finally:
        reset_config()
        original_element_callback = pydicom.config.data_element_callback

    assert vr_mismatch_element_callback != original_element_callback


def test_parse_dicom_file_acq_no(dicom_file):
    path = dicom_file(
        "pydicom",
        "CT1.2.752.243.1.1.20200625120033641.5900.70727.dcm",
    )

    df = DicomFile(path, parse=True)

    assert df.session_uid == "1.2.752.243.1.1.20200625120032763.5000.57603"
    assert df.acquisition_uid == "1.2.752.243.1.1.20200625120032763.6000.84155"


def test_parse_dicom_file_with_is_study_id(dicom_file):
    """Check that when the "StudyID" returns an integer string (IS), we can still
    instantiate the DicomFile
    """
    path = dicom_file(
        "custom",
        "mp2rage_deriv.dcm",
    )

    df = DicomFile(path, parse=True)

    assert df.subject_label == "ex25049"
    assert (
        df.session_uid == "1.2.840.113619.6.475.248181703341742397412040423732007674472"
    )
    assert (
        df.acquisition_uid == "1.2.840.113619.2.156.1084953804.24750.1651779163.416023"
    )


def test_dicom_file_stop_before_pixels(dicom_file):
    """Check that the stop_before_pixels flag works as expected."""
    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    df = DicomFile(path, parse=True)
    df_nopx = DicomFile(path, parse=True, stop_before_pixels=True)
    with warns(
        UserWarning,
        match="Conflicting de_identify and stop_before_pixels parameters provided",
    ):
        df_nopx_deid = DicomFile(path, de_identify=True, stop_before_pixels=True)
    assert "PixelData" in df.raw
    assert "PixelData" not in df_nopx.raw
    assert "PixelData" in df_nopx_deid.raw
    assert (
        df.session_uid == "1.2.840.113619.6.408.128090802883025653595086587293755801755"
    )
    assert (
        df.acquisition_uid
        == "1.2.840.113619.2.408.5282380.5220731.30424.1516669014.474"
    )
    assert df.acquisition_label == "3Plane Loc fgre"
    assert df.subject_label == "ex16844"
    assert df.subject_code == "ex16844"
    assert df.subject_firstname == "Firstname"
    assert df.subject_lastname == "Lastname"
    assert (
        df_nopx.session_uid
        == "1.2.840.113619.6.408.128090802883025653595086587293755801755"
    )
    assert (
        df_nopx.acquisition_uid
        == "1.2.840.113619.2.408.5282380.5220731.30424.1516669014.474"
    )
    assert df_nopx.acquisition_label == "3Plane Loc fgre"
    assert df_nopx.subject_label == "ex16844"
    assert df_nopx.subject_code == "ex16844"
    assert df_nopx.subject_firstname == "Firstname"
    assert df_nopx.subject_lastname == "Lastname"
    assert df_nopx_deid.subject_firstname is None
    assert df_nopx_deid.subject_lastname is None


def test_dicom_file_stop_when_exception(dicom_file):
    """Check that exception is raised when mutually exclusive stop_when and stop_before_pixels
    are set.
    """

    def stop_when(related_acquisitions):
        stop_tag = (0x3006, 0x0011)

        def f(tag, *args):
            return tag == stop_tag

    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )

    with raises(
        ValueError, match="stop_when and stop_before_pixels are mutually exclusive"
    ):
        DicomFile(path, parse=True, stop_when=stop_when, stop_before_pixels=True)


def test_dicom_file_buffered_read(dicom_file):
    """Test DicomFile's ability to read BufferedIOBase objects."""
    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    with open(path, "rb") as fp:
        df = DicomFile(fp, parse=True, stop_before_pixels=True)
        assert "PixelData" not in df.raw
    assert (
        df.session_uid == "1.2.840.113619.6.408.128090802883025653595086587293755801755"
    )
    assert (
        df.acquisition_uid
        == "1.2.840.113619.2.408.5282380.5220731.30424.1516669014.474"
    )
    assert df.acquisition_label == "3Plane Loc fgre"
    assert df.subject_label == "ex16844"
    assert df.subject_code == "ex16844"
    assert df.subject_firstname == "Firstname"
    assert df.subject_lastname == "Lastname"


def test_dicom_file_pathlib(dicom_file):
    """Test DicomFile's ability to read pathlib.Path objects."""
    path = pathlib.Path(
        dicom_file(
            "16844_1_1_dicoms",
            "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
        )
    )
    df = DicomFile(path, parse=True, stop_before_pixels=True)
    assert "PixelData" not in df.raw
    assert (
        df.session_uid == "1.2.840.113619.6.408.128090802883025653595086587293755801755"
    )
    assert (
        df.acquisition_uid
        == "1.2.840.113619.2.408.5282380.5220731.30424.1516669014.474"
    )
    assert df.acquisition_label == "3Plane Loc fgre"
    assert df.subject_label == "ex16844"
    assert df.subject_code == "ex16844"
    assert df.subject_firstname == "Firstname"
    assert df.subject_lastname == "Lastname"
