import os
import zipfile
from pathlib import Path

import pydicom
import six
from fs import copy as fs_copy
from fs.osfs import OSFS
from fs.tempfs import TempFS
from fs.zipfs import ReadZipFS

from flywheel_migration import deidentify
from flywheel_migration.deidentify.deid_field import DeIdField
from flywheel_migration.deidentify.file_profile import FileProfile
from flywheel_migration.deidentify.zip_archive_profile import (
    ZipArchiveMemberRecord,
    ZipArchiveProfile,
    ZipArchiveRecord,
)


def test_zip_archive_profile(zip_file):
    # Verify that subclass is registered
    file_profile_names = FileProfile.profile_names()
    assert len(file_profile_names) > 0
    assert "zip" in file_profile_names

    path = zip_file("sample", "dicom.zip")
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    # Test comment
    profile = ZipArchiveProfile()
    assert profile.matches_byte_sig(test_fs, basename)
    profile.add_field(
        DeIdField.factory({"name": "comment", "replace-with": "no comment"})
    )
    with TempFS() as temp_fs:
        profile.process_files(test_fs, temp_fs, [basename])

        with zipfile.ZipFile(temp_fs.open(basename, "rb")) as zipr:
            assert zipr.comment == b"no comment"


def test_archive_record(zip_file):
    path = zip_file("sample", "dicom.zip")
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)
    tmp_archive_record = ZipArchiveRecord(test_fs, basename)
    # test get_zip_sys_path
    assert not tmp_archive_record.get_zip_sys_path(test_fs, "does_not_exist")
    with ReadZipFS(path) as zip_fs:
        assert tmp_archive_record.get_zip_sys_path(
            zip_fs,
            "/16844_3_1_dicoms/MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.583.dcm",
        )
    # test encode comment
    assert tmp_archive_record.encode_comment(4) == b""


def test_zip_member_record(zip_file, template_file):
    path = zip_file("sample", "dicom.zip")
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_member_record = ZipArchiveMemberRecord(path)

    assert test_member_record.src_subdir == dirname

    assert test_member_record.src_filename == basename

    test_var = None

    def test_callback(dst_fs, dst_path):
        nonlocal test_var
        test_var = dst_path + "s"
        return test_var

    callback_func = test_member_record.get_callback(
        subdir="test", callback=test_callback
    )
    callback_func(None, "cat")
    assert test_var == os.path.join("test", "cats")
    assert test_member_record.dst_path == os.path.join("test", "cat")
    path_dict_out = test_member_record.get_path_dict()
    path_dict_pred = {
        "src_path": os.path.join(dirname, basename),
        "dst_path": os.path.join("test", "cat"),
    }
    assert path_dict_out == path_dict_pred
    with ReadZipFS(path) as src_fs:
        with TempFS() as dst_fs:
            profile = ZipArchiveProfile()
            profile.load_config(
                {
                    "dicom": {
                        "fields": [
                            {
                                "name": "SeriesDescription",
                                "replace-with": "FLYWHEEL",
                            }
                        ],
                        "filenames": [
                            {
                                "output": "CAT_{fnUID}.dcm",
                                "input-regex": r"^[A-Z]+\.(?P<fnUID>[\d\.]+)\.dcm$",
                            }
                        ],
                    },
                    "zip": {"fields": [{"name": "comment", "remove": True}]},
                }
            )

            test_proc_zip_member = ZipArchiveMemberRecord(
                os.path.join(
                    "16844_2_1_dicoms",
                    "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.437.dcm",
                )
            )
            assert test_proc_zip_member.src_path in [fp for fp in src_fs.walk.files("")]
            assert any(
                prof.matches_file(test_proc_zip_member.src_filename)
                for prof in profile.file_profiles
            )
            test_proc_zip_member.process_file(
                src_fs, dst_fs, archive_profile=profile, hash_subdir=True
            )
            print(test_proc_zip_member.dst_path)

            # subdir hash value is 'fb5848d31f1c5e19269e41e33af6a4934b1eb635a20ee6224067ad628245afe8'
            assert test_proc_zip_member.dst_path == os.path.join(
                "fb5848d31f1c5e19269e41e33af6a4934b1eb635a20ee6224067ad628245afe8",
                "CAT_1.2.840.113619.2.408.5282380.5220731.23348.1516669692.437.dcm",
            )

            for fp in dst_fs.walk.files(""):
                dcm = pydicom.dcmread(dst_fs.getsyspath(fp))
                assert getattr(dcm, "SeriesDescription") == "FLYWHEEL"


def test_zip_archive_profile_filenames(zip_file):
    path = zip_file("sample", "dicom_valid.zip")
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    profile = ZipArchiveProfile()
    zip_config = {
        "zip": {
            "filenames": [
                {
                    "output": "{SeriesDescription}_{SeriesNumber}.dicom.zip",
                    "input-regex": r"^.*\.zip$",
                }
            ]
        },
        "dicom": {
            "filenames": [
                {
                    "output": "{SeriesNumber}_{InstanceNumber}_{SeriesDescription}_{SOPInstanceUID}.dcm",
                    "input-regex": r"^.*\.dcm.*",
                }
            ],
            "fields": [
                {"name": "SeriesDescription", "replace-with": "FLYWHEEL"},
                {"name": "SeriesNumber", "replace-with": "1"},
            ],
        },
    }
    profile.load_config(zip_config)
    with OSFS(dirname) as src_fs:
        assert profile.matches_byte_sig(src_fs, basename)
        with TempFS() as dst_fs:
            profile.process_files(src_fs=src_fs, dst_fs=dst_fs, files=[basename])

            assert hasattr(profile, "record")
            expected_name = "FLYWHEEL_1.dicom.zip"
            assert (
                profile.get_dest_path(None, getattr(profile, "record"), basename)
                == expected_name
            )

            assert not os.path.exists(profile.record.processed_sys_path)
            assert dst_fs.exists(expected_name)
            assert not profile.record.get_missing()
            assert dst_fs.exists(expected_name)
            zip_path = dst_fs.getsyspath(expected_name)
            with ReadZipFS(zip_path) as zipfs:
                with TempFS() as temp_fs:
                    fs_copy.copy_fs(zipfs, temp_fs)
                    for fp in temp_fs.walk.files():
                        dcm = pydicom.dcmread(temp_fs.getsyspath(fp))
                        expected_fp = (
                            f"1_{dcm.InstanceNumber}_FLYWHEEL_{dcm.SOPInstanceUID}.dcm"
                        )
                        assert expected_fp == os.path.basename(fp)


def test_DeIdProfile_on_zip_archive(zip_file, template_file):
    zip_path = zip_file("sample", "dicom_valid.zip")
    dirname, basename = os.path.split(zip_path)
    basename = six.u(basename)  # fs requires unicode
    template = template_file("zip-archive-profile")
    deid_profile = deidentify.load_profile(template)
    with OSFS(dirname) as src_fs:
        with TempFS() as dst_fs:
            deid_profile.process_file(src_fs, basename, dst_fs)
            zip_profile = deid_profile.get_file_profile("zip")
            expected_zip_name = "FLYWHEEL_FLYWHEEL.dcm.zip"
            dst_zip_path = dst_fs.getsyspath(expected_zip_name)
            assert getattr(zip_profile, "record", None)
            assert (
                zip_profile.get_dest_path(None, zip_profile.record, basename)
                == expected_zip_name
            )
            assert dst_fs.exists(expected_zip_name)
            with zipfile.ZipFile(dst_fs.open(expected_zip_name, "rb")) as zipr:
                assert zipr.comment == b"FLYWHEEL"
            with ReadZipFS(dst_zip_path) as zipfs:
                with TempFS() as temp_fs:
                    fs_copy.copy_fs(zipfs, temp_fs)
                    for fp in temp_fs.walk.files():
                        dcm = pydicom.dcmread(temp_fs.getsyspath(fp))
                        expected_fp = f"{dcm.Modality}_{dcm.SOPInstanceUID}.dcm"
                        assert expected_fp == os.path.basename(fp)


def test_zip_archive_nested_matches_byte_sig_only(zip_file, template_file):
    zip_path = zip_file("sample", "dicom_valid.zip")
    dirname, basename = os.path.split(zip_path)
    basename = six.u(basename)  # fs requires unicode
    template = template_file("zip-archive-profile")
    deid_profile = deidentify.load_profile(template)
    new_zip = Path(dirname) / "test.zip"
    with zipfile.ZipFile(new_zip, "w") as zipw:
        with zipfile.ZipFile(zip_path, "r") as zipr:
            for z_path in zipr.namelist():
                if z_path.endswith(".dcm"):
                    out = zipr.extract(z_path)
                    new_path = Path(z_path.strip(".dcm"))
                    zipw.write(out, arcname=new_path)
                    os.unlink(out)
    with OSFS(dirname) as src_fs:
        with TempFS() as dst_fs:
            deid_profile.process_file(src_fs, new_zip.name, dst_fs)
            zip_profile = deid_profile.get_file_profile("zip")
            expected_zip_name = "FLYWHEEL_FLYWHEEL.dcm.zip"
            dst_zip_path = dst_fs.getsyspath(expected_zip_name)
            assert getattr(zip_profile, "record", None)
            assert (
                zip_profile.get_dest_path(None, zip_profile.record, basename)
                == expected_zip_name
            )
            assert dst_fs.exists(expected_zip_name)
            with zipfile.ZipFile(dst_fs.open(expected_zip_name, "rb")) as zipr:
                assert zipr.comment == b"FLYWHEEL"
            with ReadZipFS(dst_zip_path) as zipfs:
                with TempFS() as temp_fs:
                    fs_copy.copy_fs(zipfs, temp_fs)
                    for fp in temp_fs.walk.files():
                        dcm = pydicom.dcmread(temp_fs.getsyspath(fp))
                        expected_fp = f"{dcm.Modality}_{dcm.SOPInstanceUID}.dcm"
                        assert expected_fp == os.path.basename(fp)


def test_subdir_hashing(zip_file, template_file):
    zip_path = zip_file("sample", "nested_folders.zip")
    dirname, basename = os.path.split(zip_path)
    template = template_file("zip-archive-profile")
    deid_profile = deidentify.load_profile(template)
    expected_zip_name = "FLYWHEEL_FLYWHEEL.dcm.zip"
    with OSFS(dirname) as src_fs:
        with TempFS() as dst_fs:
            deid_profile.process_file(src_fs, basename, dst_fs)
            # Make sure the number of directories is the same
            with zipfile.ZipFile(dst_fs.open(expected_zip_name, "rb")) as zipr:
                dir_list = list()
                for item in zipr.infolist():
                    if item.filename.endswith(os.path.sep) and len(item.filename) > 1:
                        dir_list.append(item.filename)

    assert len(dir_list) == 5
    contains_nested_dir = False
    for item in dir_list:
        if all([item in x for x in dir_list]):
            contains_nested_dir = True
    assert contains_nested_dir
