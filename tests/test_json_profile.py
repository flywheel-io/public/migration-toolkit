import json
import os

import six
from fs.osfs import OSFS

from flywheel_migration import deidentify
from flywheel_migration.deidentify.deid_field import DeIdField
from flywheel_migration.deidentify.file_profile import FileProfile
from flywheel_migration.deidentify.json_file_profile import (
    JSONFileProfile,
    JSONRecord,
)


def test_json_record_get_all_dot_paths(json_file):
    path = json_file("sample", "sample1.json")
    with open(path, "r") as fp:
        record = JSONRecord(fp)

    all_dot_paths = record.get_all_dotty_paths()
    expected = [
        "PatientID",
        "nested",
        "nested.key1",
        "nested.key2",
        "nested.RegexTest",
        "nested.list.0",
        "nested.list.1",
        "nested.list.2",
        "list",
        "list.0",
        "list.0.key1",
        "list.1",
        "list.1.key2",
        "list.2",
        "list.2.RegexTest",
        "Date",
        "DateTime",
        "RegexTest",
    ]
    assert all([x in all_dot_paths for x in expected])


def test_json_record_get_all_dot_paths_with_other_separator():
    data = {"dot.key": "Test", "nested": {"dot.key": [1, 2]}}
    record = JSONRecord.from_dict(data, separator=":")
    all_dot_paths = record.get_all_dotty_paths()
    expected = [
        "dot.key",
        "nested",
        "nested:dot.key",
        "nested:dot.key:0",
        "nested:dot.key:1",
    ]
    assert all([x in all_dot_paths for x in expected])


def test_json_file_profile(json_file):
    # Verify that subclass is registered
    file_profile_names = FileProfile.profile_names()
    assert len(file_profile_names) > 0
    assert "json" in file_profile_names

    path = json_file("sample", "sample1.json")
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    profile = JSONFileProfile()
    profile.date_increment = -1
    profile.add_field(DeIdField.factory({"name": "PatientID", "remove": True}))
    profile.add_field(
        DeIdField.factory({"name": "nested.key1", "replace-with": "REDACTED"})
    )
    profile.add_field(DeIdField.factory({"name": "New.key1", "replace-with": "NEW"}))
    profile.add_field(
        DeIdField.factory({"name": "New.list", "replace-with": [1, 2, 3]})
    )
    profile.add_field(
        DeIdField.factory({"name": "New.dict", "replace-with": {"new": "dict"}})
    )
    profile.add_field(
        DeIdField.factory({"name": "list.0.key1", "replace-with": "REDACTED"})
    )
    profile.add_field(
        DeIdField.factory({"name": "DateTime", "increment-datetime": True})
    )
    profile.add_field(
        DeIdField.factory({"regex": ".*RegexTest.*", "replace-with": "REDACTED"})
    )

    profile.process_files(test_fs, test_fs, [basename])
    with test_fs.open(basename, "r") as fp:
        deid = json.load(fp)
        assert "PatientID" not in deid
        assert deid["nested"]["key1"] == "REDACTED"
        assert deid["New"]["key1"] == "NEW"
        assert deid["New"]["list"] == [1, 2, 3]
        assert deid["New"]["dict"] == {"new": "dict"}
        assert deid["list"][0]["key1"] == "REDACTED"
        assert deid["DateTime"] == "2020-08-02 07:55:00"
        assert deid["nested"]["RegexTest"] == "REDACTED"
        assert deid["list"][2]["RegexTest"] == "REDACTED"
        assert deid["RegexTest"] == "REDACTED"


def test_json_profile_remove_field_do_not_raise_if_key_does_not_exist(json_file):
    path = json_file("sample", "sample1.json")
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    profile = JSONFileProfile()
    profile.add_field(DeIdField.factory({"name": "DoesNotExist", "remove": True}))
    profile.process_files(test_fs, test_fs, [basename])


def test_json_profile_can_load_template_and_process(json_file, template_file):
    path = json_file("sample", "session_container.json")
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    profile = deidentify.load_deid_profile(template_file("json-profile"))
    profile.process_packfile("json", test_fs, test_fs, [basename])
    with test_fs.open(
        "one_cool_cat_G2LOQ_2020-07-23 13_41_00+00_00_TOTO.json", "r"
    ) as fp:
        deid = json.load(fp)
        assert deid["label"] == "one_cool_cat_G2LOQ_2020-07-23 13:41:00+00:00"
        assert deid["timestamp"] == "2020-07-23 13:41:00+00:00"
        assert deid["weight"] != 92.986
        assert deid["info"]["subject_raw"]["lastname"] == "REDACTED"
        assert deid["info"]["subject_raw"]["sex"] == "REDACTED"
        assert deid["info"]["test"] == {"new": "value", "type": "dict"}


def test_increment_field_with_local_format(json_file):
    path = json_file("sample", "sample1.json")
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    # can process
    profile = JSONFileProfile()
    profile.date_increment = -1
    profile.datetime_format = "SomeRandomFormat"
    profile.date_format = "LikeCucumbers"
    profile.add_field(
        DeIdField.factory(
            {
                "name": "DateTime",
                "increment-datetime": True,
                "datetime-format": "%Y-%m-%d %H:%M:%S",
            }
        )
    )
    profile.add_field(
        DeIdField.factory(
            {"name": "Date", "increment-date": True, "date-format": "%Y-%m-%d"}
        )
    )
    profile.process_files(test_fs, test_fs, [basename])
    with test_fs.open(basename, "r") as fp:
        deid = json.load(fp)
        assert deid["DateTime"] == "2020-08-02 07:55:00"
        assert deid["Date"] == "2020-08-02"

    # to config
    config = profile.to_config()
    assert config["datetime-format"] == "SomeRandomFormat"
    assert config["date-format"] == "LikeCucumbers"
    assert config["fields"][0] == {
        "name": "DateTime",
        "increment-datetime": True,
        "datetime-format": "%Y-%m-%d %H:%M:%S",
    }
    assert config["fields"][1] == {
        "name": "Date",
        "increment-date": True,
        "date-format": "%Y-%m-%d",
    }


def test_json_file_profile_regex(tmp_path, mocker):
    basename = "file.json"
    with open(tmp_path / basename, "w") as fp:
        json.dump(
            {"test": {"nested": [{"val": "2022-02-04"}, {"val": "2022-02-05"}]}}, fp
        )
    profile = JSONFileProfile()
    profile.date_format = "%Y %m %d"
    profile.date_increment = -2
    profile.add_field(
        DeIdField.factory(
            {
                "regex": "test.nested.*.val",
                "increment-datetime": True,
                "datetime-format": "%Y-%m-%d",
                "date-increment-override": -3,
            }
        )
    )
    test_fs = OSFS(tmp_path)
    profile.process_files(test_fs, test_fs, [basename])
    with test_fs.open(basename, "r") as fp:
        deid = json.load(fp)
        assert deid["test"]["nested"][0]["val"] == "2022-02-01"
        assert deid["test"]["nested"][1]["val"] == "2022-02-02"
