import logging
import os
import secrets
import shutil
from unittest import mock
from unittest.mock import patch

import pydicom
import pydicom.tag
import pytest
import six
from fs.osfs import OSFS
from pydicom.datadict import get_private_entry

from flywheel_migration import deidentify
from flywheel_migration.deidentify.deid_field import DeIdField
from flywheel_migration.deidentify.dicom_file_profile import (
    DicomFileProfile,
    DicomTagStr,
)
from flywheel_migration.deidentify.file_profile import FileProfile


def test_dicom_file_profile(dicom_file):
    # Verify that subclass is registered
    file_profile_names = FileProfile.profile_names()
    assert len(file_profile_names) > 0
    assert "dicom" in file_profile_names

    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    # Seed initial values
    dcm = pydicom.dcmread(path)
    assert dcm.AccessionNumber == "Accession"
    dcm.PatientID = "1234"
    dcm.PatientName = "Can Akgun"
    dcm.PatientBirthDate = "20000101"
    dcm.SeriesInstanceUID = "1.2.840.113619.6.283.4.983142589.7316.1300473420.841"
    dcm.SOPInstanceUID = "1.3.12.2.1107.5.2.32.35068.2014102309133567866314331"
    dcm.StudyDate = "20180124"
    dcm.AcquisitionDateTime = "20180124114849.123456"
    dcm.StartAcquisitionDateTime = "20180124114849.123456+0400"
    dcm.PatientWeight = 52.3
    dcm.save_as(path)

    profile = DicomFileProfile()
    config = {
        "patient-age-from-birthdate": True,
        "patient-age-units": "Y",
        "date-increment": -17,
        "fields": [
            {"name": "AccessionNumber", "hash": True},
            {"name": "PatientName", "remove": True},
            {"name": "PatientBirthDate", "remove": True},
            {"name": "PatientID", "replace-with": "0000"},
            {"name": "SeriesInstanceUID", "hashuid": True},
            {"name": "SOPInstanceUID", "hashuid": True},
            {"name": "StudyDate", "increment-date": True},
            {
                "name": "AcquisitionDateTime",
                "increment-datetime": True,
                "datetime-max": "+1days",
            },
            {
                "name": "StartAcquisitionDateTime",
                "increment-datetime": True,
                "datetime-max": "20240205",
            },
            {"name": "PatientWeight", "jitter": True, "jitter-min": 40},
        ],
    }
    profile.load_config(config)

    assert profile.matches_packfile("dicom")
    assert not profile.matches_packfile("pv6")
    assert not profile.matches_file("test.txt")

    # apply field operations
    profile.process_files(test_fs, test_fs, [basename])

    # Reload and verify
    # Hashed value should be: 1.3.12.2.651092.137711.166132.421848.119968.345027.314331
    dst_path = os.path.join(
        dirname, "1.3.12.2.651092.137711.166132.421848.119968.345027.314331.MR.dcm"
    )
    dcm = pydicom.dcmread(dst_path)
    assert dcm.AccessionNumber == "057fdb1eb48b2215"
    assert dcm.PatientID == "0000"
    assert dcm.StudyDate == "20180107"
    assert dcm.AcquisitionDateTime == "20180107114849.123456"
    assert dcm.StartAcquisitionDateTime == "20180107114849.123456+0400"
    assert not hasattr(dcm, "PatientName")
    assert not hasattr(dcm, "PatientBirthDate")
    # Check hashed value
    assert (
        dcm.SeriesInstanceUID
        == "1.2.840.113619.551726.420312.177022.222461.230571.501817.841"
    )
    assert (
        dcm.SOPInstanceUID
        == "1.3.12.2.651092.137711.166132.421848.119968.345027.314331"
    )
    assert dcm.PatientAge == "018Y"
    assert dcm.PatientWeight != 52.3


def test_dicom_file_profile_used_OID_when_defined(dicom_file):
    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    # Seed initial values
    dcm = pydicom.dcmread(path)
    dcm.SOPInstanceUID = "1.3.12.2.1107.5.2.32.35068.2014102309133567866314331"
    dcm.save_as(path)

    profile = DicomFileProfile()
    config = {
        "uid-numeric-name": "2.16.840.1.114570",
        "uid-prefix-fields": 5,
        "fields": [{"name": "SOPInstanceUID", "hashuid": True}],
    }
    profile.load_config(config)

    # apply field operations
    profile.process_files(test_fs, test_fs, [basename])

    # Reload and verify
    # Hashed value should be: 2.16.840.1.114570.651092.137711.166132.421848.119968.3450.314331
    dst_path = os.path.join(
        dirname,
        "2.16.840.1.114570.651092.137711.166132.421848.119968.3450.314331.MR.dcm",
    )
    dcm = pydicom.dcmread(dst_path)
    # Check hashed value
    assert dcm.SOPInstanceUID.startswith(profile.uid_numeric_name)


def test_dicom_file_profile_validate_OID_when_used(dicom_file):
    profile = DicomFileProfile()
    profile.uid_numeric_name = "2.16.840.1.114570"
    profile.uid_prefix_fields = 4
    errors = profile.validate()
    assert any(
        [
            "uid_prefix_fields is different from number of blocks in uid_numeric_name"
            in err
            for err in errors
        ]
    )

    profile.uid_numeric_name = "2.16.840.1.114570"
    errors = profile.validate()
    assert any(
        [
            "uid_numeric_name used Flywheel root OID but does not conform" in err
            for err in errors
        ]
    )

    profile.uid_numeric_name = "2.16.840.1.114570.2.2"
    profile.uid_prefix_fields = 7
    errors = profile.validate()
    assert not errors


def test_can_load_dump_from_to_config_OID_related_attributes():
    profile = DicomFileProfile()
    config = {
        "uid-prefix-fields": 5,
        "uid-numeric-name": "2.16.840.1.114570",
        "uid-suffix-fields": 0,
    }
    profile.load_config(config)
    assert profile.uid_numeric_name == "2.16.840.1.114570"
    assert profile.uid_prefix_fields == 5
    assert profile.uid_suffix_fields == 0

    result = profile.to_config()
    assert result["uid-numeric-name"] == "2.16.840.1.114570"
    assert result["uid-prefix-fields"] == 5
    assert result["uid-suffix-fields"] == 0


def test_dicom_file_profile_invalid_dates(dicom_file):
    # Verify that subclass is registered
    file_profile_names = FileProfile.profile_names()
    assert len(file_profile_names) > 0
    assert "dicom" in file_profile_names

    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    # Seed initial values
    dcm = pydicom.dcmread(path)
    dcm.PatientBirthDate = "00000000"
    dcm.StudyDate = "00000000"
    dcm.save_as(path)

    profile = DicomFileProfile()
    config = {
        "patient-age-from-birthdate": True,
        "patient-age-units": "Y",
        "date-increment": 5,
        "fields": [
            {"name": "PatientBirthDate", "remove": True},
            {"name": "StudyDate", "increment-date": True},
        ],
    }
    profile.load_config(config)

    # apply field operations
    # PatientBirthDate and StudyDate both cannot be parsed as a date
    # Because PatientBirthDate is to be removed (not inc), it shouldn't raise
    # But because StudyDate is supposed to increment, it should raise there.
    with pytest.raises(ValueError) as err:
        profile.process_files(test_fs, test_fs, [basename])

    assert "Unable to parse date field: StudyDate" in str(err)


def test_dicom_file_profile_remove_private_tags(dicom_file):
    PRIVATE_TAGS = (0x00090001, 0x00090010)

    # Verify that subclass is registered
    file_profile_names = FileProfile.profile_names()
    assert len(file_profile_names) > 0
    assert "dicom" in file_profile_names

    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    # Seed initial values
    dcm = pydicom.dcmread(path)
    for private_tag in PRIVATE_TAGS:
        dcm.add_new(private_tag, "PN", "PRIVATE DATA")
        assert private_tag in dcm
    dcm.save_as(path)

    profile = DicomFileProfile()
    profile.load_config({"remove-private-tags": True})

    # apply field operations
    profile.process_files(test_fs, test_fs, [basename])

    # Reload and verify
    dst_path = os.path.join(
        dirname, "1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.MR.dcm"
    )
    dcm = pydicom.dcmread(dst_path)
    for private_tag in PRIVATE_TAGS:
        assert private_tag not in dcm


def test_dicom_file_profile_remove_private_tags_keeps_defined_tags(dicom_file):
    # Verify that subclass is registered
    file_profile_names = FileProfile.profile_names()
    assert len(file_profile_names) > 0
    assert "dicom" in file_profile_names

    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    # Create new private block and tag
    dcm = pydicom.dcmread(path)
    block = dcm.private_block(0x0009, "Flywheel", create=True)
    block.add_new(0x0000, "PN", "My^Value")
    block2 = dcm.private_block(0x0011, "Flywheel", create=True)
    block2.add_new(0x0000, "PN", "My^Value")
    dcm.save_as(path)

    profile = DicomFileProfile()
    profile.load_config(
        {
            "remove-private-tags": True,
            "fields": [
                {"name": "(0009, Flywheel, 00)", "keep": True},  # Exists
                {"name": "(0013, Flywheel, 00)", "keep": True},  # Doesn't exist
                # {'name': "(0011, Flywheel, 00)", "keep": True} # Exists in dicom, but not specified in config.
            ],
        }
    )

    # apply field operations
    profile.process_files(test_fs, test_fs, [basename])

    # Reload and verify
    dst_path = os.path.join(
        dirname, "1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.MR.dcm"
    )
    dcm = pydicom.dcmread(dst_path)
    assert 0x00091100 in dcm
    all_groups = set()
    for tag in dcm._dict.keys():
        all_groups.add(tag.group)
    assert 0x11 not in all_groups
    assert 0x13 not in all_groups


def test_DicomTagStr():  # noqa: F811
    result = DicomTagStr("AccessionNumber")
    assert result == "AccessionNumber"
    assert result._dicom_tag is None

    result = DicomTagStr(0x00101010)
    assert result == "(0010, 1010)"
    assert result._dicom_tag == pydicom.tag.Tag(0x00101010)

    result = DicomTagStr("0x00100015")
    assert result == "0x00100015"
    assert result._dicom_tag == pydicom.tag.Tag(0x00100015)

    result = DicomTagStr("00100015")
    assert result == "00100015"
    assert result._dicom_tag == pydicom.tag.Tag(0x00100015)

    result = DicomTagStr("(1000,0001)")
    assert result == "(1000,0001)"
    assert result._dicom_tag == pydicom.tag.Tag(0x10000001)

    result = DicomTagStr("( ff00 , EE01 )")
    assert result == "( ff00 , EE01 )"
    assert result._dicom_tag == pydicom.tag.Tag(0xFF00EE01)

    # test ValueError if multiple matches
    with mock.patch.object(
        DicomTagStr,
        "_parse_nested",
        return_value="SAME",
    ):
        with mock.patch.object(
            DicomTagStr,
            "_parse_nested",
            return_value="SAME",
        ):
            with pytest.raises(ValueError):
                result = DicomTagStr("ValueErrorRaisePlease")


def test_dicom_file_profile_replace_private_tag(dicom_file):
    PRIVATE_TAGS = (0x00090001, 0x00090010)

    # Verify that subclass is registered
    file_profile_names = FileProfile.profile_names()
    assert len(file_profile_names) > 0
    assert "dicom" in file_profile_names

    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    # Seed initial values
    dcm = pydicom.dcmread(path)
    for private_tag in PRIVATE_TAGS:
        dcm.add_new(private_tag, "PN", "PRIVATE DATA")
        assert private_tag in dcm
        assert dcm[private_tag].value == "PRIVATE DATA"
    dcm.save_as(path)

    profile = DicomFileProfile()
    config = {
        "fields": [
            {"name": "00090001", "replace-with": "REDACTED"},
            {"name": "(0009, 0010)", "remove": True},
        ]
    }
    profile.load_config(config)

    # apply field operations
    profile.process_files(test_fs, test_fs, [basename])

    # Reload and verify
    dst_path = os.path.join(
        dirname, "1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.MR.dcm"
    )
    dcm = pydicom.dcmread(dst_path)
    assert dcm[(0x0009, 0x0001)].value == "REDACTED"
    assert (0x0009, 0x0010) not in dcm


def test_dicom_file_profile_load_record_returns_none_false_for_non_dicoms_if_deid_profile_is_none(
    efile,
):
    path = efile
    print(path)
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)
    profile = DicomFileProfile()
    profile.deid_name = "none"
    state = {"series_uid": None, "session_uid": None, "sop_uids": set()}

    result = profile.load_record(state, test_fs, basename)
    assert result == (None, False)


def test_dicom_file_profile_load_record_returns_none_false_for_non_dicoms_if_deid_profile_is_not_none(
    efile,
):
    path = efile
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)
    profile = DicomFileProfile()
    profile.deid_name = "dicom"
    state = {"series_uid": None, "session_uid": None, "sop_uids": set()}

    result = profile.load_record(state, test_fs, basename)
    assert result == (None, False)


def test_dicom_file_profile_load_record_returns_true_false_for_invalid_dicoms_if_deid_profile_is_none(
    dicom_file,
):
    path = dicom_file("invalid", "invalid.dcm")
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)
    profile = DicomFileProfile()
    profile.deid_name = "none"
    state = {"series_uid": None, "session_uid": None, "sop_uids": set()}

    result = profile.load_record(state, test_fs, basename)
    assert result == (True, False)


def test_dicom_file_profile_load_record_returns_none_false_for_invalid_dicoms_if_deid_profile_is_not_none(
    dicom_file,
):
    path = dicom_file("invalid", "invalid.dcm")
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)
    profile = DicomFileProfile()
    profile.deid_name = "dicom"
    state = {"series_uid": None, "session_uid": None, "sop_uids": set()}

    result = profile.load_record(state, test_fs, basename)
    assert result == (None, False)


def test_dicom_file_profile_load_record_returns_loaded_record_for_valid_dicoms_if_deid_profile_is_none(
    dicom_file,
):
    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)
    profile = DicomFileProfile()
    profile.deid_name = "none"
    state = {"series_uid": None, "session_uid": None, "sop_uids": set()}

    result = profile.load_record(state, test_fs, basename)
    assert isinstance(result[0], pydicom.dataset.Dataset)


def test_dicom_file_profile_load_record_returns_loaded_record_for_valid_dicoms_if_deid_profile_is_not_none(
    dicom_file,
):
    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)
    profile = DicomFileProfile()
    profile.deid_name = "dicom"
    state = {"series_uid": None, "session_uid": None, "sop_uids": set()}

    result = profile.load_record(state, test_fs, basename)
    assert isinstance(result[0], pydicom.dataset.Dataset)


def test_dicom_file_profile_replace_private_tag_sys_exit_and_log_error(
    dicom_file, caplog
):
    PRIVATE_TAGS = (0x00090001,)

    # Verify that subclass is registered
    file_profile_names = FileProfile.profile_names()
    assert len(file_profile_names) > 0
    assert "dicom" in file_profile_names

    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    # Check non existence
    dcm = pydicom.dcmread(path)
    for private_tag in PRIVATE_TAGS:
        assert private_tag not in dcm
    dcm.save_as(path)

    profile = DicomFileProfile()
    profile.add_field(
        DeIdField.factory({"name": "00090001", "replace-with": "REDACTED"})
    )

    # apply field operations
    with caplog.at_level(logging.ERROR):
        with pytest.raises(SystemExit) as exc:
            profile.process_files(test_fs, test_fs, [basename])
    assert "Invalid replace-with action" in caplog.messages[0]
    assert exc.value.code == 1


def test_deid_profile_with_tag(dicom_file, template_file):
    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    profile = deidentify.load_deid_profile(template_file("nametag-profile"))
    profile.process_packfile("dicom", test_fs, test_fs, [basename])

    # Verify the de-id
    dst_path = os.path.join(
        dirname, "1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.MR.dcm"
    )
    dcm = pydicom.dcmread(dst_path)
    assert not hasattr(dcm, "PatientID")
    assert not hasattr(dcm, "PatientName")
    assert not hasattr(dcm, "PatientBirthDate")


def test_get_or_create_field_if_sequence_can_create_element(dicom_file):
    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    dcm = pydicom.read_file(path)
    profile = DicomFileProfile()
    assert "DirectoryRecordSequence" not in dcm

    profile._get_or_create_data_element_if_sequence(dcm, ["DirectoryRecordSequence"])
    assert "DirectoryRecordSequence" in dcm


def test_get_or_create_field_if_sequence_can_create_sequence(dicom_file):
    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    dcm = pydicom.read_file(path)
    profile = DicomFileProfile()
    assert "DirectoryRecordSequence" not in dcm

    profile._get_or_create_data_element_if_sequence(dcm, ["DirectoryRecordSequence", 1])
    assert "DirectoryRecordSequence" in dcm
    assert type(dcm["DirectoryRecordSequence"][1]) is pydicom.Dataset

    profile._get_or_create_data_element_if_sequence(
        dcm, ["DirectoryRecordSequence", 1, "PatientID"]
    )
    assert "DirectoryRecordSequence" in dcm
    assert type(dcm["DirectoryRecordSequence"][1]) is pydicom.Dataset
    assert "PatientID" in dcm["DirectoryRecordSequence"][1]


def test_get_or_create_field_if_sequence_can_extend_sequence(dicom_file):
    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    dcm = pydicom.read_file(path)
    profile = DicomFileProfile()
    assert "CodeValue" in dcm["AnatomicRegionSequence"][0]
    with pytest.raises(IndexError):
        _ = dcm["AnatomicRegionSequence"][1]

    profile._get_or_create_data_element_if_sequence(
        dcm, ["AnatomicRegionSequence", 1, "CodeValue"]
    )
    assert "CodeValue" in dcm["AnatomicRegionSequence"][1]


def test_deid_profile_with_nested_data_element(dicom_file, template_file):
    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    profile = deidentify.load_deid_profile(template_file("nestedname-profile"))
    profile.process_packfile("dicom", test_fs, test_fs, [basename])

    # Verify the de-id
    dst_path = os.path.join(
        dirname, "1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.MR.dcm"
    )
    dcm = pydicom.dcmread(dst_path)
    assert dcm["AnatomicRegionSequence"][0]["CodeValue"].value == "REPLACED"
    assert dcm["00082218"][0]["00080102"].value == "REPLACED"
    with pytest.raises(KeyError):
        _ = dcm["AnatomicRegionSequence"][0]["00080104"]
    assert dcm["AnatomicRegionSequence"][0]["PatientID"].value == "REPLACED"


def test_deid_create_field_with_nested_data_element_and_replace_with(dicom_file):
    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    dcm = pydicom.dcmread(path)
    with pytest.raises(IndexError):
        assert dcm["AnatomicRegionSequence"][1]["CodeValue"]
    with pytest.raises(KeyError):
        assert dcm["AnatomicRegionSequence"][0]["AnatomicRegionSequence"][0][
            "CodeValue"
        ]
    profile = DicomFileProfile()
    config = {
        "patient-age-from-birthdate": True,
        "patient-age-units": "Y",
        "fields": [
            {"name": "AnatomicRegionSequence.1.CodeValue", "replace-with": "REPLACED"},
            {
                "name": "AnatomicRegionSequence.0.AnatomicRegionSequence.0.CodeValue",
                "replace-with": "REPLACED",
            },
        ],
    }
    profile.load_config(config)
    profile.process_files(test_fs, test_fs, [basename])

    # Verify the de-id
    dst_path = os.path.join(
        dirname, "1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.MR.dcm"
    )
    dcm = pydicom.dcmread(dst_path)
    assert dcm["AnatomicRegionSequence"][1]["CodeValue"].value == "REPLACED"
    assert (
        dcm["AnatomicRegionSequence"][0]["AnatomicRegionSequence"][0]["CodeValue"].value
        == "REPLACED"
    )


def test_replacewith_fails_if_nested_sequence_key_unknown(dicom_file):
    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    OSFS(dirname)

    profile = DicomFileProfile()
    config = {
        "patient-age-from-birthdate": True,
        "patient-age-units": "Y",
        "fields": [
            {"name": "AnatomicRegionSequence.0.NotAKeyword", "replace-with": "REPLACED"}
        ],
    }
    with pytest.raises(ValueError):
        profile.load_config(config)


@pytest.mark.parametrize(
    "action",
    [
        c.key
        for c in DeIdField.__subclasses__()
        if c.key not in ["replace-with", "regex-sub"]
    ],
)
def test_all_actions_but_replace_with_do_not_raise_if_tag_not_defined(
    dicom_file, action
):
    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    profile = DicomFileProfile()
    profile.load_config(
        {
            "fields:": [
                {"name": "DoesNotExist", action: True},
                {"name": "AnatomicRegionSequence.0.DoesNotExist", action: True},
                {"name": "AnatomicRegionSequence.*.DoesNotExist", action: True},
            ]
        }
    )
    profile.process_files(test_fs, test_fs, [basename])

    dcm = pydicom.dcmread(os.path.join(dirname, basename))
    assert not hasattr(dcm, "DoesNotExist")
    assert not hasattr(dcm["AnatomicRegionSequence"][0], "DoesNotExist")


def test_nested_field_with_double_digit_index(dicom_file):
    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    dcm = pydicom.dcmread(path)
    for i in range(101):
        dcm.AnatomicRegionSequence.append(dcm["AnatomicRegionSequence"][0])
    dcm.save_as(path)

    profile = DicomFileProfile()
    config = {
        "fields": [
            {"name": "AnatomicRegionSequence.1.CodeValue", "replace-with": "REPLACED"},
            {"name": "AnatomicRegionSequence.10.CodeValue", "replace-with": "REPLACED"},
            {
                "name": "AnatomicRegionSequence.100.CodeValue",
                "replace-with": "REPLACED",
            },
        ]
    }
    profile.load_config(config)
    profile.process_files(test_fs, test_fs, [basename])

    dcm = pydicom.dcmread(
        os.path.join(
            dirname, "1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.MR.dcm"
        )
    )
    assert dcm.AnatomicRegionSequence[1].CodeValue == "REPLACED"
    assert dcm.AnatomicRegionSequence[10].CodeValue == "REPLACED"
    assert dcm.AnatomicRegionSequence[100].CodeValue == "REPLACED"


def test_deid_profile_validate_with_enhanced(template_file):
    profile = DicomFileProfile()
    profile.load_config(
        {
            "fields": [
                {"name": "SeriesDescription", "replace-with": "REDACTED"},
                {"name": "00080034", "remove": True},
                {"name": "DoesNotExist", "remove": True},
                {"name": "00082218.0.00080102", "replace-with": "REDACTED"},
                # PhotometricInterpretation does not exist in dataset
                {
                    "name": "AnatomicRegionSequence.0.PhotometricInterpretation",
                    "remove": True,
                },
                {
                    "name": "AnatomicRegionSequence.0.00080104",
                    "replace-with": "REDACTED",
                },
                {
                    "name": "AnatomicRegionSequence.1.PatientID",
                    "replace-with": "REDACTED",
                },
            ]
        }
    )
    profile.validate(enhanced=True)


def test_deid_profile_validate_with_enhanced_raised(template_file):
    profile = DicomFileProfile()
    profile.load_config(
        {
            "fields": [
                {
                    "name": "SeriesDescription.0.SeriesDescription",
                    "replace-with": "REDACTED",
                }
            ]
        }
    )
    with pytest.raises(Exception):
        profile.validate(enhanced=True)


@pytest.mark.parametrize(
    "cfg,no_errs",
    [
        ([{"keep": [0, 1, 2, 3], "when": "test", "foo": "bar"}], 1),
        ([{"keep": [0, 1, 2], "when": "test"}], 1),
        ([{"remove": [0, 1, 2], "when": "test"}], 1),
        ([{"remove": [0, 1, 2], "when": "test", "foo": "bar"}], 2),
        ([{"remove": [0, 1, 2, 3], "when": "test"}], 0),
        ([{"remove": [0, 1, 2, 3], "foo": "bar"}], 1),
    ],
)
def test_deid_profile_validate_pixels(cfg, no_errs):
    from flywheel_migration.deidentify.dicom_file_profile import DEID_PACKAGE_FOUND

    profile = DicomFileProfile()
    profile.load_config({"alter_pixels": {"pixels": cfg}})

    errors = profile.validate()
    assert len(errors) == no_errs
    assert DEID_PACKAGE_FOUND is True


def test_deid_profile_does_not_validate_when_deid_package_is_not_installed():
    with patch(
        "flywheel_migration.deidentify.dicom_file_profile.DEID_PACKAGE_FOUND", new=False
    ):
        profile = DicomFileProfile()
        profile.load_config(
            {
                "alter_pixels": {
                    "pixels": [
                        {"remove": [0, 0, 10, 10], "when": "contains modality MR"}
                    ],
                    "detect": ["dicom"],
                }
            }
        )
        errors = profile.validate()
        assert len(errors) == 1
        assert (
            "Pixel actions requires pydicom deid package which was not found."
            in errors[0]
        )


def test_load_deid_dicom_profile(dicom_file, template_file):
    profile = deidentify.load_deid_profile(template_file("dicom-profile"))

    assert profile.file_profiles[0].patient_age_from_birthdate is True
    assert profile.file_profiles[0].patient_age_units == "Y"
    assert profile.file_profiles[0].uid_numeric_name == "2.16.840.1.114570.2.2"
    assert profile.file_profiles[0].uid_prefix_fields == 7


def test_process_filename_groups_deals_with_dateformat():
    profile = DicomFileProfile()
    format_kws = {
        "date1": "20200420",
        "date2": "1982-11-18",
        "date3": "2020/01/30",
        "datetime1": "20100205113527",
        "datetime2": "20100101 16h30",
    }
    groups = [
        {"name": "date1", "increment-date": True},
        {"name": "date2", "increment-date": True},
        {"name": "date3", "increment-date": True},
        {"name": "datetime1", "increment-datetime": True},
        {"name": "datetime2", "increment-datetime": True},
    ]
    format_kws = profile._process_filename_groups(format_kws, groups)
    assert format_kws["date1"] == "20200420"
    assert format_kws["date2"] == "19821118"
    assert format_kws["date3"] == "20200130"
    assert format_kws["datetime1"] == "20100205113527.000000"
    assert format_kws["datetime2"] == "20100101163000.000000"


def test_can_set_attribute_to_record_base_on_filename_regex_group(dicom_file):
    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    dcm = pydicom.dcmread(path)  # that's a record example
    test_name = "useful-info.not-so-useful.dcm"  # input filename used for test
    profile = DicomFileProfile()
    config = {
        "filenames": [
            {
                "output": "{keep1}_{SOPInstanceUID}.dcm",
                "input-regex": r"^(?P<keep1>[\w-]+)\..+dcm$",
            }
        ]
    }
    profile.load_config(config)
    profile.set_filenames_attributes(dcm, test_name)
    assert hasattr(dcm, f"{profile.filename_field_prefix}_filename0_keep1")


def test_datetime_and_date_groups_get_converted_when_increment_is_true(dicom_file):
    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    dcm = pydicom.dcmread(path)
    test_name = "2010-01-01_2010-01-01 4:30pm.dcm"

    profile = DicomFileProfile()
    config = {
        "date-increment": 10,
        "filenames": [
            {
                "output": "{date}_{datetime}.dcm",
                "input-regex": r"^(?P<date>\d{4}-\d{2}-\d{2})_(?P<datetime>[\d\s\w:-]+).dcm$",
                "groups": [
                    {"name": "date", "increment-date": True},
                    {"name": "datetime", "increment-datetime": True},
                ],
            }
        ],
    }
    profile.load_config(config)
    profile.set_filenames_attributes(dcm, test_name)

    assert hasattr(dcm, f"{profile.filename_field_prefix}_filename0_date")
    assert dcm.get(f"{profile.filename_field_prefix}_filename0_date") == "20100101"
    assert hasattr(dcm, f"{profile.filename_field_prefix}_filename0_datetime")
    assert (
        dcm.get(f"{profile.filename_field_prefix}_filename0_datetime")
        == "20100101163000.000000"
    )


def test_dicom_file_with_filename_generate_simple_dst_path(dicom_file):
    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    dcm = pydicom.dcmread(path)
    tmp_name = "useful-info.not-so-useful.dcm"
    profile = DicomFileProfile()
    config = {
        "filenames": [
            {
                "output": "{keep1}_{SOPInstanceUID}.dcm",
                "input-regex": r"^(?P<keep1>[\w-]+)\..+dcm$",
            }
        ]
    }
    profile.load_config(config)
    profile.set_filenames_attributes(
        dcm, tmp_name
    )  # dcm is extended with attributes coming from the filenames groups

    dst_path = profile.get_dest_path({}, dcm, tmp_name)

    assert (
        dst_path
        == "useful-info_1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm"
    )


def test_dicom_file_with_slash_in_modality_generate_dst_path(dicom_file):
    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    dcm = pydicom.dcmread(path)
    dcm.Modality = "MR/CT"
    tmp_name = "useful-info.not-so-useful.dcm"
    profile = DicomFileProfile()

    dst_path = profile.get_dest_path({}, dcm, tmp_name)

    assert (
        dst_path
        == "1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.MR_CT.dcm"
    )


def test_load_deid_profile_with_filename_group_not_in_dicom_keyword_returns_error():
    profile = DicomFileProfile()
    profile.filenames = [{"output": "{DoesNotExist}_{SOPInstanceUID}.dcm"}]

    errors = profile.validate()
    assert (
        "Filename output invalid. Group not in Dicom keyword or in regex groups: DoesNotExist"
        in errors
    )


def test_log_warning_if_dst_path_cannot_be_generated_from_filename(dicom_file, caplog):
    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    dcm = pydicom.dcmread(path)
    tmp_name = "useful-info.not-so-useful.dcm"
    profile = DicomFileProfile()
    config = {
        "filenames": [
            {
                "output": "{nomatch}_{SOPInstanceUID}.dcm",
                "input-regex": r"^(?P<nomatch>[\d-]+)\..+dcm$",
            }
        ]
    }
    profile.load_config(config)
    profile.set_filenames_attributes(
        dcm, tmp_name
    )  # dcm is extended with attributes coming from the filenames groups
    with caplog.at_level(logging.WARNING):
        _ = profile.get_dest_path({}, dcm, tmp_name)
        assert any(["IGNORING" in message for message in caplog.messages])


def test_can_take_action_on_group_in_filenames(dicom_file):
    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    dirname, _ = os.path.split(path)
    basename = "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164_2010-09-18_05-29-2015 05:50.dcm"
    os.rename(path, os.path.join(dirname, basename))
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)
    profile = DicomFileProfile()
    config = {
        "date-increment": -17,
        "filenames": [
            {
                "output": "{group1}_{date1}_{datetime1}.dcm",
                "input-regex": r"^MR.(?P<group1>[\d\.]+)_(?P<date1>\d{4}-\d{2}-\d{2})_(?P<datetime1>[\d\s:-]+).dcm$",
                "groups": [
                    {"name": "group1", "hashuid": True},
                    {"name": "date1", "increment-date": True},
                    {"name": "datetime1", "increment-datetime": True},
                ],
            }
        ],
    }

    profile.load_config(config)
    profile.process_files(test_fs, test_fs, [basename])

    # Hashed value should be: 1.2.840.113619.154011.623122.575183.251492.162351.542113.164
    dst_path = os.path.join(
        dirname,
        "1.2.840.113619.154011.623122.575183.251492.162351.542113.164_20100901_20150512055000.000000.dcm",
    )
    assert pydicom.dcmread(dst_path)


def test_filenames_can_handle_multiple_entries(dicom_file):
    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    dirname, _ = os.path.split(path)
    basename = "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm"
    os.rename(path, os.path.join(dirname, basename))
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)
    profile = DicomFileProfile()
    config = {
        "date-increment": -17,
        "filenames": [
            {
                "output": "{nomatch}.dcm",
                "input-regex": r"^MR.(?P<nomatch>[\w\.]+).dcm$",
            },
            {"output": "{match}.dcm", "input-regex": r"^MR.(?P<match>[\d\.]+).dcm$"},
        ],
    }

    profile.load_config(config)
    profile.process_files(test_fs, test_fs, [basename])

    # Hashed value should be: 1.2.840.113619.154011.623122.575183.251492.162351.542113.164
    dst_path = os.path.join(
        dirname, "1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm"
    )
    assert pydicom.dcmread(dst_path)


def test_dicom_file_profile_with_nested_wild_card_tag(dicom_file):
    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)
    dcm = pydicom.dcmread(path)
    dcm["AnatomicRegionSequence"].value.append(
        pydicom.dataset.Dataset()
    )  # appending another element so that len == 2
    assert not hasattr(dcm, "DimensionOrganizationSequence")
    dcm.save_as(path)
    profile = DicomFileProfile()

    # wild nest
    profile.load_config(
        {
            "fields": [
                {
                    "name": "AnatomicRegionSequence.*.CodeValue",
                    "replace-with": "REPLACED",
                },
                {"name": "DimensionOrganizationSequence.*.CodeValue", "remove": True},
            ]
        }
    )
    profile.validate()
    profile.process_files(test_fs, test_fs, [basename])

    # Verify the de-id
    dst_path = os.path.join(
        dirname, "1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.MR.dcm"
    )
    dcm = pydicom.dcmread(dst_path)
    assert dcm["AnatomicRegionSequence"][0]["CodeValue"].value == "REPLACED"
    assert (
        dcm["AnatomicRegionSequence"][1]["CodeValue"].value == "REPLACED"
    )  # created with replace-with
    assert not hasattr(dcm, "DimensionOrganizationSequence")


def test_file_profile_can_read_save_file_filter():
    profile = FileProfile()
    config = {"file-filter": ["*.ext"]}
    profile.load_config(config)

    assert profile.file_filter == ["*.ext"]

    result = profile.to_config()
    assert result["file-filter"] == ["*.ext"]


def test_dicom_file_profile_with_regex_field_name(dicom_file):
    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    dcm = pydicom.dcmread(path)
    assert "PatientAge" in dcm
    assert "PatientName" in dcm
    assert dcm["StudyDate"].value == "20180124"
    assert dcm["SeriesDate"].value == "20180124"
    assert dcm["AcquisitionDate"].value == "20180124"
    dcm["AnatomicRegionSequence"].value.append(
        pydicom.dataset.Dataset()
    )  # appending another element so that len == 2
    dcm.save_as(path)

    profile = DicomFileProfile()
    profile.load_config(
        {
            "date-increment": 10,
            "fields": [
                {"regex": "Patient.*", "remove": True},
                {"regex": ".*Date", "increment-date": True},
            ],
        }
    )
    profile.process_files(test_fs, test_fs, [basename])

    # Verify the de-id
    dst_path = os.path.join(
        dirname, "1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.MR.dcm"
    )
    dcm = pydicom.dcmread(dst_path)
    assert "PatientAge" not in dcm
    assert "PatientName" not in dcm
    assert dcm["StudyDate"].value == "20180203"
    assert dcm["SeriesDate"].value == "20180203"
    assert dcm["AcquisitionDate"].value == "20180203"


def test_file_profile_not_regex_compatible_raised():
    profile = DicomFileProfile()
    profile.regex_compatible = False
    with pytest.raises(ValueError):
        profile.load_config({"fields": [{"regex": "Patient.*", "remove": True}]})


def test_datetime_increment_missing_frac_seconds(dicom_file):
    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    dcm = pydicom.dcmread(path)
    setattr(dcm, "AcquisitionDateTime", "20110411095020")
    dcm.save_as(path)

    profile = DicomFileProfile()
    profile.load_config(
        {
            "date-increment": 10,
            "fields": [{"regex": ".*DateTime", "increment-datetime": True}],
        }
    )
    profile.process_files(test_fs, test_fs, [basename])
    # Verify the de-id
    dst_path = os.path.join(
        dirname, "1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.MR.dcm"
    )
    dcm = pydicom.dcmread(dst_path)
    assert dcm["AcquisitionDateTime"].value == "20110421095020.000000"


@pytest.mark.parametrize("decode", [True, False])
def test_dicom_profile_can_process_dicom_with_invalid_VR(decode, dicom_file):
    path = dicom_file(
        "invalid",
        "invalid_VR_in_UN_sequence.dcm",
    )
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    profile = DicomFileProfile()
    profile.load_config(
        {
            "decode": decode,
            "fields": [
                {
                    "name": "CTDIPhantomTypeCodeSequence.0.CodeValue",
                    "replace-with": "pfew",
                }
            ],
        }
    )
    profile.process_files(test_fs, test_fs, [basename])
    dst_path = os.path.join(dirname, basename)
    dcm = pydicom.dcmread(dst_path, force=True)
    assert dcm["CTDIPhantomTypeCodeSequence"][0]["CodeValue"].VR == "SH"
    assert dcm["CTDIPhantomTypeCodeSequence"][0]["CodeValue"].value == "pfew"


def test_replace_with_can_upsert_if_tag_does_not_exist(dicom_file, tmpdir):
    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    # removing a few fields
    dcm = pydicom.dcmread(path)
    if "PatientID" in dcm:
        del dcm["PatientID"]
    if 0x00180010 in dcm:
        del dcm[0x00180010]
    if 0x0008103E in dcm:
        del dcm[0x0008103E]
    if 0x000910E8 in dcm:
        del dcm[0x000910E8]
    dcm.save_as(path)

    # setting up profile with log enabled
    deid_log_path = os.path.join(tmpdir, "deidlog.csv")
    config = {
        "deid-log": deid_log_path,
        "dicom": {
            "fields": [
                {"name": "PatientID", "replace-with": "XXX"},
                {"name": "0x00180010", "replace-with": "XXX"},
                {"name": "(0008,103e)", "replace-with": "XXX"},
                {"name": "AnatomicRegionSequence.1.CodeValue", "replace-with": "XXX"},
                {"name": '(0009, "GEMS_IDEN_01", E8)', "replace-with": "XXX"},
            ]
        },
    }
    profile = deidentify.DeIdProfile()
    profile.load_config(config)
    profile.initialize()

    profile.process_packfile("dicom", test_fs, test_fs, [basename])

    dst_path = os.path.join(
        dirname, "1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.MR.dcm"
    )
    dcm = pydicom.dcmread(dst_path, force=True)
    assert dcm.PatientID == "XXX"
    assert dcm[0x00180010].value == "XXX"
    assert dcm[0x0008103E].value == "XXX"
    assert dcm.AnatomicRegionSequence[1].CodeValue == "XXX"
    assert dcm[0x000910E8].value == "XXX"


@pytest.mark.parametrize(
    "keyword,value,VR,error",
    [
        ("PatientID", 1, "LO", True),
        ("Rows", "toto", "US", True),
        ("SOPClassUID", 1, "UI", True),
        ("StudyDate", 20191023, "DA", True),
        ("PatientName", "Lastname^Firstname", "PN", False),
        ("SOPClassUID", "not.a.good.id.but.you.know", "UI", False),
        ("OtherPatientIDsSequence", "", "SQ", False),
        ("PatientName", "", "PN", False),
    ],
)
def test_profile_validate_errors_on_incorrect_value_type_for_replace_with(
    keyword, value, VR, error
):
    profile = DicomFileProfile()
    profile.load_config({"fields": [{"name": keyword, "replace-with": value}]})
    errors = profile.validate()
    if error:
        assert len(errors) == 1
        assert (
            errors[0]
            == f"Incorrect value type for Dicom element {keyword} (VR={VR}): {type(value).__name__}"
        )
    else:
        assert len(errors) == 0


@pytest.mark.parametrize(
    "keyword,error_length,VR",
    [
        ("PatientID", 0, "LO"),
        ("Rows", 1, "US"),
    ],
)
def test_profile_validate_errors_on_incorrect_VR_for_hash(keyword, error_length, VR):
    profile = DicomFileProfile()
    profile.load_config({"fields": [{"name": keyword, "hash": True}]})
    errors = profile.validate()
    assert len(errors) == error_length
    if errors:
        assert f"{keyword} cannot be hashed" in errors[0]


def test_extended_dicom_profile(dicom_file, template_file):
    profile = deidentify.load_deid_profile(template_file("dicom-profile-extended"))
    errors = profile.validate()
    assert len(errors) == 0


@pytest.mark.parametrize(
    "config,expected",
    [
        ({"name": "PatientID", "remove": True}, ["PatientID"]),
        (
            {"name": "AnatomicRegionSequence.0.CodeValue", "remove": True},
            ["AnatomicRegionSequence.0.CodeValue"],
        ),
        (
            {"name": "AnatomicRegionSequence.*.CodeValue", "remove": True},
            [
                "00082218.0.00080100",
                "00082218.1.00080100",
            ],
        ),
        (
            {"regex": ".*Date.*", "remove": True},
            [
                "AcquisitionDate",
                "ContentDate",
                "PatientBirthDate",
                "PerformedProcedureStepStartDate",
                "SeriesDate",
                "StudyDate",
            ],
        ),
    ],
)
def test_list_fieldnames(config, expected, dicom_file):
    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    dcm = pydicom.dcmread(path)

    # Adding a element to AnatomicRegionSequence for test purposes
    dcm["AnatomicRegionSequence"].value.append(dcm["AnatomicRegionSequence"][0])

    profile = DicomFileProfile()
    profile.load_config({"fields": [config]})
    fieldnames = profile.fields[0].list_fieldname(dcm)
    assert fieldnames == expected


@pytest.mark.parametrize(
    "fieldname,dicom_tag,is_sequence",
    [
        ("Unknown", None, False),
        ("PatientID", None, False),
        ("(0010, 0020)", pydicom.tag.Tag(0x00100020), False),
        ("(0010,0020)", pydicom.tag.Tag(0x00100020), False),
        ("00100020", pydicom.tag.Tag(0x00100020), False),
        ("0x00100020", pydicom.tag.Tag(0x00100020), False),
        (
            "AnatomicRegionSequence.0.CodeValue",
            ["AnatomicRegionSequence", 0, "CodeValue"],
            True,
        ),
        ("00082218.0.CodeValue", ["AnatomicRegionSequence", 0, "CodeValue"], True),
        ("0x00082218.0.00080100", ["AnatomicRegionSequence", 0, "CodeValue"], True),
        (
            "AnatomicRegionSequence.*.CodeValue",
            ["AnatomicRegionSequence", "*", "CodeValue"],
            True,
        ),
    ],
)
def test_DicomTagStr(fieldname, dicom_tag, is_sequence):  # noqa: F811
    fieldname = DicomTagStr(fieldname)
    assert fieldname._dicom_tag == dicom_tag
    assert fieldname._is_sequence == is_sequence


def test_jitter_for_dicom(dicom_file, caplog):
    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)
    original = pydicom.dcmread(path)

    profile = DicomFileProfile()
    profile.load_config(
        {
            "jitter-range": 2,
            "fields": [
                {
                    "name": "AcquisitionNumber",  # VR = IS, original = 10
                    "jitter": True,
                    "jitter-type": "int",
                    "jitter-max": 10,
                },
                {
                    "name": "SeriesNumber",  # VR = IS, original = 1
                    "jitter": True,
                    "jitter-type": "float",
                    "jitter-min": 1.0,
                },
                {
                    "name": "SliceLocation",  # VR = DS
                    "jitter": True,
                },
                {
                    "name": "Columns",  # VR = US (unsigned short), cannot be negative, original 64
                    "jitter": True,
                    "jitter-type": "int",
                },
                {
                    "name": 0x00271033,  # VR = UL (unsigned long), original 8192
                    "jitter": True,
                    "jitter-type": "int",
                },
            ],
        }
    )
    with patch("random.uniform", return_value=-0.4):
        with patch("random.randint", return_value=-1):
            profile.process_files(test_fs, test_fs, [basename])
    dst_path = os.path.join(
        dirname, "1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.MR.dcm"
    )
    dcm = pydicom.dcmread(dst_path, force=True)
    assert dcm.SliceLocation == original.SliceLocation - 2 * 0.4
    assert dcm.AcquisitionNumber == original.AcquisitionNumber - 1
    assert dcm.SeriesNumber == original.SeriesNumber  # jitter_min applied
    # check casting to integer
    assert dcm.Columns == original.Columns - 1
    assert dcm[0x00271033].value == original[0x00271033].value - 1
    assert "Casting SeriesNumber to integer to match VR=IS" in caplog.messages[0]

    # testing that US and UL are positive
    with patch(
        "random.randint", return_value=-10000
    ):  # jitter will bring to negative value
        profile.process_files(test_fs, test_fs, [basename])
    dcm = pydicom.dcmread(dst_path, force=True)
    assert dcm[0x00271033].value == 0
    assert dcm.Columns == 0
    assert "Jitter on Columns yielded a negative" in caplog.messages[2]


def test_validate_hash_op_errors_on_uid():
    config = {
        "fields": [
            # fail hashing UID field
            {"name": "StudyInstanceUID", "hash": True},
            # fail hashing UID field
            {"name": "SeriesInstanceUID", "hashuid": True},
            # hash normal field
            {"name": "PatientID", "hash": True},
        ]
    }
    profile = DicomFileProfile()
    profile.load_config(config)
    errors = profile.validate()
    assert len(errors) == 1
    assert "StudyInstanceUID cannot be hashed - VR not compatible (UI)" in errors[0]


@pytest.mark.parametrize(
    "name,is_private",
    [
        ("(0009, GEMS_IDEN_01, 02)", True),
        ("(0009,GEMS_IDEN_01,02)", True),
        ("( 0009 , GEMS_IDEN_01 , 02 ) ", True),
        ('(0009, "GEMS_IDEN_01", 02)', True),
        ("(0009, GEMS-IDEN-01, 02)", True),
        (r'(0009, "GEMS\IDEN-01", 02)', False),
        ("(0009, GEMS)(IDEN-01, 02)", False),
        ('(0009, "GEMS_IDEN_01", 02)(0009, "GEMS_IDEN_01", ``02)', False),
        # private creator can have comma
        ("(7E01, 'HOLOGIC, Inc.', 01)", True),
        ("(7E01, 'HOLOGIC, Inc.', 01)", True),
        ("(7E01, HOLOGIC, Inc., 01)", True),
    ],
)
def test_parse_private_tag(name, is_private):
    fieldname = DicomTagStr(name)
    assert fieldname.is_private == is_private


def test_processing_of_private_tag(dicom_file):
    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    config = {
        "fields": [
            # tag exists
            {"name": "(0009, GEMS_IDEN_01, 02)", "remove": True},
            # tag does not exist
            {"name": "(0009, GEMS_IDEN_01, e7)", "remove": True},
            # tag exists
            {"name": "(0009, GEMS_IDEN_01, 04)", "replace-with": "XXX"},
            # tag does not exist but is known and group exists, upsert
            {"name": "(0009, GEMS_IDEN_01, e8)", "replace-with": "XXX"},
            # group does not exists
            {"name": "(004b, GEMS_HINO_CT_01, 01)", "replace-with": 1.0},
            # Add GEMS_PETD_01 to next available block in 0009 group (i.e. 0011)
            {"name": "(0009, GEMS_PETD_01, 02)", "replace-with": "XXX"},
        ]
    }
    profile = DicomFileProfile()
    profile.load_config(config)
    profile.process_files(test_fs, test_fs, [basename])

    dst_path = os.path.join(
        dirname, "1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.MR.dcm"
    )
    dcm = pydicom.dcmread(dst_path, force=True)
    assert 0x00091002 not in dcm
    assert 0x000910E7 not in dcm
    assert dcm[0x00091004].value == "XXX"
    assert dcm[0x000910E8].value == "XXX"
    assert dcm[0x004B1001].value == 1.0
    assert dcm[0x00090011].value == "GEMS_PETD_01"
    assert dcm[0x00091102].value == "XXX"


def test_validate_private_tag_fieldname():
    profile = DicomFileProfile()
    profile.load_config({"fields": [{"name": "(0020, TOTO, 10)", "keep": True}]})
    errors = profile.validate()
    assert len(errors) == 1
    assert "Not a private tag" in errors[0]

    profile = DicomFileProfile()
    profile.load_config({"fields": [{"name": "(0021, " ", 10)", "keep": True}]})
    errors = profile.validate()
    assert len(errors) == 1
    assert "Private creator is empty for" in errors[0]


def test_pydicom_private_dictionaries_is_extended():
    de = get_private_entry(0x00411001, "SIEMENS MI RWVM SUV")
    assert de


def test_deid_with_recurse_sequence(dicom_file):
    path = dicom_file(
        "pydicom",
        "CT_small.dcm",
    )
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)
    dcm = pydicom.dcmread(path)
    # Adding private tags to a Sequence element
    block = dcm.OtherPatientIDsSequence[0].private_block(0x000B, "Pffew", create=True)
    block.add_new(0x01, "SH", "Pouet")
    pydicom.datadict.add_private_dict_entry(
        "Pffew", 0x000B0001, "SH", "A dump tag", VM="1"
    )
    dcm.save_as(path)

    config = {
        "recurse-sequence": True,
        "fields": [
            # PatientID is also found in OtherPatientIDsSequence
            {"name": "PatientID", "replace-with": "XXX"},
            # Testing with private tag
            {"name": "(000B, Pffew, 01)", "replace-with": "PouetWasHere"},
        ],
    }
    profile = DicomFileProfile()
    profile.load_config(config)

    field = profile.field_map.get("PatientID")
    assert field.recurse_sequence is True
    assert field.list_fieldname(dcm) == [
        "PatientID",
        "OtherPatientIDsSequence.0.PatientID",
        "OtherPatientIDsSequence.1.PatientID",
    ]
    field = profile.field_map.get("(000B, Pffew, 01)")
    assert field.recurse_sequence is True
    assert field.list_fieldname(dcm) == ["(000B, Pffew, 01)", "00101002.0.000b1001"]

    profile.process_files(test_fs, test_fs, [basename])
    dst_path = os.path.join(
        dirname, "1.3.6.1.4.1.5962.1.1.1.1.1.20040119072730.12322.CT.dcm"
    )
    dcm = pydicom.dcmread(dst_path, force=True)
    assert dcm.PatientID == "XXX"
    assert dcm.OtherPatientIDsSequence[0].PatientID == "XXX"
    assert dcm.OtherPatientIDsSequence[1].PatientID == "XXX"
    # got created
    assert dcm[0x000B1001].value == "PouetWasHere"
    # was replaced
    assert dcm.OtherPatientIDsSequence[0][0x000B1001].value == "PouetWasHere"


def test_load_save_recurse_sequence_from_config():
    profile = DicomFileProfile()
    config = {"recurse-sequence": True}

    profile.load_config(config)
    assert profile.recurse_sequence is True

    config = profile.to_config()
    assert config["recurse-sequence"] is True


def test_recurse_sequence_validate():
    # pass validation when no field type conflict
    profile = DicomFileProfile()
    profile.load_config(
        {
            "recurse-sequence": True,
            "fields": [{"name": "PatientID", "replace-with": "XXX"}],
        }
    )
    assert len(profile.validate()) == 0

    # not compatible with regex
    profile = DicomFileProfile()
    profile.load_config(
        {
            "recurse-sequence": True,
            "fields": [{"regex": ".*PatientID.*", "replace-with": "XXX"}],
        }
    )
    errors = profile.validate()
    assert len(errors) == 1
    assert "incompatible with regex field" in errors[0]

    # not compatible with sequence type fieldname
    profile = DicomFileProfile()
    profile.load_config(
        {
            "recurse-sequence": True,
            "fields": [
                {"name": "OtherPatientIDsSequence.0.PatientID", "replace-with": "XXX"}
            ],
        }
    )
    errors = profile.validate()
    assert len(errors) == 1
    assert "incompatible with field SQ" in errors[0]


def test_profile_supports_repeater_group_for_50XX_and_60XX_only():
    profile = DicomFileProfile()
    profile.load_config(
        {"fields": [{"name": "40XX0010", "replace-with": "just a joke"}]}
    )
    errors = profile.validate()
    assert len(errors) == 1
    assert "Not in DICOM keyword list" in errors[0]

    profile = DicomFileProfile()
    profile.load_config(
        {
            "fields": [
                {"name": "50XX0010", "replace-with": "I am a repeater"},
                {"name": "60XX0010", "replace-with": "I am a repeater"},
            ]
        }
    )
    errors = profile.validate()
    assert len(errors) == 0


def test_profile_deid_repeater_group(dicom_file):
    path = dicom_file(
        "pydicom",
        "CT_small.dcm",
    )
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)
    dcm = pydicom.dcmread(path)
    # Adding repeater tags
    dcm.add_new(0x50000010, "US", 1)
    dcm.add_new(0x50010010, "US", 1)
    dcm.add_new(0x60000010, "US", 1)
    dcm.add_new(0x60010010, "US", 1)
    dcm.add_new(0x50000022, "LO", "BOB")
    dcm.save_as(path)

    profile = DicomFileProfile()
    profile.load_config(
        {
            "fields": [
                {"name": "50XX0010", "replace-with": 2},
                {"name": "60XX0010", "replace-with": 3},
                {"name": "(50XX, 0022)", "replace-with": "TOTO"},
            ]
        }
    )
    profile.process_files(test_fs, test_fs, [basename])
    dst_path = os.path.join(
        dirname, "1.3.6.1.4.1.5962.1.1.1.1.1.20040119072730.12322.CT.dcm"
    )
    dcm = pydicom.dcmread(dst_path, force=True)
    assert dcm[0x50000010].value == 2
    assert dcm[0x50010010].value == 2
    assert dcm[0x60000010].value == 3
    assert dcm[0x60010010].value == 3
    assert dcm[0x50000022].value == "TOTO"


def test_profile_deid_repeater_group_with_recurse_sequence(dicom_file):
    path = dicom_file(
        "pydicom",
        "CT_small.dcm",
    )
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)
    dcm = pydicom.dcmread(path)
    # Adding repeater tags
    ds = dcm.OtherPatientIDsSequence[0]
    ds.add_new(0x50000010, "US", 1)
    ds.add_new(0x50010010, "US", 1)
    ds.add_new(0x60000010, "US", 1)
    ds.add_new(0x60010010, "US", 1)
    dcm.save_as(path)

    profile = DicomFileProfile()
    profile.load_config(
        {
            "recurse-sequence": True,
            "fields": [
                {"name": "50XX0010", "replace-with": 2},
                {"name": "60XX0010", "replace-with": 3},
            ],
        }
    )
    profile.process_files(test_fs, test_fs, [basename])
    dst_path = os.path.join(
        dirname, "1.3.6.1.4.1.5962.1.1.1.1.1.20040119072730.12322.CT.dcm"
    )
    dcm = pydicom.dcmread(dst_path, force=True)
    ds = dcm.OtherPatientIDsSequence[0]
    assert ds[0x50000010].value == 2
    assert ds[0x50010010].value == 2
    assert ds[0x60000010].value == 3
    assert ds[0x60010010].value == 3


def test_profile_remove_undefined_option(dicom_file):
    path = dicom_file(
        "pydicom",
        "CT_small.dcm",
    )
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    dcm = pydicom.dcmread(path)
    while len(dcm.OtherPatientIDsSequence) <= 10:
        dcm.OtherPatientIDsSequence.append(dcm["OtherPatientIDsSequence"][0])
    dcm.save_as(path)

    profile = DicomFileProfile()
    profile.load_config(
        {
            "remove-undefined": True,
            "fields": [
                {"name": "PatientID", "keep": True},
                {"name": "SOPInstanceUID", "keep": True},
                {"name": 0x00100040, "keep": True},
                {"name": "(0010, 0010)", "keep": True},
                {"name": "(0043, GEMS_PARM_01, 4e)", "keep": True},
                # OtherPatientIDsSequence has 2 element, both
                # with PatientID (00100020) and TypeOfPatientID
                {"name": "OtherPatientIDsSequence.0.00100020", "keep": True},
                {
                    "name": "OtherPatientIDsSequence.10.00100020",
                    "replace-with": "REPLACED",
                },
                {"regex": ".*TypeOfPatientID", "keep": True},
            ],
        }
    )

    profile.process_files(test_fs, test_fs, [basename])
    dst_path = os.path.join(
        dirname, "1.3.6.1.4.1.5962.1.1.1.1.1.20040119072730.12322.NA.dcm"
    )
    dcm = pydicom.dcmread(dst_path, force=True)
    assert len(dcm) == 6
    assert len(dcm.OtherPatientIDsSequence) == 11
    assert len(dcm.OtherPatientIDsSequence[0]) == 2
    assert len(dcm.OtherPatientIDsSequence[1]) == 1
    assert dcm.OtherPatientIDsSequence[10][0x00100020].value == "REPLACED"


def test_profile_remove_undefined_with_recurse_sequence(dicom_file):
    path = dicom_file(
        "pydicom",
        "CT_small.dcm",
    )
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    profile = DicomFileProfile()
    profile.load_config(
        {
            "remove-undefined": True,
            "recurse-sequence": True,
            "fields": [
                {
                    "name": "PatientID",
                    "keep": True,
                },  # Recursing in OtherPatientIDsSequence
                {"name": "SOPInstanceUID", "keep": True},
            ],
        }
    )

    profile.process_files(test_fs, test_fs, [basename])
    dst_path = os.path.join(
        dirname, "1.3.6.1.4.1.5962.1.1.1.1.1.20040119072730.12322.NA.dcm"
    )
    dcm = pydicom.dcmread(dst_path, force=True)
    assert len(dcm) == 3
    assert len(dcm.OtherPatientIDsSequence) == 2
    assert len(dcm.OtherPatientIDsSequence[0]) == 1
    assert len(dcm.OtherPatientIDsSequence[1]) == 1


def test_remove_undefined_load_and_save():
    profile = DicomFileProfile()
    profile.load_config({"remove-undefined": True})
    assert profile.remove_undefined is True

    profile = DicomFileProfile()
    profile.remove_undefined = True
    config = profile.to_config()
    assert config["remove-undefined"] is True


def test_profile_can_modify_0002_group_elements(dicom_file):
    path = dicom_file(
        "pydicom",
        "CT_small.dcm",
    )
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    # hex notation
    config = {
        "fields": [
            {"name": 0x00020012, "replace-with": "my own class"},
            {"name": 0x00020013, "remove": True},
            {"name": 0x00020003, "hashuid": True},  # use read_field
        ]
    }
    profile = DicomFileProfile()
    profile.load_config(config)

    profile.process_files(test_fs, test_fs, [basename])
    dst_path = os.path.join(
        dirname, "1.3.6.1.4.1.5962.1.1.1.1.1.20040119072730.12322.CT.dcm"
    )
    dcm = pydicom.dcmread(dst_path, force=True)
    assert dcm.file_meta[0x00020012].value == "my own class"
    assert 0x00020013 not in dcm.file_meta
    assert (
        dcm.file_meta[0x00020003].value
        == "1.3.6.1.124412.321825.157231.165211.215310.119012.12322"
    )

    # hex notation
    config = {
        "fields": [
            {"name": "ImplementationClassUID", "replace-with": "my own class"},
            {"name": "ImplementationVersionName", "remove": True},
            {"name": "MediaStorageSOPInstanceUID", "hashuid": True},  # use read_field
        ]
    }
    profile = DicomFileProfile()
    profile.load_config(config)
    assert len(profile.validate()) == 0

    profile.process_files(test_fs, test_fs, [basename])
    dst_path = os.path.join(
        dirname, "1.3.6.1.4.1.5962.1.1.1.1.1.20040119072730.12322.CT.dcm"
    )
    dcm = pydicom.dcmread(dst_path, force=True)
    assert dcm.file_meta["ImplementationClassUID"].value == "my own class"
    assert dcm.file_meta.get("ImplementationVersionName") is None
    assert (
        dcm.file_meta["MediaStorageSOPInstanceUID"].value
        == "1.3.6.1.124412.321825.157231.165211.215310.119012.12322"
    )


def test_profile_can_insert_in_0002_group(dicom_file):
    path = dicom_file(
        "pydicom",
        "meta_missing_tsyntax.dcm",
    )
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    # keyword notation
    config = {
        "fields": [
            {"name": "TransferSyntaxUID", "replace-with": "Explicit VR Little Endian"},
        ]
    }
    profile = DicomFileProfile()
    profile.load_config(config)
    assert len(profile.validate()) == 0

    profile.process_files(test_fs, test_fs, [basename])
    dst_path = os.path.join(dirname, basename)
    dcm = pydicom.dcmread(dst_path, force=True)
    assert dcm.file_meta["TransferSyntaxUID"].value == "Explicit VR Little Endian"

    # hex notation
    config = {
        "fields": [
            {"name": 0x00020010, "replace-with": "Explicit VR Little Endian"},
        ]
    }
    profile = DicomFileProfile()
    profile.load_config(config)
    assert len(profile.validate()) == 0

    profile.process_files(test_fs, test_fs, [basename])
    dst_path = os.path.join(dirname, basename)
    dcm = pydicom.dcmread(dst_path, force=True)
    assert dcm.file_meta[0x00020010].value == "Explicit VR Little Endian"


@pytest.mark.parametrize(
    "profile_name",
    [
        # "dicom-profile-extended",
        # "dicom-profile-example-1",
        "dicom-profile-removed-undefined",
    ],
)
@pytest.mark.parametrize(
    "path",
    [
        ("pydicom", "CT_small.dcm"),
        ("pydicom", "rtstruct.dcm"),
        (
            "16844_1_1_dicoms",
            "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
        ),
    ],
)
def test_additional_profiles(dicom_file, template_file, path, profile_name):
    profile = deidentify.load_deid_profile(template_file(profile_name))
    errors = profile.validate()
    assert len(errors) == 0

    path = dicom_file(path[0], path[1])
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)
    assert profile.process_packfile("dicom", test_fs, test_fs, [basename])


def test_file_profile_sanitize_filename():
    profile = FileProfile()
    invalid_path = "some-invalid:path.dcm"
    dest_path = profile.get_dest_path({}, None, invalid_path)
    assert dest_path == "some-invalid_path.dcm"

    profile.sanitize_filename = False
    dest_path = profile.get_dest_path({}, None, invalid_path)
    assert dest_path == invalid_path


def test_dicom_matches_byte_sig(dicom_file, efile):
    # test dicom file
    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    profile = DicomFileProfile()
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)
    assert profile.matches_byte_sig(test_fs, basename)

    # test non-dicom
    path = efile
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)
    assert not profile.matches_byte_sig(test_fs, basename)


@pytest.mark.parametrize(
    "fieldname, is_flat",
    [
        ("(60XX,0010)", False),
        ("AnatomicRegionSequence.*.CodeValue", False),
        ("(0020,0010)", True),
    ],
)
def test_DicomTagStr_is_flat(fieldname, is_flat):
    fn = DicomTagStr(fieldname)
    assert fn.is_flat is is_flat


def test_get_log_fields_replace_field_with_the_on_field_map():
    profile = DicomFileProfile()
    profile.log_fields = ["StudyInstanceUID"]
    profile.load_config({"fields": [{"name": "StudyInstanceUID", "hashuid": True}]})
    res = profile.get_log_fields()
    assert len(res) == 1
    assert isinstance(res[0], DicomTagStr)


def test_parse_pixel_actions(tmp_path, mocker):
    cwd = mocker.patch("flywheel_migration.deidentify.dicom_file_profile.Path.cwd")
    tmp = mocker.patch("flywheel_migration.deidentify.dicom_file_profile.tempfile")
    tmp.gettempdir.return_value = tmp_path
    cwd.return_value = tmp_path
    profile = DicomFileProfile()
    profile.load_config(
        {
            "alter_pixels": {
                "detect": ["dicom", "dicom.xray.chest"],
                "pixels": [
                    {"remove": [0, 0, 10, 10], "when": "contains modality MR"},
                    {
                        "keep": [10, 10, 1000, 1000],
                        "when": "contains Manufacturer Siemens",
                    },
                ],
            }
        }
    )
    state = profile.create_file_state()
    assert state["deid"] == str(tmp_path / "mtk-dicom")
    assert state["deid_temp"] == tmp_path / "pixel_scrubbing"


def test_pixel_scrubbing_full(dicom_file, mocker, tmp_path):
    cwd = mocker.patch("flywheel_migration.deidentify.dicom_file_profile.Path.cwd")
    cwd.return_value = tmp_path
    tmp = mocker.patch("flywheel_migration.deidentify.dicom_file_profile.tempfile")
    tmp.gettempdir.return_value = tmp_path
    client = mocker.patch(
        "flywheel_migration.deidentify.dicom_file_profile.DicomCleaner"
    )
    pydicom = mocker.patch("flywheel_migration.deidentify.dicom_file_profile.pydicom")
    path = dicom_file(
        "pydicom",
        "CT_small.dcm",
    )

    def cp(v):
        shutil.copy(path, v)

    pydicom.read_file.return_value.save_as.side_effect = cp
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    profile = DicomFileProfile()
    profile.load_config(
        {
            "alter_pixels": {
                "detect": ["dicom", "dicom.xray.chest"],
                "pixels": [
                    {"remove": [0, 0, 10, 10], "when": "contains modality MR"},
                ],
            }
        }
    )
    profile.process_files(test_fs, test_fs, [basename])
    client.assert_called_once_with(
        output_folder=(cwd() / "pixel_scrubbing"),
        deid=["dicom", "dicom.xray.chest", str(cwd() / "mtk-dicom")],
    )
    pydicom.read_file.assert_called_once_with(
        client.return_value.dicom_file, force=True
    )
    assert not (cwd() / "pixel_scrubbing").exists()
    assert not (cwd() / "mtk-dicom").exists()


@pytest.mark.parametrize("asymmetric", [True, False])
def test_dicom_file_profile_retain_decrypt(dicom_file, asymmetric):
    secret_key = secrets.token_hex(16)
    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    # Seed initial values
    dcm = pydicom.dcmread(path)
    assert dcm.AccessionNumber == "Accession"
    dcm.PatientID = "1234"
    dcm.PatientName = "Can Akgun"
    dcm.PatientBirthDate = "20000101"
    dcm.SeriesInstanceUID = "1.2.840.113619.6.283.4.983142589.7316.1300473420.841"
    dcm.SOPInstanceUID = "1.3.12.2.1107.5.2.32.35068.2014102309133567866314331"
    dcm.StudyDate = "20180124"
    dcm.AcquisitionDateTime = "20180124114849.123456"
    dcm.PatientWeight = 52.3
    dcm.save_as(path)

    profile = DicomFileProfile()
    config = {
        "patient-age-from-birthdate": True,
        "patient-age-units": "Y",
        "date-increment": -17,
        "retain": True,
        "asymmetric-encryption": asymmetric,
        "secret-key": secret_key,
        "public-key": ["tests/data/encryption/cert.pem"],
        "fields": [
            {"name": "AccessionNumber", "hash": True},
            {"name": "PatientName", "remove": True},
            {"name": "PatientBirthDate", "remove": True},
            {"name": "PatientID", "replace-with": "0000"},
            {"name": "SeriesInstanceUID", "hashuid": True},
            {"name": "SOPInstanceUID", "hashuid": True},
            {"name": "StudyDate", "increment-date": True},
            {
                "name": "AcquisitionDateTime",
                "increment-datetime": True,
                "datetime-max": "+1days",
            },
            {"name": "PatientWeight", "jitter": True, "jitter-min": 40},
        ],
    }
    profile.load_config(config)

    # apply field operations
    profile.process_files(test_fs, test_fs, [basename])

    # Reload and verify
    # Hashed value should be: 1.3.12.2.651092.137711.166132.421848.119968.345027.314331
    dst_path = os.path.join(
        dirname, "1.3.12.2.651092.137711.166132.421848.119968.345027.314331.MR.dcm"
    )
    dcm = pydicom.dcmread(dst_path)
    assert dcm.AccessionNumber == "057fdb1eb48b2215"
    assert dcm.PatientID == "0000"
    assert dcm.StudyDate == "20180107"
    assert dcm.AcquisitionDateTime == "20180107114849.123456"
    assert not hasattr(dcm, "PatientName")
    assert not hasattr(dcm, "PatientBirthDate")
    # Check hashed value
    assert (
        dcm.SeriesInstanceUID
        == "1.2.840.113619.551726.420312.177022.222461.230571.501817.841"
    )
    assert (
        dcm.SOPInstanceUID
        == "1.3.12.2.651092.137711.166132.421848.119968.345027.314331"
    )
    assert dcm.PatientAge == "018Y"
    assert dcm.PatientWeight != 52.3
    assert dcm.EncryptedAttributesSequence[0]

    # Part two, use the EncryptedAttributesSequence to "revert" changes
    decrypt_profile = DicomFileProfile()
    decrypt_config = {
        "asymmetric-encryption": asymmetric,
        "secret-key": secret_key,
        "public-key": "tests/data/encryption/cert.pem",
        "private-key": "tests/data/encryption/key.pem",
        "fields": [
            {"name": "AccessionNumber", "decrypt": True},
            {"name": "PatientName", "decrypt": True},
            {"name": "PatientBirthDate", "decrypt": True},
            {"name": "PatientID", "decrypt": True},
            {"name": "SeriesInstanceUID", "decrypt": True},
            {"name": "SOPInstanceUID", "decrypt": True},
            {"name": "StudyDate", "decrypt": True},
            {"name": "AcquisitionDateTime", "decrypt": True},
            {"name": "PatientWeight", "decrypt": True},
        ],
    }
    decrypt_profile.load_config(decrypt_config)

    # apply field operations
    decrypt_profile.process_files(
        test_fs,
        test_fs,
        ["1.3.12.2.651092.137711.166132.421848.119968.345027.314331.MR.dcm"],
    )

    # All decrypted values should be set as the original (non-deid) values
    updated_dst_path = os.path.join(
        dirname, "1.3.12.2.1107.5.2.32.35068.2014102309133567866314331.MR.dcm"
    )
    dcm = pydicom.dcmread(updated_dst_path)
    assert dcm.AccessionNumber == "Accession"
    assert dcm.PatientID == "1234"
    assert dcm.PatientName == "Can Akgun"
    assert dcm.PatientBirthDate == "20000101"
    assert (
        dcm.SeriesInstanceUID == "1.2.840.113619.6.283.4.983142589.7316.1300473420.841"
    )
    assert dcm.SOPInstanceUID == "1.3.12.2.1107.5.2.32.35068.2014102309133567866314331"
    assert dcm.StudyDate == "20180124"
    assert dcm.AcquisitionDateTime == "20180124114849.123456"
    assert dcm.PatientWeight == 52.3


@pytest.mark.parametrize("asymmetric", [True, False])
def test_dicom_file_profile_selective_encrypt_decrypt(dicom_file, asymmetric):
    secret_key = secrets.token_hex(16)
    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    # Seed initial values
    dcm = pydicom.dcmread(path)
    dcm.PatientID = "1234"
    dcm.PatientBirthDate = "20000101"
    dcm.SeriesInstanceUID = "1.2.840.113619.6.283.4.983142589.7316.1300473420.841"
    dcm.SOPInstanceUID = "1.3.12.2.1107.5.2.32.35068.2014102309133567866314331"
    dcm.save_as(path)

    profile = DicomFileProfile()
    config = {
        "asymmetric-encryption": asymmetric,
        "secret-key": secret_key,
        "public-key": ["tests/data/encryption/cert.pem"],
        "fields": [
            {"name": "PatientBirthDate", "encrypt": True},
            {"name": "PatientID", "encrypt": True},
            {"name": "SeriesInstanceUID", "keep": True},
            {"name": "SOPInstanceUID", "hashuid": True},
        ],
    }
    profile.load_config(config)

    # apply field operations
    profile.process_files(test_fs, test_fs, [basename])

    # Reload and verify
    dst_path = os.path.join(
        dirname, "1.3.12.2.651092.137711.166132.421848.119968.345027.314331.MR.dcm"
    )
    dcm = pydicom.dcmread(dst_path)
    assert not hasattr(dcm, "PatientID")
    assert not hasattr(dcm, "PatientBirthDate")
    assert (
        dcm.SeriesInstanceUID == "1.2.840.113619.6.283.4.983142589.7316.1300473420.841"
    )
    assert (
        dcm.SOPInstanceUID
        == "1.3.12.2.651092.137711.166132.421848.119968.345027.314331"
    )
    assert dcm.EncryptedAttributesSequence[0]

    # Part two, decrypt only part of the encryption
    decrypt_profile = DicomFileProfile()
    decrypt_config = {
        "asymmetric-encryption": asymmetric,
        "secret-key": secret_key,
        "public-key": "tests/data/encryption/cert.pem",
        "private-key": "tests/data/encryption/key.pem",
        "fields": [
            {"name": "PatientID", "decrypt": True},
        ],
    }
    decrypt_profile.load_config(decrypt_config)

    # apply field operations
    decrypt_profile.process_files(
        test_fs,
        test_fs,
        ["1.3.12.2.651092.137711.166132.421848.119968.345027.314331.MR.dcm"],
    )

    # All decrypted values should be set as the original (non-deid) values
    updated_dst_path = os.path.join(
        dirname, "1.3.12.2.651092.137711.166132.421848.119968.345027.314331.MR.dcm"
    )
    dcm = pydicom.dcmread(updated_dst_path)
    assert dcm.PatientID == "1234"
    # PatientBirthDate was not configed to decrypt, so it should still be blank
    assert not hasattr(dcm, "PatientBirthDate")
    assert (
        dcm.SeriesInstanceUID == "1.2.840.113619.6.283.4.983142589.7316.1300473420.841"
    )
    assert (
        dcm.SOPInstanceUID
        == "1.3.12.2.651092.137711.166132.421848.119968.345027.314331"
    )
