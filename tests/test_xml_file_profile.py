import csv
import os
import shutil

import six
from fs.osfs import OSFS
from lxml import etree

from flywheel_migration import deidentify
from flywheel_migration.deidentify.deid_field import DeIdField
from flywheel_migration.deidentify.deid_profile import DeIdProfile
from flywheel_migration.deidentify.file_profile import FileProfile
from flywheel_migration.deidentify.xml_file_profile import (
    XMLFileProfile,
    parse_fieldname,
)


def test_xml_file_profile(xml_file):
    # Verify that subclass is registered
    file_profile_names = FileProfile.profile_names()
    assert len(file_profile_names) > 0
    assert "xml" in file_profile_names

    path = xml_file("sample", "sample1.xml")
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    profile = XMLFileProfile()
    profile.date_increment = 1
    profile.add_field(
        DeIdField.factory(
            {
                "name": "/Patient/Patient_Date_Of_Birth",
                "increment-date": True,
                "date-format": "%Y-%m-%d",
            }
        )
    )
    profile.add_field(
        DeIdField.factory(
            {"name": "/Patient/Patient_Gender", "replace-with": "REDACTED"}
        )
    )
    profile.add_field(
        DeIdField.factory({"name": "/Patient/SUBJECT_ID", "remove": True})
    )
    profile.add_field(
        DeIdField.factory(
            {"name": "/Patient/Visit/Scan/ScanTime", "increment-datetime": True}
        )
    )

    assert profile.matches_byte_sig(test_fs, basename)

    profile.process_files(test_fs, test_fs, [basename])
    with test_fs.open(basename, "r") as fp:
        tree = etree.parse(fp)
        assert tree.xpath("/Patient/Patient_Date_Of_Birth")[0].text == "1952-06-11"
        assert not tree.xpath("/Patient/SUBJECT_ID")
        assert (
            tree.xpath("/Patient/Visit[1]/Scan[1]/ScanTime")[0].text
            == "20200219160400.000000"
        )
        assert (
            tree.xpath("/Patient/Visit[1]/Scan[2]/ScanTime")[0].text
            == "20200219170400.000000"
        )


def test_xml_profile_can_log(tmpdir, xml_file, template_file):
    path = xml_file("sample", "sample1.xml")
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    cfg = {}
    deid_log_path = os.path.join(tmpdir, "deid_log.csv")
    cfg["deid-log"] = deid_log_path
    cfg["xml"] = {
        "name": "test",
        "date-increment": 1,
        "fields": [
            {
                "name": "/Patient/Patient_Date_Of_Birth",
                "increment-date": True,
                "date-format": "%Y-%m-%d",
            },
            {
                "name": "/Patient/Visit/Scan/OtherTime",
                "increment-datetime": True,
            },
            {"name": "/Patient/Visit/Scan/ScanTime", "increment-datetime": True},
        ],
    }

    profile = DeIdProfile()
    profile.load_config(cfg)

    assert profile.log is not None

    profile.initialize()
    assert profile.process_packfile("xml", test_fs, test_fs, [basename])
    profile.finalize()

    # Verify the contents of the log
    with open(deid_log_path, "r") as f:
        reader = csv.DictReader(f)
        rows = list(reader)

        assert len(rows) == 2
        before = rows[0]
        after = rows[1]

        assert before["type"] == "before"
        assert (
            before["/Patient/Patient_Date_Of_Birth"]
            == '{"/Patient/Patient_Date_Of_Birth": "1952-06-10"}'
        )
        assert (
            before["/Patient/Visit/Scan/ScanTime"]
            == '{"/Patient/Visit/Scan[1]/ScanTime": "20200218160400.000000", "/Patient/Visit/Scan[2]/ScanTime": "20200218170400.000000"}'
        )

        assert after["type"] == "after"
        assert (
            after["/Patient/Patient_Date_Of_Birth"]
            == '{"/Patient/Patient_Date_Of_Birth": "1952-06-11"}'
        )
        assert (
            after["/Patient/Visit/Scan/ScanTime"]
            == '{"/Patient/Visit/Scan[1]/ScanTime": "20200219160400.000000", "/Patient/Visit/Scan[2]/ScanTime": "20200219170400.000000"}'
        )
        assert (
            after["/Patient/Visit/Scan/OtherTime"]
            == '{"/Patient/Visit/Scan[1]/OtherTime": "19541216000000.000000", "/Patient/Visit/Scan[2]/OtherTime": "19541217000000.000000"}'
        )


def test_xml_profile_can_load_template(xml_file, template_file):
    path = xml_file("sample", "sample1.xml")
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    profile = deidentify.load_deid_profile(template_file("xml-profile"))
    profile.process_packfile("xml", test_fs, test_fs, [basename])

    with test_fs.open(basename, "rb") as fp:
        tree = etree.parse(fp)
        assert tree.xpath("/Patient/Patient_Date_Of_Birth")[0].text == "1900-01-01"
        assert not tree.xpath("/Patient/Patient_Name")
        assert tree.xpath("/Patient/SUBJECT_ID")[0].text == "8e591a7be5cda468"
        assert (
            tree.xpath("/Patient/Visit[1]/Scan[1]/ScanTime")[0].text
            == "20200201160400.000000"
        )
        assert (
            tree.xpath("/Patient/Visit[1]/Scan[2]/ScanTime")[0].text
            == "20200201170400.000000"
        )


def test_parse_fieldname_check_for_xpath_correctness():
    res = parse_fieldname(r"$invalid\xpath")
    assert res._is_xpath is not True
    res = parse_fieldname(r"/valid/xpath")
    assert res._is_xpath is True


def test_xml_profile_pass_and_log_if_not_xml_file(jpg_file, caplog):
    path = jpg_file("camera", "Canon_DIGITAL_IXUS_400.jpg")
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    profile = XMLFileProfile()
    assert profile.load_record({}, test_fs, basename) == (None, False)


def test_xml_profile_can_run_with_filename_only(xml_file):
    path = xml_file("sample", "sample1.xml")
    dirname, _ = os.path.split(path)
    basename = "1.2.276.0.75.2.2.40.33166211806.201002051135279530.xml"
    shutil.copy(path, os.path.join(dirname, basename))
    test_fs = OSFS(dirname)
    profile = XMLFileProfile()

    config = {
        "filenames": [
            {
                "output": "{group1}.xml",
                "input-regex": r"^(?P<group1>[\d\.]+).xml$",
                "groups": [{"name": "group1", "hashuid": True}],
            }
        ]
    }
    profile.load_config(config)
    profile.process_files(test_fs, test_fs, [basename])

    # hashuid is 1.2.276.0.131163.531882.440130.237143.225259.182160.279530
    expected_filename = "1.2.276.0.131163.531882.440130.237143.225259.182160.279530.xml"
    assert os.path.exists(os.path.join(dirname, expected_filename))


def test_tiff_filter_file():
    profile = XMLFileProfile()
    assert profile.matches_file("test.xml")
    assert profile.matches_file("test.XML")
    assert not profile.matches_file("test.png")


def test_xml_filename_supports_mix_of_record_attributes_and_regexgroup(xml_file):
    path = xml_file("sample", "bla-001-2007-11-15-bla-bla-bla.xml")
    dirname, _ = os.path.split(path)
    basename = "bla-001-2007-11-15-bla-bla-bla.xml"
    shutil.copy(path, os.path.join(dirname, basename))
    test_fs = OSFS(dirname)
    profile = XMLFileProfile()

    config = {
        "date-increment": -17,
        "filenames": [
            {
                "output": "{/Patient/EMR_ID}_{regdatetime}.xml",
                "input-regex": r"^(\w+)-(\d{3})-(?P<regdatetime>\d{4}-\d{2}-\d{2})-.*.xml$",
                "groups": [{"name": "regdatetime", "increment-datetime": True}],
            }
        ],
    }
    profile.load_config(config)
    profile.process_files(test_fs, test_fs, [basename])
    expected_filename = "123456-1234 _20071029000000.000000.xml"

    assert os.path.exists(os.path.join(dirname, expected_filename))
