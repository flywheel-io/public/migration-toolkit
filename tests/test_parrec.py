import pytz

from flywheel_migration import parse_par_header, parse_par_timestamp


def test_parse_par_header(par_file):
    path = par_file("DTI.PAR")
    with open(path) as f:
        results = parse_par_header(f)

    assert results
    assert results["patient_name"] == "05aug14test"
    assert results["protocol_name"] == "WIP DTI SENSE"
    assert results["exam_name"] == "test"
    assert results["exam_date"] == "2014.08.05 / 11:27:34"
    assert results["series_type"] == "Image   MRSERIES"
    assert results["test_unk_key"] == "Hello World"


def test_parse_par_timestamp():
    result = parse_par_timestamp("2014.08.05 / 11:27:34", timezone=pytz.UTC)

    assert result.isoformat() == "2014-08-05T11:27:34+00:00"
