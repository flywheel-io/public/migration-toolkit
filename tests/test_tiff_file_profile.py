import os
import shutil
import tempfile

import pytest
import six
from fs.osfs import OSFS
from PIL import Image, UnidentifiedImageError
from PIL.TiffTags import TAGS_V2

from flywheel_migration import deidentify
from flywheel_migration.deidentify.deid_field import DeIdField
from flywheel_migration.deidentify.file_profile import FileProfile
from flywheel_migration.deidentify.tiff_file_profile import TIFFFileProfile, TIFFRecord

TAGS_V2_INVERTED = {v.name: k for k, v in TAGS_V2.items()}


def test_tiff_file_profile(tiff_file):
    # Verify that subclass is registered
    file_profile_names = FileProfile.profile_names()
    assert len(file_profile_names) > 0
    assert "tiff" in file_profile_names

    path = tiff_file("sample", "sample.tiff")
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    profile = TIFFFileProfile()
    profile.date_increment = -17
    profile.add_field(DeIdField.factory({"name": "Software", "remove": True}))
    profile.add_field(
        DeIdField.factory({"name": "DateTime", "increment-datetime": True})
    )
    profile.add_field(DeIdField.factory({"name": "Model", "replace-with": "REDACTED"}))

    assert profile.matches_packfile("tiff")
    with test_fs.open(basename, "rb") as fp:
        im = Image.open(fp)
        assert im.tag_v2[TAGS_V2_INVERTED["DateTime"]] == "2020:02:14 14:14:14"

    profile.process_files(test_fs, test_fs, [basename])

    with test_fs.open(basename, "rb") as fp:
        im = Image.open(fp)
        assert TAGS_V2_INVERTED["Software"] not in im.tag_v2
        assert im.tag_v2[TAGS_V2_INVERTED["DateTime"]] == "2020:01:28 14:14:14"
        assert im.tag_v2[TAGS_V2_INVERTED["Model"]] == "REDACTED"


def test_tiff_file_profile_can_remove_private_tags(tiff_file):
    path = tiff_file("sample", "sample.tiff")
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    profile = TIFFFileProfile()
    profile.remove_private_tags = True

    with test_fs.open(basename, "rb") as fp:
        im = Image.open(fp)
        assert any([k >= profile.private_tags_lower_bound for k in im.tag_v2.keys()])

    profile.process_files(test_fs, test_fs, [basename])

    with test_fs.open(basename, "rb") as fp:
        im = Image.open(fp)
        assert all([k < profile.private_tags_lower_bound for k in im.tag_v2.keys()])


def test_tiff_record_raised_if_not_tiff_file(dicom_file, tiff_file):
    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    with pytest.raises(UnidentifiedImageError):
        with open(path, "rb") as fp:
            TIFFRecord(fp)

    path = tiff_file("not_a_tiff", "sample1.png")
    with pytest.raises(TypeError):
        with open(path, "rb") as fp:
            TIFFRecord(fp)


def test_tiff_profile_validate_returns_errors_if_kw_undefined(jpg_file):
    profile = TIFFFileProfile()
    profile.add_field(DeIdField.factory({"name": "Undefined", "replace-with": "None"}))
    profile.add_field(DeIdField.factory({"name": "DateTime", "replace-with": "None"}))
    errors = profile.validate()
    assert len(errors) == 1
    assert "Undefined" in errors[0]


def test_tiff_profile_can_load_template(tiff_file, template_file):
    path = tiff_file("sample", "sample.tiff")
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    profile = deidentify.load_deid_profile(template_file("tiff-profile"))
    profile.process_packfile("tiff", test_fs, test_fs, [basename])

    with test_fs.open(basename, "rb") as fp:
        im = Image.open(fp, mode="r")
        assert TAGS_V2_INVERTED["Software"] not in im.tag_v2
        assert im.tag_v2[TAGS_V2_INVERTED["DateTime"]] == "2020:01:28 14:14:14"
        assert im.tag_v2[TAGS_V2_INVERTED["Model"]] == "REDACTED"


def test_jpg_profile_can_run_without_touching_metadata(tiff_file):
    path = tiff_file("sample", "sample.tiff")
    dirname, _ = os.path.split(path)
    basename = "1.2.276.0.75.2.2.40.33166211806.201002051135279530.tiff"
    shutil.copy(path, os.path.join(dirname, basename))
    test_fs = OSFS(dirname)
    profile = TIFFFileProfile()

    config = {
        "filenames": [
            {
                "output": "{group1}.tiff",
                "input-regex": r"^(?P<group1>[\d\.]+).tiff$",
                "groups": [{"name": "group1", "hashuid": True}],
            }
        ]
    }
    profile.load_config(config)
    profile.process_files(test_fs, test_fs, [basename])

    # hashuid is 1.2.276.0.131163.531882.440130.237143.225259.182160.279530
    expected_filename = (
        "1.2.276.0.131163.531882.440130.237143.225259.182160.279530.tiff"
    )
    assert os.path.exists(os.path.join(dirname, expected_filename))


def test_tiff_filter_file():
    profile = TIFFFileProfile()
    assert profile.matches_file("test.tiff")
    assert profile.matches_file("test.tif")
    assert profile.matches_file("test.TIFF")
    assert profile.matches_file("test.TIF")
    assert not profile.matches_file("test.png")


def test_profile_filename_supports_mix_of_record_attributes_and_regexgroup(tiff_file):
    path = tiff_file("sample", "70201-2007-11-15.tiff")
    # Subset of Metadata stored in IFD: Human readable mapping in `PIL.TiffTags.TAGS_v2`
    # {256: 1600    # ImageWidth
    #  257: 2100    # ImageLength
    #  258: (1,)    # BitsPerSample
    # ...
    dirname, _ = os.path.split(path)
    basename = "70201-2007-11-15.tiff"
    shutil.copy(path, os.path.join(dirname, basename))
    test_fs = OSFS(dirname)
    profile = TIFFFileProfile()

    config = {
        "date-increment": -17,
        "filenames": [
            {
                "output": "{ImageLength}_{regdatetime}.tiff",
                "input-regex": r"^(\d{5})-(?P<regdatetime>\d{4}-\d{2}-\d{2}).tiff$",
                "groups": [{"name": "regdatetime", "increment-datetime": True}],
            }
        ],
    }
    profile.load_config(config)
    profile.process_files(test_fs, test_fs, [basename])
    expected_filename = "2100_2007_10_29 00_00_00.tiff"

    assert os.path.exists(os.path.join(dirname, expected_filename))


def test_tiffrecord_save_workaround_for_tiff_with_tag_issue(tiff_file):
    path = tiff_file("sample", "test_bitonal_text_with_tag_issue.tif")
    with open(path, "rb") as fp:
        record = TIFFRecord(fp)

    with tempfile.NamedTemporaryFile(suffix=".tiff") as tmpfile:
        record.save_as(tmpfile.name, compression="group4")

        assert os.path.exists(tmpfile.name)
        assert os.path.getsize(tmpfile.name)

        # can be reopened
        with open(tmpfile.name, "rb") as fp:
            TIFFRecord(fp)
