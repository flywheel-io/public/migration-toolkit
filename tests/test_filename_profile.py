import os
import shutil
import tempfile

import pytest
import six
from fs.osfs import OSFS

from flywheel_migration import deidentify
from flywheel_migration.deidentify.file_profile import FileProfile
from flywheel_migration.deidentify.filename_file_profile import (
    FilenameFileProfile,
    FilenameRecord,
)


def test_filename_record_can_save(filenames_file):
    path = filenames_file(
        "20100205113527_1.2.276.0.75.2.2.40.33166211806.201002051135279530.bin"
    )
    with tempfile.TemporaryDirectory() as tmp_dir:
        OSFS(tmp_dir)
        record = FilenameRecord(path)
        record.save_as(os.path.join(tmp_dir, "test"))
        assert os.path.exists(os.path.join(tmp_dir, "test"))


def test_filename_record_raises_if_issue_with_file(filenames_file):
    with pytest.raises(FileNotFoundError) as excinfo:
        _ = FilenameRecord("tests")
        assert "not a file" in str(excinfo.value)
    with pytest.raises(FileNotFoundError) as excinfo:
        _ = FilenameRecord("doesnotexist")
        assert "not found" in str(excinfo.value)


def test_filename_file_profile(filenames_file):
    # Verify that subclass is registered
    file_profile_names = FileProfile.profile_names()
    assert "filename" in file_profile_names

    profile = FilenameFileProfile()
    # test can load profile
    config = {
        "date-increment": 5,
        "filenames": [
            {
                "output": "{datetime}_{UID}.bin",
                "input-regex": r"^(?P<datetime>[\d+]+)_(?P<UID>[\d\.]+).*\.[\w\d]{3}$",
                "groups": [
                    {"name": "datetime", "increment-datetime": True},
                    {"name": "UID", "hashuid": True},
                ],
            }
        ],
    }
    profile.load_config(config)
    assert isinstance(profile.filenames, list)
    assert profile.filenames[0]["output"] == "{datetime}_{UID}.bin"
    assert (
        profile.filenames[0]["input-regex"]
        == r"^(?P<datetime>[\d+]+)_(?P<UID>[\d\.]+).*\.[\w\d]{3}$"
    )
    assert len(profile.filenames[0]["groups"]) == 2

    # test can dump profile
    config = profile.to_config()
    assert "filenames" in config
    assert config["filenames"][0]["output"] == "{datetime}_{UID}.bin"
    assert (
        config["filenames"][0]["input-regex"]
        == r"^(?P<datetime>[\d+]+)_(?P<UID>[\d\.]+).*\.[\w\d]{3}$"
    )

    path = filenames_file(
        "20100205113527_1.2.276.0.75.2.2.40.33166211806.201002051135279530.bin"
    )
    dirname, _ = os.path.split(path)
    basename = "20100205113527_1.2.276.0.75.2.2.40.33166211806.201002051135279530.bin"
    shutil.copy(path, os.path.join(dirname, basename))
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)
    profile.process_files(test_fs, test_fs, [basename])

    # hashuid is 1.2.276.0.131163.531882.440130.237143.225259.182160.279530
    expected_filename = "20100210113527.000000_1.2.276.0.131163.531882.440130.237143.225259.182160.279530.bin"
    assert os.path.exists(os.path.join(dirname, expected_filename))


def test_filename_file_profile_validate_raises_if_output_element_not_found():
    profile = FilenameFileProfile()
    # test can load profile
    config = {
        "date-increment": 5,
        "filenames": [
            {
                "output": "{doesnotexist}.bin",
                "input-regex": r"^(?P<datetime>[\d+]+)_(?P<UID>[\d\.]+).*\.[\w\d]{3}$",
            }
        ],
    }
    profile.load_config(config)
    errors = profile.validate()
    assert "doesnotexist" in errors[0]


def test_filename_file_profile_load_and_parse_from_template_yaml(template_file):
    profile = deidentify.load_deid_profile(template_file("filename-profile"))
    filename = "1.2.276.0.75.2.2.40.33166211806-2010-02-05-11:35:27.yop"
    with tempfile.TemporaryDirectory() as tmpdir:
        with open(os.path.join(tmpdir, filename), "w") as fp:
            fp.writelines([])
        basename = six.u(filename)  # fs requires unicode
        test_fs = OSFS(tmpdir)
        profile.process_packfile("filename", test_fs, test_fs, [basename])
        expected_filename = "1.2.276.0.116189.209792.417924.226166.741301.531031.211806_2010-02-06-11_35_27.yop"
        assert os.path.exists(os.path.join(tmpdir, expected_filename))


def test_filename_profile_matches_all_file():
    profile = FilenameFileProfile()
    assert not profile.matches_file("*")
    profile = FilenameFileProfile(file_filter="*")
    assert profile.matches_file("*")


def test_filename_with_embedded_datetime_format(filenames_file):
    profile = FilenameFileProfile()
    # test can load profile
    config = {
        "date-increment": 5,
        "filenames": [
            {
                "output": "{datetime}.toto",
                "input-regex": r"^(?P<datetime>[\d+]+).toto",
                "groups": [
                    {
                        "name": "datetime",
                        "increment-datetime": True,
                        "datetime-format": "%Y-%m-%d %H:%M:%S",
                    },
                    {"name": "UID", "hashuid": True},
                ],
            }
        ],
    }
    profile.load_config(config)
    with tempfile.TemporaryDirectory() as tmpdir:
        with open(os.path.join(tmpdir, "20100205113527.toto"), "w") as fp:
            fp.writelines(["do-si-mi-do-si-mi-fa"])
        path = os.path.join(tmpdir, "20100205113527.toto")
        dirname, basename = os.path.split(path)
        test_fs = OSFS(dirname)
        profile.process_files(test_fs, test_fs, [basename])
        expected_filename = "2010-02-10 11_35_27.toto"
        assert os.path.exists(os.path.join(dirname, expected_filename))
