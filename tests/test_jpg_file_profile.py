import os
import shutil

import piexif
import pytest
import six
from fs.osfs import OSFS
from PIL import Image, UnidentifiedImageError

from flywheel_migration import deidentify
from flywheel_migration.deidentify.deid_field import DeIdField
from flywheel_migration.deidentify.file_profile import FileProfile
from flywheel_migration.deidentify.jpg_file_profile import JPGFileProfile, JPGRecord


def test_jpg_file_profile(jpg_file):
    # Verify that subclass is registered
    file_profile_names = FileProfile.profile_names()
    assert len(file_profile_names) > 0
    assert "jpg" in file_profile_names

    path = jpg_file("camera", "Canon_DIGITAL_IXUS_400.jpg")
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    profile = JPGFileProfile()
    profile.date_increment = -17
    profile.add_field(
        DeIdField.factory({"name": "DateTimeOriginal", "increment-datetime": True})
    )
    profile.add_field(
        DeIdField.factory({"name": "DateTimeDigitized", "replace-with": "REDACTED"})
    )
    profile.add_field(DeIdField.factory({"name": "DateTime", "remove": True}))

    assert profile.matches_packfile("jpg")

    profile.process_files(test_fs, test_fs, [basename])

    with test_fs.open(basename, "rb") as fp:
        image = Image.open(fp, mode="r")
        exif = piexif.load(image.info["exif"])
        assert exif["Exif"][piexif.ExifIFD.DateTimeOriginal] == b"2004:08:10 13:52:55"
        assert exif["Exif"][piexif.ExifIFD.DateTimeDigitized] == b"REDACTED"
        assert piexif.ImageIFD.DateTime not in exif["0th"]
        assert piexif.ImageIFD.DateTime not in exif["1st"]


def test_jpg_file_profile_can_remove_exif(jpg_file):
    path = jpg_file("camera", "Canon_DIGITAL_IXUS_400.jpg")
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    profile = JPGFileProfile()
    profile.remove_exif = True

    profile.process_files(test_fs, test_fs, [basename])

    with test_fs.open(basename, "rb") as fp:
        image = Image.open(fp, mode="r")
        assert "exif" not in image.info


def test_jpg_file_profile_can_remove_gps(jpg_file):
    path = jpg_file("camera", "DSCN0010_with_gps.jpg")
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    profile = JPGFileProfile()
    profile.remove_gps = True
    with test_fs.open(basename, "rb") as fp:
        image = Image.open(fp, mode="r")
        exif = piexif.load(image.info["exif"])
        assert len(exif["GPS"]) > 0

    profile.process_files(test_fs, test_fs, [basename])

    with test_fs.open(basename, "rb") as fp:
        image = Image.open(fp, mode="r")
        exif = piexif.load(image.info["exif"])
        assert len(exif["GPS"]) == 0


def test_jpg_record_raised_if_not_jpg_file(dicom_file, png_file):
    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    with pytest.raises(UnidentifiedImageError):
        with open(path, "rb") as fp:
            JPGRecord(fp)

    path = png_file("exif", "sample1.png")
    with pytest.raises(TypeError):
        with open(path, "rb") as fp:
            JPGRecord(fp)


def test_jpg_profile_validate_returns_errors_if_kw_undefined(jpg_file):
    profile = JPGFileProfile()
    profile.date_increment = -17
    profile.add_field(DeIdField.factory({"name": "Undefined", "replace-with": "None"}))
    profile.add_field(
        DeIdField.factory({"name": "DateTimeOriginal", "replace-with": "None"})
    )
    errors = profile.validate()
    assert len(errors) == 1
    assert "Undefined" in errors[0]


def test_jpg_profile_create_attribute_on_replace_with_if_not_in_file(jpg_file):
    path = jpg_file("camera", "Canon_DIGITAL_IXUS_400.jpg")
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    with test_fs.open(basename, "rb") as fp:
        image = Image.open(fp, mode="r")
        exif = piexif.load(image.info["exif"])
        assert piexif.ExifIFD.LensSerialNumber not in exif["Exif"]

    profile = JPGFileProfile()
    profile.add_field(
        DeIdField.factory({"name": "LensSerialNumber", "replace-with": "TEST"})
    )

    profile.process_files(test_fs, test_fs, [basename])

    with test_fs.open(basename, "rb") as fp:
        image = Image.open(fp, mode="r")
        exif = piexif.load(image.info["exif"])
        assert exif["Exif"][piexif.ExifIFD.LensSerialNumber] == b"TEST"


def test_jpg_profile_can_load_template(jpg_file, template_file):
    path = jpg_file("camera", "Canon_DIGITAL_IXUS_400.jpg")
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    profile = deidentify.load_deid_profile(template_file("jpg-profile"))
    profile.process_packfile("jpg", test_fs, test_fs, [basename])

    with test_fs.open(basename, "rb") as fp:
        image = Image.open(fp, mode="r")
        exif = piexif.load(image.info["exif"])
        assert exif["Exif"][piexif.ExifIFD.DateTimeOriginal] == b"2004:08:10 13:52:55"
        assert exif["Exif"][piexif.ExifIFD.DateTimeDigitized] == b"2004:08:10 13:52:55"
        assert exif["0th"][piexif.ImageIFD.DateTime] == b"2008:07:14 17:15:01"
        assert piexif.ImageIFD.Artist not in exif["0th"]
        assert piexif.ImageIFD.Artist not in exif["1st"]
        assert exif["Exif"][piexif.ExifIFD.CameraOwnerName] == b"REDACTED"


def test_jpg_profile_can_run_without_touching_metadata(jpg_file):
    path = jpg_file("camera", "Canon_DIGITAL_IXUS_400.jpg")
    dirname, _ = os.path.split(path)
    basename = "1.2.276.0.75.2.2.40.33166211806.201002051135279530.jpg"
    shutil.copy(path, os.path.join(dirname, basename))
    test_fs = OSFS(dirname)
    profile = JPGFileProfile()

    config = {
        "filenames": [
            {
                "output": "{group1}.jpg",
                "input-regex": r"^(?P<group1>[\d\.]+).jpg$",
                "groups": [{"name": "group1", "hashuid": True}],
            }
        ]
    }
    profile.load_config(config)
    profile.process_files(test_fs, test_fs, [basename])

    # hashuid is 1.2.276.0.131163.531882.440130.237143.225259.182160.279530
    expected_filename = "1.2.276.0.131163.531882.440130.237143.225259.182160.279530.jpg"
    assert os.path.exists(os.path.join(dirname, expected_filename))


def test_jpg_profile_matches_file():
    profile = JPGFileProfile()
    assert profile.matches_file("test.jpg")
    assert profile.matches_file("test.jpeg")
    assert profile.matches_file("test.JPEG")
    assert profile.matches_file("test.JPG")
    assert not profile.matches_file("test.ext")


def test_jpg_profile_matches_file_raised_type_error_if_not_list_or_string():
    profile = JPGFileProfile()
    profile.file_filter = ("*.jpg",)  # should be list or string
    with pytest.raises(TypeError):
        profile.matches_file("test.jpg")


def test_jpg_profile_filename_supports_mix_of_record_attributes_and_regexgroup(
    jpg_file,
):
    path = jpg_file("camera", "00001-20071115.jpg")
    # Subset of Metadata stored in IFD 0th for that test file is: Human readable mapping in `piexif.TAGS`
    #  {270: b'                               ',    # ImageDescription
    #   271: b'NIKON',                              # Make
    #   272: b'COOLPIX P6000',                      # Model
    #   274: 1,                                     # Orientation
    #   282: (300, 1),                              # XResolution
    #   283: (300, 1),                              # YResolution
    #   ...
    #   }

    dirname, _ = os.path.split(path)
    basename = "00001-20071115.jpg"
    shutil.copy(path, os.path.join(dirname, basename))
    test_fs = OSFS(dirname)
    profile = JPGFileProfile()

    config = {
        "date-increment": -17,
        "filenames": [
            {
                "output": "{Make}_{regdatetime}.jpg",
                "input-regex": r"^(\d{5})-(?P<regdatetime>\d{4}\d{2}\d{2}).jpg$",
                "groups": [{"name": "regdatetime", "increment-datetime": True}],
            }
        ],
    }
    profile.load_config(config)
    profile.process_files(test_fs, test_fs, [basename])
    expected_filename = "NIKON_2007_10_29 00_00_00.jpg"

    assert os.path.exists(os.path.join(dirname, expected_filename))


def test_jpg_matches_byte_sig(jpg_file, efile):
    # test jpg
    profile = JPGFileProfile()
    path = jpg_file("camera", "DSCN0010_with_gps.jpg")
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)
    assert profile.matches_byte_sig(test_fs, basename)

    # test non-jpg
    path = efile
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)
    assert not profile.matches_byte_sig(test_fs, basename)
