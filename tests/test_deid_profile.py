import csv
import json
import os
import shutil
import tempfile

import pydicom
import pytest
import six
from fs.osfs import OSFS
from pydicom.dataset import Dataset
from ruamel.yaml import YAML

from flywheel_migration import deidentify
from flywheel_migration.deidentify import ValidationError
from flywheel_migration.deidentify.deid_log import DeIdLog

yaml = YAML()


def test_load_deid_profile():
    profile = deidentify.load_deid_profile("minimal")
    assert profile

    config = profile.to_config()

    try:
        fd, yml_path = tempfile.mkstemp(".yml")
        with os.fdopen(fd, "w") as f:
            yaml.dump(config, f)

        custom_profile = deidentify.load_deid_profile(yml_path)
        assert custom_profile
    finally:
        os.remove(yml_path)

    # Not existing profile name
    with pytest.raises(Exception) as e:
        assert deidentify.load_deid_profile("test")
    assert str(e.value) == "Unknown de-identification profile: test"


def test_load_deid_profile_none():
    profile = deidentify.load_deid_profile("none")
    assert not profile


def test_load_deid_profile_should_validate_fields(template_file):
    """invalid-profile has 2 non-DICOM field names"""
    try:
        deidentify.load_deid_profile(template_file("invalid-profile"))
        pytest.fail("Expected ValidationError")
    except ValidationError as err:
        assert len(err.errors) == 2


def test_load_deid_profile_should_validate_datetime_format(template_file):
    """invalid-date-format-profile has invalid format code"""
    try:
        deidentify.load_deid_profile(template_file("invalid-date-format-profile"))
        pytest.fail("Expected ValidationError")
    except ValidationError as err:
        assert len(err.errors) == 4
        assert any("%O" in err for err in err.errors)
        assert any("%k" in err for err in err.errors)
        assert any("%i" in err for err in err.errors)
        assert any("%J" in err for err in err.errors)


def test_load_deid_profile_should_normalize_fields(template_file, dicom_file):
    """abnormal-profile has 3 DICOM field name case changes, remove action"""
    profile = deidentify.load_deid_profile(template_file("abnormal-profile"))
    assert profile
    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)
    dcm = pydicom.dcmread(path)
    assert dcm.PatientID
    assert dcm.PatientName
    assert dcm.PatientBirthDate
    assert profile.process_packfile("dicom", test_fs, test_fs, [basename])
    dst_path = os.path.join(
        dirname, "1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.MR.dcm"
    )
    dcm = pydicom.dcmread(dst_path)
    assert not hasattr(dcm, "PatientID")
    assert not hasattr(dcm, "PatientName")
    assert not hasattr(dcm, "PatientBirthDate")


def test_load_deid_profile_with_filename_attributes():
    config = {
        "name": "Test",
        "tmp": {
            "filenames": [
                {
                    "output": "{group2}_{group1}.jpg",
                    "input-regex": r"^(?P<group2>\w+)-(?P<group1>\w+).jpg$",
                }
            ]
        },
    }

    # Creating a tmp FileProfile class
    class TmpProfile(deidentify.file_profile.FileProfile):
        name = "tmp"
        pass

    with tempfile.NamedTemporaryFile(suffix=".yml") as filename:
        with open(filename.name, "w") as fid:
            yaml.dump(config, fid)

        profile = deidentify.load_deid_profile(filename.name)
        dicom_profile = [fp for fp in profile.file_profiles if fp.name == "dicom"][0]
        tmp_profile = [fp for fp in profile.file_profiles if fp.name == "tmp"][0]

        assert dicom_profile.filenames == []
        assert tmp_profile.filenames[0]["output"] == "{group2}_{group1}.jpg"
        assert (
            tmp_profile.filenames[0]["input-regex"]
            == r"^(?P<group2>\w+)-(?P<group1>\w+).jpg$"
        )


def test_validate_deid_profile_with_corrupted_regex_fails():
    profile = deidentify.deid_profile.FileProfile()
    profile.filenames = [
        {"output": "{DoesNotExist}_{SOPInstanceUID}.dcm", "input-regex": "??"}
    ]
    errors = profile.validate()
    assert "Regex in deid profile is invalid: ??" in errors


def test_load_and_save_profile():
    profiles = deidentify.load_default_profiles()
    profile = next(profile for profile in profiles if profile.name == "minimal")
    assert profile

    config = profile.to_config()
    assert config

    yml_path = None
    json_path = None

    try:
        # Write yaml
        fd, yml_path = tempfile.mkstemp(".yml")
        with os.fdopen(fd, "w") as f:
            yaml.dump(config, f)

        # Load from yaml
        yml_profile = deidentify.load_profile(yml_path)
        assert yml_profile.to_config() == config

        # Write json
        fd, json_path = tempfile.mkstemp(".json")
        with os.fdopen(fd, "w") as f:
            json.dump(config, f)

        # Load from json
        json_profile = deidentify.load_profile(json_path)
        assert json_profile.to_config() == config

    finally:
        if yml_path:
            os.remove(yml_path)
        if json_path:
            os.remove(json_path)


def test_dicom_file_profile(dicom_file):
    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    profiles = deidentify.load_default_profiles()
    assert len(profiles) >= 2

    # Test none profile
    profile = next(profile for profile in profiles if profile.name == "none")

    assert not profile
    assert profile.name == "none"
    assert profile.description

    assert profile.process_packfile("dicom", test_fs, test_fs, [basename])

    dst_path = os.path.join(
        dirname, "1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.MR.dcm"
    )
    dcm = pydicom.dcmread(dst_path)
    assert dcm.PatientID == "flywheel/reaper"
    assert dcm.PatientName == "Lastname^Firstname"
    assert dcm.PatientBirthDate == "20000101"

    # Test minimal profile
    profile = next(profile for profile in profiles if profile.name == "minimal")

    assert profile
    assert profile.name == "minimal"
    assert profile.description

    assert profile.process_packfile("dicom", test_fs, test_fs, [basename])

    dst_path = os.path.join(
        dirname, "1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.MR.dcm"
    )
    dcm = pydicom.dcmread(dst_path)
    assert not hasattr(dcm, "PatientID")
    assert not hasattr(dcm, "PatientName")
    assert not hasattr(dcm, "PatientBirthDate")


def test_dicom_subject_map_and_deid_log_profile(dicom_file):  # noqa: PLR0915
    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    profiles = deidentify.load_default_profiles()
    assert len(profiles) >= 2

    # Test none profile
    profile = next(profile for profile in profiles if profile.name == "minimal")
    cfg = profile.to_config()

    tmpdir = tempfile.mkdtemp()
    try:
        # Add deid-log to profile
        deid_log_path = os.path.join(tmpdir, "deid_log.csv")
        cfg["deid-log"] = deid_log_path

        # Create a new profile instance and reload config
        profile = deidentify.DeIdProfile()
        # adding a enhanced dicom field
        cfg["dicom"]["fields"].append(
            {"regex": "^(?!.*EchoTime|RepetitionTime).*Time$", "remove": True}
        )
        cfg["dicom"]["fields"].append({"name": "50xx0010"})
        profile.load_config(cfg)

        assert profile.log is not None

        # Process the dicom
        profile.initialize()
        assert profile.process_packfile("dicom", test_fs, test_fs, [basename])
        profile.finalize()

        # Verify the contents of the log
        with open(deid_log_path, "r") as f:
            reader = csv.DictReader(f)
            rows = list(reader)

            assert len(rows) == 2
            before = rows[0]
            after = rows[1]

            assert before["path"]
            assert before["type"] == "before"
            assert before["SeriesInstanceUID"]
            assert before["StudyInstanceUID"]
            assert before["SOPInstanceUID"]
            assert before["PatientID"] == "flywheel/reaper"
            assert before["PatientName"] == "Lastname^Firstname"
            assert before["PatientBirthDate"] == "20000101"
            assert before["^(?!.*EchoTime|RepetitionTime).*Time$"] == json.dumps(
                {
                    "AcquisitionTime": "173623",
                    "ContentTime": "173623",
                    "PerformedProcedureStepStartTime": "173354",
                    "SeriesTime": "173623",
                    "StudyTime": "173501",
                }
            )
            assert before["50xx0010"] == "{}"

            assert after["path"] == before["path"]
            assert after["type"] == "after"
            assert after["SeriesInstanceUID"] == before["SeriesInstanceUID"]
            assert after["StudyInstanceUID"] == before["StudyInstanceUID"]
            assert after["SOPInstanceUID"] == before["SOPInstanceUID"]
            assert after["PatientID"] == ""
            assert after["PatientName"] == ""
            assert after["PatientBirthDate"] == ""
            assert after["^(?!.*EchoTime|RepetitionTime).*Time$"] == "{}"
            assert after["50xx0010"] == "{}"

        # Verify the de-id
        dst_path = os.path.join(
            dirname, "1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.MR.dcm"
        )
        dcm = pydicom.dcmread(dst_path)
        assert not hasattr(dcm, "PatientID")
        assert not hasattr(dcm, "PatientName")
        assert not hasattr(dcm, "PatientBirthDate")

    finally:
        shutil.rmtree(tmpdir)


def test_multiple_deid_logger(dicom_file):
    class CustomDeIdLog(DeIdLog):
        def __init__(self):
            self.logs = {}

        def to_config_str(self):
            return None

        def initialize(self, profile):
            pass

        def write_entry(self, entry, **kwargs):
            payload = {"entry": entry}
            payload = {**payload, **kwargs}
            self.logs[entry.get("path") + "_" + entry.get("type")] = payload

        def close(self):
            pass

    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)

    profiles = deidentify.load_default_profiles()
    assert len(profiles) >= 2

    loggers = [CustomDeIdLog(), CustomDeIdLog()]

    profile = next(profile for profile in profiles if profile.name == "minimal")

    for file_profile in profile.file_profiles:
        file_profile.set_log(loggers)

    assert profile
    assert profile.name == "minimal"
    assert profile.description

    assert profile.process_packfile("dicom", test_fs, test_fs, [basename])

    for logger in loggers:
        assert len(logger.logs) == 2
        before = list(logger.logs.keys())[0]
        after = list(logger.logs.keys())[1]

        assert before.endswith("_before")
        assert after.endswith("_after")

        assert logger.logs[before]["entry_type"] == "before"
        assert logger.logs[after]["entry_type"] == "after"

        assert list(logger.logs[before].keys()) == [
            "entry",
            "path",
            "entry_type",
            "state",
            "record",
            "logged_fields",
        ]
        assert list(logger.logs[after].keys()) == [
            "entry",
            "path",
            "entry_type",
            "state",
            "record",
            "logged_fields",
        ]

        assert list(logger.logs[before]["logged_fields"].keys()) == [
            "PatientBirthDate",
            "PatientName",
            "PatientID",
        ]
        assert list(logger.logs[after]["logged_fields"].keys()) == [
            "PatientBirthDate",
            "PatientName",
            "PatientID",
        ]

        assert isinstance(logger.logs[before]["record"], Dataset)
        assert logger.logs[before]["record"] == logger.logs[after]["record"]

        assert (
            logger.logs[before]["state"]["series_uid"]
            == "1.2.840.113619.2.408.5282380.5220731.30424.1516669014.474"
        )
        assert (
            logger.logs[before]["state"]["session_uid"]
            == "1.2.840.113619.6.408.128090802883025653595086587293755801755"
        )
        assert logger.logs[before]["state"]["sop_uids"] == {
            "1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164"
        }
        assert logger.logs[before]["state"] == logger.logs[after]["state"]


def test_load_deid_profile_encryption():
    config = {
        "name": "Test",
        "secret-key": "global secret_key value",
        "dicom": {
            "public-key": ["public_key value 1", "public_key value 2"],
            "private-key": "private_key value",
        },
        "json": {
            "secret-key": "profile secret_key value",
        },
        "jpg": {},
    }

    with tempfile.NamedTemporaryFile(suffix=".yml") as filename:
        with open(filename.name, "w") as fid:
            yaml.dump(config, fid)

        profile = deidentify.load_deid_profile(filename.name)
        dicom_profile = [fp for fp in profile.file_profiles if fp.name == "dicom"][0]
        json_profile = [fp for fp in profile.file_profiles if fp.name == "json"][0]
        jpg_profile = [fp for fp in profile.file_profiles if fp.name == "jpg"][0]

        # dicom key transfer keys passed
        assert dicom_profile.public_key == ["public_key value 1", "public_key value 2"]
        assert dicom_profile.private_key == "private_key value"
        # because no dicom secret_key was provided, should default to global
        # (later logic will handle Key Transport vs Symmetric encryption)
        assert dicom_profile.secret_key == "global secret_key value"
        # json secret_key was set, should override global
        assert json_profile.secret_key == "profile secret_key value"
        # jpg secret_key was not set, should default to global
        assert jpg_profile.secret_key == "global secret_key value"
