import csv
import os
import tempfile

import pytest

from flywheel_migration import DicomFile, deidentify
from flywheel_migration.deidentify.csv_subject_map import CONFIG_KEY as CSV_CONFIG_KEY
from flywheel_migration.deidentify.factory import (
    DEFAULT_SUBJECT_MAP_FIELDS as DEFAULT_FIELDS,
)
from flywheel_migration.deidentify.factory import DEFAULT_SUBJECT_MAP_FORMAT
from flywheel_migration.deidentify.subject_map import SUBJECT_LABEL_KEY


def test_csv_subject_map(dicom_file):  # noqa: PLR0915
    # Create temp directory
    fd, path = tempfile.mkstemp()
    os.close(fd)

    subject_map = deidentify.create_subject_map(path)
    assert subject_map is not None
    assert subject_map.path == path
    assert subject_map.config.fields == DEFAULT_FIELDS
    assert subject_map.config.format_str == DEFAULT_SUBJECT_MAP_FORMAT

    assert os.path.exists(path)

    # Add dicom record
    # PatientName: Lastname^Firstname
    # PatientBirthDate: 20000101
    dcm_path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    df = DicomFile(dcm_path, parse=True)

    label = subject_map.get_label(df)
    assert label == "1"
    assert label == subject_map.get_code(df)  # Backward-compatibility #FLYW-3539

    # Save and reload
    subject_map.save()

    subject_map = deidentify.load_subject_map(path)
    assert subject_map is not None
    assert subject_map.path == path
    assert subject_map.config.fields == DEFAULT_FIELDS
    assert subject_map.config.format_str == DEFAULT_SUBJECT_MAP_FORMAT

    assert (
        ("PatientName", "lastname^firstname"),
        ("PatientBirthDate", "20000101"),
    ) in subject_map.idmap

    # Test that new values = new label
    assert "2" == subject_map.get_label(
        {"PatientName": "Last First", "PatientBirthDate": "20010202"}
    )

    # Backward-compatibility #FLYW-3539
    assert "2" == subject_map.get_code(
        {"PatientName": "Last First", "PatientBirthDate": "20010202"}
    )

    # Test that missing values are OK
    assert "3" == subject_map.get_label({"PatientName": "lastname^firstname"})
    # Backward-compatibility #FLYW-3539
    assert "3" == subject_map.get_code({"PatientName": "lastname^firstname"})

    # Check that repeated values are the same
    assert "1" == subject_map.get_label(
        {"PatientName": " LASTNAME^FIRSTNAME ", "PatientBirthDate": "20000101"}
    )
    # Backward-compatibility #FLYW-3539
    assert "1" == subject_map.get_code(
        {"PatientName": " LASTNAME^FIRSTNAME ", "PatientBirthDate": "20000101"}
    )

    subject_map.save()

    # Read the CSV file and verify the contents
    with open(path, "r") as f:
        reader = csv.DictReader(f)
        rows = iter(reader)
        assert reader.fieldnames == DEFAULT_FIELDS + [SUBJECT_LABEL_KEY, CSV_CONFIG_KEY]

        # Config row
        row = next(rows)
        for field in DEFAULT_FIELDS:
            assert row.get(field, "") == ""

        assert row.get(SUBJECT_LABEL_KEY, "") == ""
        assert row.get(CSV_CONFIG_KEY)

        # Check subject records
        row = next(rows)
        assert row["PatientName"] == "lastname^firstname"
        assert row["PatientBirthDate"] == "20000101"
        assert row[SUBJECT_LABEL_KEY] == "1"
        assert not row.get(CSV_CONFIG_KEY)

        row = next(rows)
        assert row["PatientName"] == "last first"
        assert row["PatientBirthDate"] == "20010202"
        assert row[SUBJECT_LABEL_KEY] == "2"
        assert not row.get(CSV_CONFIG_KEY)

        row = next(rows)
        assert row["PatientName"] == "lastname^firstname"
        assert row["PatientBirthDate"] == ""
        assert row[SUBJECT_LABEL_KEY] == "3"
        assert not row.get(CSV_CONFIG_KEY)

        # No more rows
        try:
            next(rows)
            pytest.fail("Expected no more rows!")
        except StopIteration:
            pass

    os.remove(path)
