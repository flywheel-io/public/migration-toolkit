import datetime
import os
from unittest import mock

import pydicom
import pytest
import six
import tzlocal
from fs.osfs import OSFS
from fw_file.dicom import DICOM

from flywheel_migration import util


def diff_dates(d1, d2, unit=None, max_value=None):
    d1 = datetime.datetime.strptime(d1, "%Y%m%d")
    d2 = datetime.datetime.strptime(d2, "%Y%m%d")

    d, u = util.date_delta(d1, d2, desired_unit=unit, max_value=max_value)
    return "{}{}".format(d, u)


def test_date_delta():
    assert diff_dates("20180904", "20180901") == "3D"
    assert diff_dates("20150904", "20180904") == "1096D"
    assert diff_dates("20150904", "20180904", max_value=999) == "36M"
    assert diff_dates("20150904", "20180904", unit="Y", max_value=999) == "3Y"
    assert diff_dates("20180904", "19380101", max_value=960) == "80Y"


def test_matches_byte_sig_should_return_true_if_byte_signatures_match():
    assert util.matches_byte_sig(b"DICM hello", 0, b"DICM")


def test_matches_byte_sig_should_return_true_if_byte_signature_matches_after_offset():
    assert util.matches_byte_sig(b"hello DICM world", 6, b"DICM")


def test_matches_byte_sig_should_return_false_if_byte_signatures_dont_match():
    assert not util.matches_byte_sig(b"abcd", 0, b"DICM")


def test_is_dicom_should_not_read_entire_file_into_memory():
    src_fs = mock.MagicMock()
    file_object = src_fs.open.return_value.__enter__.return_value

    util.is_dicom(src_fs, "filepath")

    file_object.read.assert_called_with(132)


def test_is_dicom_should_call_matches_byte_sig_with_file_bytes():
    src_fs = mock.MagicMock()
    file_bytes = src_fs.open.return_value.__enter__.return_value.read.return_value

    with mock.patch("flywheel_migration.util.matches_byte_sig") as m:
        util.is_dicom(src_fs, "filepath")

        m.assert_called_with(file_bytes, 128, b"DICM")


def test_is_dicom_should_return_matches_byte_sig_return_value():
    src_fs = mock.MagicMock()

    with mock.patch("flywheel_migration.util.matches_byte_sig") as m:
        result = util.is_dicom(src_fs, "filepath")

        assert result == m.return_value


def test_is_dicom_should_return_true_for_dicom_file(dicom_file):
    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)
    assert util.is_dicom(test_fs, basename)


def test_is_dicom_should_return_false_for_non_dicom(efile):
    path = efile
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)
    assert not util.is_dicom(test_fs, basename)


def test_is_dicom_should_return_true_for_invalid_dicom(dicom_file):
    path = dicom_file("invalid", "invalid.dcm")
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)
    assert util.is_dicom(test_fs, basename)


def test_is_jpg_should_return_false_for_non_jpg(efile):
    path = efile
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)
    assert not util.is_jpg(test_fs, basename)


def test_is_jpg_should_return_true_for_jpg_file(jpg_file):
    path = jpg_file("camera", "DSCN0010_with_gps.jpg")
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)
    assert util.is_jpg(test_fs, basename)


def test_dict_paths():
    tree = {"SequenceKeyword": {0: "OtherKeyword", 1: {"OtherKeyword": {0: "Test"}}}}
    res = list(util.dict_paths(tree))
    assert res == [
        ["SequenceKeyword", 0, "OtherKeyword"],
        ["SequenceKeyword", 1, "OtherKeyword", 0, "Test"],
    ]


def test_walk_dicom_wild_sequence(dicom_file):
    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    dcm = pydicom.dcmread(path)
    tag_list = ["AnatomicRegionSequence", "*", "CodeValue"]
    res = util.walk_dicom_wild_sequence(dcm, tag_list)
    assert res == {"AnatomicRegionSequence": {0: "CodeValue"}}

    tag_list = ["00082218", "*", "CodeValue"]  # 00082218 is AnatomicRegionSequence
    res = util.walk_dicom_wild_sequence(dcm, tag_list)
    assert res == {"00082218": {0: "CodeValue"}}

    ds = pydicom.dataset.Dataset()
    dcm["AnatomicRegionSequence"].value.append(ds)
    tag_list = ["AnatomicRegionSequence", "*", "CodeValue"]
    res = util.walk_dicom_wild_sequence(dcm, tag_list)
    assert res == {"AnatomicRegionSequence": {0: "CodeValue", 1: "CodeValue"}}

    delattr(dcm, "AnatomicRegionSequence")
    res = util.walk_dicom_wild_sequence(dcm, tag_list)
    assert res == {"AnatomicRegionSequence": {}}


def test_walk_dict_wild_sequence():
    in_dict = {"AnatomicRegionSequence": [{"CodeValue": None}, {"CodeValue": None}]}
    tag_list = ["AnatomicRegionSequence", "*", "CodeValue"]
    res = util.walk_dict_wild_sequence(in_dict, tag_list)
    assert res == {"AnatomicRegionSequence": {0: "CodeValue", 1: "CodeValue"}}

    tag_list = ["DoNotExist", "*", "DoNotExist"]
    res = util.walk_dict_wild_sequence(in_dict, tag_list)
    assert res == {"DoNotExist": {}}


def test_is_dicom_should_return_false_for_non_png(efile):
    path = efile
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)
    assert not util.is_png(test_fs, basename)


def test_is_dicom_should_return_true_for_png_file(png_file):
    path = png_file("exif", "sample1.png")
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)
    assert util.is_png(test_fs, basename)


@mock.patch("os.sep", "/")
@mock.patch("os.name", "posix")
def test_hash_path_posix():
    input_str = "/dir/starts/with/sep"
    exp_str = os.sep + os.sep.join(
        [util.hash_value(x) for x in "dir/starts/with/sep".split(os.sep)]
    )
    assert util.hash_path(input_str, util.hash_value) == exp_str
    input_str = "//dir/starts/with/sep"
    exp_str = 2 * os.sep + os.sep.join(
        [util.hash_value(x) for x in "dir/starts/with/sep".split(os.sep)]
    )
    assert util.hash_path(input_str, util.hash_value) == exp_str


@mock.patch("os.sep", "\\")
@mock.patch("os.name", "nt")
def test_hash_path_nt():
    # test nt
    input_str = "\dir\starts\with\sep"
    exp_str = os.sep + os.sep.join(
        [util.hash_value(x) for x in "dir\starts\with\sep".split(os.sep)]
    )
    assert util.hash_path(input_str, util.hash_value) == exp_str
    input_str = "C:\dir\starts\with\sep"
    exp_str = (
        "C:"
        + os.sep
        + os.sep.join([util.hash_value(x) for x in "dir\starts\with\sep".split(os.sep)])
    )
    assert util.hash_path(input_str, util.hash_value) == exp_str


def test_get_dicom_data_elements_hex_path_log_warning_when_walking_corrupted_dicom(
    dicom_file, caplog
):
    path = dicom_file("invalid", "invalid_VR_in_UN_sequence.dcm")
    dcm = pydicom.dcmread(path, force=True)
    # Test file as a SQ element with VR=UN -> non-decoded by default
    res_hex = util.get_dicom_data_elements_hex_path(dcm)
    res_kw = util.get_dicom_data_elements_keyword_path(dcm)
    assert len(res_hex) == 1
    assert len(res_kw) == 1

    # Test file has a SQ element with VR=UN
    # withing this context VR are infered (UN->SQ) and reveals data elements
    # in sequence that turn out to be corrupted (VR corrupted), 1 public and 2 privates.
    dcm = DICOM(path, force=True)
    dcm.decode()
    res_hex = util.get_dicom_data_elements_hex_path(dcm.dataset.raw)
    res_kw = util.get_dicom_data_elements_keyword_path(dcm.dataset.raw)
    assert (
        len(res_hex) == 4
    )  # 1 Seq element + 1 public + 2 private data elements in sequence[0]
    assert len(res_kw) == 4  # 1 Seq element + 1 public data element in sequence[0]


@pytest.mark.parametrize(
    "original,sanitized",
    [
        ('fi:l*e/p"a?t>h|.t<xt', "fi_l_e_p_a_t_h_.t_xt"),
        ("random.t2*", "random.t2star"),
        ("random.T2*", "random.T2star"),
        ("random.t2 *", "random.t2 star"),
        ("random.T2 *", "random.T2 star"),
        ("random.t2_*", "random.t2_star"),
        ("random.T2_*", "random.T2_star"),
    ],
)
def test_sanitize_filename(original, sanitized):
    assert sanitized == util.sanitize_filename(original)


def test_get_dicom_data_elements_keyword_path_do_not_list_repeating_group_kw(
    dicom_file,
):
    path = dicom_file("pydicom", "CT_small.dcm")
    dcm = pydicom.dcmread(path)
    # adding a repeater tag
    dcm.add_new(
        pydicom.tag.Tag(0x60000051), "US", 1
    )  # ImageFrameOrigin, in repeating group range
    kw_list = util.get_dicom_data_elements_keyword_path(dcm)
    assert "ImageFrameOrigin" not in kw_list


def test_sort_info(dicom_file):
    path = dicom_file(
        "16844_1_1_dicoms",
        "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
    )
    dcm = pydicom.dcmread(path)
    dcm["ReferringPhysicianName"].value = "  grp/prj  "
    sort_info = dcm.get("ReferringPhysicianName")
    (
        subject_label,
        group__id,
        project_label,
    ) = util.parse_sort_info(sort_info, "ex" + dcm.get("StudyID", ""))

    assert subject_label == "ex16844"
    assert group__id == "grp"
    assert project_label == "prj"


def test_local_timezone():
    # get expected tz
    expected_timezone = tzlocal.get_localzone()
    actual_tz = util.get_local_timezone()
    assert expected_timezone == actual_tz


def test_localize_timestamp():
    timestamp = datetime.datetime(2021, 11, 15, 18, 0, 6)  # -5
    timestamp_localizer = util.localize_timestamp(timestamp=timestamp)
    assert timestamp_localizer.tzinfo is not None
