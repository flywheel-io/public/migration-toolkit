import os
import textwrap

import pandas as pd
import six
from fs.osfs import OSFS

from flywheel_migration import deidentify
from flywheel_migration.deidentify.deid_field import DeIdField
from flywheel_migration.deidentify.table_file_profile import CSVFileProfile


def test_csv_profile_file_process_file(table_file, template_file):
    path = table_file("csv", "sample.csv")
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)
    profile = deidentify.load_profile(template_file("csv-profile"))

    profile.process_packfile("csv", test_fs, test_fs, [basename])
    # Verify the de-id
    expected_filename = "toto.csv"
    with test_fs.open(expected_filename, "r") as fp:
        df = pd.read_csv(fp)
    assert "SeriesNumber" not in df
    assert df.loc[0, "SiteSubject"] == "one_cool_cat_2_16Sep2017 15:29:57"
    assert df.loc[0, "SeriesDateTime"] == "16Sep2017 15:29:57"
    assert df.loc[1, "SiteSubject"] == "one_cool_cat_3_16Sep2018 15:29:57"
    assert df.loc[1, "SeriesDateTime"] == "16Sep2018 15:29:57"


def test_csv_profile_file_export_config(template_file):
    profile = CSVFileProfile()
    profile.delimiter = "\t"
    profile.reader = "tsv"
    config = profile.to_config()
    assert config["delimiter"] == "\t"
    assert config["reader"] == "tsv"


def test_table_profile_validate(table_file, template_file):
    profile = CSVFileProfile()
    errors = profile.validate()
    assert len(errors) == 0

    profile.reader = None
    errors = profile.validate()
    assert len(errors) == 1
    assert "Table profile invalid: reader is not defined" in errors[0]

    profile.reader = "doesnotexist"
    errors = profile.validate()
    assert len(errors) == 1
    assert "Unknown read method for reader: doesnotexist" in errors[0]


def test_csv_profile_can_process_empty_csv_temp(tmpdir, table_file, template_file):
    path = table_file("csv", "header-only.csv")
    dirname, basename = os.path.split(path)
    basename = six.u(basename)  # fs requires unicode
    test_fs = OSFS(dirname)
    dst_fs = OSFS(tmpdir)
    profile = CSVFileProfile()
    profile.process_files(test_fs, dst_fs, [basename])

    with dst_fs.open(basename, "r") as fp:
        res = pd.read_csv(fp)
    assert all(res.columns == ["Empty", "DataFrame"])


def test_csv_profile_remove_column_not_exist(tmp_path):
    test_fs = OSFS(tmp_path)
    dst_fs = OSFS(tmp_path)
    basename = "test.csv"
    with open(tmp_path / basename, "w") as fp:
        fp.write(
            textwrap.dedent(
                """
            test,test1,test2
            v1,v2,v3
            v4,v5,v6
            """
            )
        )
    profile = CSVFileProfile()
    profile.add_field(DeIdField.factory({"name": "test3", "remove": True}))
    profile.add_field(DeIdField.factory({"name": "test1", "remove": True}))
    profile.process_files(test_fs, test_fs, [basename])

    with dst_fs.open(basename, "r") as fp:
        res = pd.read_csv(fp)
    assert all(res.columns == ["test", "test2"])
