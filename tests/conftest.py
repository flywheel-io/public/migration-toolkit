import os
import shutil
import tempfile

import pytest

DATA_ROOT = os.path.join("tests", "data")
DICOM_ROOT = os.path.join(DATA_ROOT, "DICOM")
JPG_ROOT = os.path.join(DATA_ROOT, "JPG")
TIFF_ROOT = os.path.join(DATA_ROOT, "TIFF")
PNG_ROOT = os.path.join(DATA_ROOT, "PNG")
NIFTI_ROOT = os.path.join(DATA_ROOT, "NIfTI")
XML_ROOT = os.path.join(DATA_ROOT, "XML")
JSON_ROOT = os.path.join(DATA_ROOT, "JSON")
KVTEXT_ROOT = os.path.join(DATA_ROOT, "key-value-text-file")
TABLE_ROOT = os.path.join(DATA_ROOT, "table")
ZIP_ROOT = os.path.join(DATA_ROOT, "ZIP")


@pytest.fixture(scope="function")
def dicom_file():
    def get_dicom_file(folder, filename):
        fd, path = tempfile.mkstemp(suffix=".dcm")
        os.close(fd)

        src_path = os.path.join(DICOM_ROOT, folder, filename)
        shutil.copy(src_path, path)

        return path

    return get_dicom_file


@pytest.fixture(scope="function")
def efile():
    fd, path = tempfile.mkstemp("EFILE")
    os.close(fd)

    src_path = os.path.join(DATA_ROOT, "EFILE")
    shutil.copy(src_path, path)

    return path


@pytest.fixture(scope="function")
def pfile():
    fd, path = tempfile.mkstemp("PFILE")
    os.close(fd)

    src_path = os.path.join(DATA_ROOT, "PFILE")
    shutil.copy(src_path, path)

    return path


@pytest.fixture(scope="function")
def bruker_file():
    def get_bruker_path(folder, filename):
        return os.path.join(DATA_ROOT, folder, filename)

    return get_bruker_path


@pytest.fixture(scope="function")
def par_file():
    def get_par_path(filename):
        return os.path.join(DATA_ROOT, filename)

    return get_par_path


@pytest.fixture(scope="function")
def template_file():
    def get_template_file(name):
        return os.path.join(DATA_ROOT, name + ".yml")

    return get_template_file


@pytest.fixture(scope="function")
def filenames_file():
    def get_filename_path(filename):
        fd, path = tempfile.mkstemp(suffix=".dcm")
        os.close(fd)

        src_path = os.path.join(DATA_ROOT, "filenames", filename)
        shutil.copy(src_path, path)

        return path

    return get_filename_path


@pytest.fixture(scope="function")
def jpg_file():
    def get_jpg_file(folder, filename):
        fd, path = tempfile.mkstemp(suffix=".jpg")
        os.close(fd)
        src_path = os.path.join(JPG_ROOT, folder, filename)
        shutil.copy(src_path, path)
        return path

    return get_jpg_file


@pytest.fixture(scope="function")
def tiff_file():
    def get_tiff_file(folder, filename):
        fd, path = tempfile.mkstemp(suffix=".tiff")
        os.close(fd)
        src_path = os.path.join(TIFF_ROOT, folder, filename)
        shutil.copy(src_path, path)
        return path

    return get_tiff_file


@pytest.fixture(scope="function")
def png_file():
    def get_png_file(folder, filename):
        fd, path = tempfile.mkstemp(suffix=".png")
        os.close(fd)
        src_path = os.path.join(PNG_ROOT, folder, filename)
        shutil.copy(src_path, path)
        return path

    return get_png_file


@pytest.fixture(scope="function")
def nifti_file():
    def get_nifti_file(folder, filename):
        fd, path = tempfile.mkstemp(suffix=".nii.gz")
        os.close(fd)
        src_path = os.path.join(NIFTI_ROOT, folder, filename)
        shutil.copy(src_path, path)
        return path

    return get_nifti_file


@pytest.fixture(scope="function")
def xml_file():
    def get_xml_file(folder, filename):
        fd, path = tempfile.mkstemp(suffix=".xml")
        os.close(fd)
        src_path = os.path.join(XML_ROOT, folder, filename)
        shutil.copy(src_path, path)

        return path

    return get_xml_file


@pytest.fixture(scope="function")
def json_file():
    def get_json_file(folder, filename):
        fd, path = tempfile.mkstemp(suffix=".json")
        os.close(fd)
        src_path = os.path.join(JSON_ROOT, folder, filename)
        shutil.copy(src_path, path)

        return path

    return get_json_file


@pytest.fixture(scope="function")
def kvtext_file():
    def get_kvtext_file(folder, filename):
        dir_path = tempfile.mkdtemp()
        src_path = os.path.join(KVTEXT_ROOT, folder, filename)
        out_path = os.path.join(dir_path, filename)
        shutil.copyfile(src_path, out_path)

        return out_path

    return get_kvtext_file


@pytest.fixture(scope="function")
def table_file():
    def get_table_file(folder, filename):
        _, suffix = os.path.splitext(filename)
        fd, path = tempfile.mkstemp(suffix=suffix)
        os.close(fd)
        src_path = os.path.join(TABLE_ROOT, folder, filename)
        shutil.copy(src_path, path)

        return path

    return get_table_file


@pytest.fixture(scope="function")
def zip_file():
    def get_zip_file(folder, filename):
        dir_path = tempfile.mkdtemp()
        src_path = os.path.join(ZIP_ROOT, folder, filename)
        out_path = os.path.join(dir_path, filename)
        shutil.copyfile(src_path, out_path)

        return out_path

    return get_zip_file
