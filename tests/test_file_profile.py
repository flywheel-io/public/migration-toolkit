import pytest

from flywheel_migration.deidentify.file_profile import FileProfile


def test_read_field_returns_none():
    for subclass in FileProfile.get_subclasses():
        subclass_instance = subclass()
        value = subclass_instance.read_field(None, dict(), "does_not_exist")
        assert value is None


@pytest.mark.parametrize("profile", [p() for p in FileProfile.get_subclasses()])
def test_default_hash_digits_is_16_for_all_subclasses(profile):
    assert profile.hash_digits == 16


def test_hash_digits_can_be_set():
    config = {"hash-digits": 32}
    profile = FileProfile()

    assert profile.hash_digits == 0
    profile.load_config(config)
    assert profile.hash_digits == 32
