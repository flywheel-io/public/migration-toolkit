import datetime

from flywheel_migration import parse_bruker_epoch, parse_bruker_params


def test_bruker_subject_v5(bruker_file):
    path = bruker_file("pv5", "subject.txt")
    with open(path, "r") as f:
        results = parse_bruker_params(f)

    assert results
    assert results["SUBJECT_id"] == "010005-m00"
    assert results["SUBJECT_weight"] == "65"
    assert results["SUBJECT_name"] == "(<>, <010005-m00>)"
    assert results["SUBJECT_study_name"] == "protocol 76"
    assert results["PARAVISION_version"] == "2.0.1"


def test_bruker_acq_v5(bruker_file):
    path = bruker_file("pv5", "acqp.txt")
    with open(path, "r") as f:
        results = parse_bruker_params(f)

    assert results
    assert results["ACQ_experiment_mode"] == "SingleExperiment"
    assert results["ACQ_protocol_name"] == "EPI_FID_200_fmri"
    assert results["ACQ_slice_offset"] == (
        "-49.000 -44.000 -39.000 -34.000 -29.000 -24.000 -19.000 -14.000 -9.000 -4.000 "
        "1.000 6.000 11.000 16.000 21.000 26.000 31.000 36.000 41.000 46.000 51.000"
    )
    assert results["ACQ_institution"] == "Cambridge U."
    assert results["ACQ_abs_time"] == "978615264"
    assert results["PARAVISION_version"] == "2.0.1"


def test_parse_bruker_epoch():
    assert parse_bruker_epoch(None) is None
    assert parse_bruker_epoch("978615264") == datetime.datetime(2001, 1, 4, 13, 34, 24)
    assert parse_bruker_epoch("(1548755100, 299, 60)") == datetime.datetime(
        2019, 1, 29, 9, 45
    )
